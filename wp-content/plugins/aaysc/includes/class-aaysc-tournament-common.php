<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/include
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/include
 * @author     Masood U <masood.u@allshoreresources.com>
 */
class Aaysc_Tournament_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Aaysc_Tournament    The ID of this plugin.
	 */
	private $Aaysc_Tournament;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version, $db, $tb_pools, $tb_pool_schedule, $tb_brackets, $tb_bracket_schedule, $tb_bracket_scoreboard, $tb_t_registration;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Aaysc_Tournament       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Aaysc_Tournament, $version ) {

		global $wpdb;
		$this->Aaysc_Tournament = $Aaysc_Tournament;
		$this->version = $version;
		$this->db = $wpdb;
		$this->tb_pools = $this->db->prefix.'pools';
		$this->tb_pool_schedule = $this->db->prefix.'pool_schedule';
		$this->tb_pool_scoreboard = $this->db->prefix.'pool_scoreboard';
		$this->tb_brackets = $this->db->prefix.'brackets';
		$this->tb_bracket_schedule = $this->db->prefix.'bracket_schedule';
		$this->tb_bracket_scoreboard = $this->db->prefix.'bracket_scoreboard';
		$this->tb_t_registration = $this->db->prefix.'t_registration';
	}


	public static function float_format( $number, $decimal = 3 ){
		$number_array = number_format($number, $decimal);
	    $number_array = explode('.', $number_array);
	    $left = ($number_array[0] != 0) ? $number_array[0]: '';
	    $right = $number_array[1];
	    return $left.'.'.$right;
	}
	/* Get Team data from team id. */
	public static function getTeamData($tid, $tournamentID = 0){
		if(!$tid) return false;
		global $wpdb;
		$where = array("`team_id`='$tid'");

		if($tid == -1){
			return $wpdb->get_row("SELECT * FROM `$this->tb_t_registration` where ". implode(' AND ', $where) ." ORDER BY rank_id", OBJECT);
		}
		if($tournamentID) $where[] = "`t_id`='$tournamentID'";
		return $wpdb->get_row("SELECT * FROM `$this->tb_t_registration` where ". implode(' AND ', $where) ." ORDER BY rank_id", OBJECT);
	}
	public static function get_coachdata($user_id){
		$data = get_userdata( $user_id );
		$data->address1  = get_metadata( 'user', $user_id, 'address1', true );
		$data->address2  = get_metadata( 'user', $user_id, 'address2', true );
		$data->city 	 = get_metadata( 'user', $user_id, 'city', true );
		$data->state 	 = get_metadata( 'user', $user_id, 'state', true );
		$data->zip_code  = get_metadata( 'user', $user_id, 'zip_code', true );
		$data->mob_phone = get_metadata( 'user', $user_id, 'mob_phone', true );
		return $data;
	}
	/* Format age group title. */
	public static function formateTitle($title){
		if(!$title) return;
		$title = str_replace('and', '&', $title);
		return str_replace('_', ' ', $title);
	}
	public static function entry_deadline($tid){
		$regDate = new DateTime( get_field('start_date', $tid) );
        $regDate->modify("-1 day");
        return $regDate;
	}
	public static function clean_age_group($agc_name){
		$term_change = str_replace("&", "and", $agc_name);
        $term_change = str_replace("andamp;", "and", $term_change);
        $term_change = str_replace(" ", "_", $term_change);
        return $term_change;
	}
	public static function term_children(){
		$parent_terms = get_terms("age_groups", "orderby=count&hide_empty=0&parent=0");
		$termChildren = array();
		foreach ($parent_terms as $pterm) {
		    //Get the Child terms
		    $terms = get_terms('age_groups', array('parent' => $pterm->term_id, 'orderby' => 'slug', 'hide_empty' => false));
		    foreach ($terms as $term) {
		        $termChildren[] = $term;
		    }
		}
		return $termChildren;
	}

	public static function debug($message) {
	    if (WP_DEBUG === true) {
	        if (is_array($message) || is_object($message)) {
	            error_log(print_r($message, true));
	        } else {
	            error_log($message);
	        }
	    }
	}

	/*
	 * used for tracking error messages
	 */
	public static function aaysc_errors(){
	    static $wp_error; // Will hold global variable safely
	    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
	}

	/*
	 * show any error messages after form submission
	 */
	public static function aaysc_show_error_messages(){
		if($codes = self::aaysc_errors()->get_error_codes()) {
		echo '<div class="aaysc_errors">';
		    // Loop error codes and display errors
		   foreach($codes as $code){
		        $message = self::aaysc_errors()->get_error_message($code);
		        echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
		    }
		echo '</div>';
	}
	}

	/**
	*   Child page conditional
	*   @ Accept's page ID, page slug or page title as parameters
	*/
	public static function is_children( $parent = '' ) { //need to work on it
	    global $post;

	    $parent_obj = get_page( $post->post_parent, ARRAY_A );
	    $parent = (string) $parent;
	    $parent_array = (array) $parent;

	    if ( in_array( (string) $parent_obj['ID'], $parent_array ) ) {
	        return false;
	    } elseif ( in_array( (string) $parent_obj['post_title'], $parent_array ) ) {
	        return false;
	    } elseif ( in_array( (string) $parent_obj['post_name'], $parent_array ) ) {
	        return false;
	    } else {
	        return $parent_obj['ID'];
	    }
	}

	/*
	Explode the tournament type
	 */
	public static function is_tournament(){
		if ( defined( 'TOURNAMENT' ) ) {
			return explode(";", TOURNAMENT);
		}
		return null;
	}

	/*
	Explode the tournament type
	 */
	public static function get_sports(){
		if ( defined( 'TOURNAMENT' ) ) {
			#return explode(";", TOURNAMENT);
			return array('event' => 'Baseball', 'basketball' => 'Basketball');
		}
		return null;
	}

	/**
	 * Return Tournament Season in array
	 *
	 * @return array [later we fetch season from database]
	 */
	public static function get_seasons(){
		if ( defined( 'SEASON' ) ) {
			return array('2015' => '2015', '2016' => '2016');
		}
		return null;
	}
}

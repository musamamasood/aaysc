<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/includes
 * @author     Your Name <email@example.com>
 */
class Aaysc_Tournament {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Aaysc_Tournament_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $Aaysc_Tournament    The string used to uniquely identify this plugin.
	 */
	protected $Aaysc_Tournament;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * varible for plugins.
	 */
	protected $plugin_shortcode;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->Aaysc_Tournament = 'aaysc-tournament';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Aaysc_Tournament_Loader. Orchestrates the hooks of the plugin.
	 * - Aaysc_Tournament_i18n. Defines internationalization functionality.
	 * - Aaysc_Tournament_Admin. Defines all hooks for the admin area.
	 * - Aaysc_Tournament_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-aaysc-tournament-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-aaysc-tournament-i18n.php';

		/**
		 * The class responsible for defining all common functions that access in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-aaysc-tournament-common.php';

		/**
		 * The class responsible for defining all common functions that access in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-aaysc-tournament-shortcode.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-aaysc-tournament-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-aaysc-tournament-public.php';

		$this->loader = new Aaysc_Tournament_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Aaysc_Tournament_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Aaysc_Tournament_i18n();
		$plugin_i18n->set_domain( $this->get_Aaysc_Tournament() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Aaysc_Tournament_Admin( $this->get_Aaysc_Tournament(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		//Ajax Sections
		$this->loader->add_action( 'wp_ajax_prepare_pool', $plugin_admin, 'prepare_pool' );
		$this->loader->add_action( 'wp_ajax_publish_pool', $plugin_admin, 'publish_pool' );
		$this->loader->add_action( 'wp_ajax_prepare_bracket', $plugin_admin, 'prepare_bracket' );
		$this->loader->add_action( 'wp_ajax_publish_bracket', $plugin_admin, 'publish_bracket' );
		$this->loader->add_action( 'wp_ajax_delete_registration', $plugin_admin, 'delete_registration' );
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'aaysc_add_meta_box', 10, 2 );
		$this->loader->add_filter( 'get_terms', $plugin_admin, 'get_terms_filter', 10, 3 );
		$this->loader->add_action( 'phpmailer_init', $plugin_admin, 'phpmailer_aaysc' );
		//Progress after update
		$this->loader->add_action( 'save_post', $plugin_admin, 'tournament_update' );
		// add acf fields
		$this->loader->add_action( 'init', $this, 'init_admin' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public 	  = new Aaysc_Tournament_Public( $this->get_Aaysc_Tournament(), $this->get_version() );
		$this->plugin_shortcode = new Aaysc_Tournament_Shortcode( $this->get_Aaysc_Tournament(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'after_setup_theme', $plugin_public, 'remove_admin_bar' );
		//Ajax
		$this->loader->add_action( 'wp_ajax_pool_scoreboard', $plugin_public, 'pool_scoreboard' );
		$this->loader->add_action( 'wp_ajax_bracket_scoreboard', $plugin_public, 'bracket_scoreboard' );
		$this->loader->add_filter( 'page_template', $plugin_public, 'tournament_templates' );
		$this->loader->add_filter( 'template_include', $plugin_public, 'tournament_templates' );
		//Shortcodes
		$this->loader->add_shortcode( 'coach_registration_form', $this->plugin_shortcode, 'aaysc_coach_registration_form' );
		$this->loader->add_action( 'init', $this, 'init_public' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_Aaysc_Tournament() {
		return $this->Aaysc_Tournament;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Aaysc_Tournament_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
	 * Register the init function for register user.
	 *
	 * @since     1.0.0
	 * @return    array    acf exported fields.
	 */
	public function init_admin(){
		if(function_exists("register_field_group")){
			register_field_group(array (
				'id' => 'acf_event-fields',
				'title' => 'Event Fields',
				'fields' => array (
					array (
						'key' => 'field_55b644230789d',
						'label' => 'Event Logo',
						'name' => 'event_logo',
						'type' => 'image',
						#'required' => 1,
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_561d18fa054f3',
						'label' => 'Row Open',
						'name' => 'row_open',
						'type' => 'row',
						'row_type' => 'row_open',
						'col_num' => 4,
					),
					array (
						'key' => 'field_55b643af07897',
						'label' => 'Start Date',
						'name' => 'start_date',
						'type' => 'date_picker',
						'required' => 1,
						'date_format' => 'yymmdd',
						'display_format' => 'dd/mm/yy',
						'first_day' => 1,
					),
					array (
						'key' => 'field_55b643c507898',
						'label' => 'End Date',
						'name' => 'end_date',
						'type' => 'date_picker',
						'required' => 1,
						'date_format' => 'yymmdd',
						'display_format' => 'dd/mm/yy',
						'first_day' => 1,
					),
					array (
						'key' => 'field_55b643e507899',
						'label' => 'Format',
						'name' => 'format',
						'type' => 'text',
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55b643f90789a',
						'label' => 'Event Type',
						'name' => 'event_type',
						'type' => 'text',
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_561d1baafce7e',
						'label' => 'Row Close',
						'name' => 'row_close',
						'type' => 'row',
						'row_type' => 'row_close',
						'col_num' => 4,
					),
					array (
						'key' => 'field_561d18fa054f4',
						'label' => 'Row Open',
						'name' => 'row_open',
						'type' => 'row',
						'row_type' => 'row_open',
						'col_num' => 3,
					),
					array (
						'key' => 'field_55b644050789b',
						'label' => 'Event Venue',
						'name' => 'event_venue',
						'type' => 'text',
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55b644140789c',
						'label' => 'Fee Range',
						'name' => 'fee_range',
						'type' => 'text',
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55c3b5433c716',
						'label' => 'Contact',
						'name' => 'contact',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_561d1baafce5e',
						'label' => 'Row Close',
						'name' => 'row_close',
						'type' => 'row',
						'row_type' => 'row_close',
						'col_num' => 3,
					),
					array (
						'key' => 'field_55b6443b0789e',
						'label' => 'Event Location',
						'name' => 'event_location',
						'type' => 'google_map',
						'required' => 1,
						'center_lat' => '',
						'center_lng' => '',
						'zoom' => '',
						'height' => '',
					),
					array (
						'key' => 'field_55b644b1078a0',
						'label' => 'Event Gallery',
						'name' => 'event_gallery',
						'type' => 'repeater',
						'sub_fields' => array (
							array (
								'key' => 'field_55b644cb078a1',
								'label' => 'Event Gallery Images',
								'name' => 'event_gallery_images',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'object',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
						),
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'table',
						'button_label' => 'Add Image',
					),
					array (
						'key' => 'field_561d18fa053f4',
						'label' => 'Row Open',
						'name' => 'row_open',
						'type' => 'row',
						'row_type' => 'row_open',
						'col_num' => 3,
					),
					array (
						'key' => 'field_55c3b4f33c713',
						'label' => 'Event Awards',
						'name' => 'event_awards',
						'type' => 'textarea',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'formatting' => 'br',
					),
					array (
						'key' => 'field_55c3b5123c714',
						'label' => 'Roster Requirements',
						'name' => 'roster_requirements',
						'type' => 'textarea',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'formatting' => 'br',
					),
					array (
						'key' => 'field_55c3b5323c715',
						'label' => 'Championship Births',
						'name' => 'championship_births',
						'type' => 'textarea',
						'default_value' => '',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => '',
						'formatting' => 'br',
					),
					array (
						'key' => 'field_561d1baafce3e',
						'label' => 'Row Close',
						'name' => 'row_close',
						'type' => 'row',
						'row_type' => 'row_close',
						'col_num' => 3,
					),
					array (
						'key' => 'field_561546d6dc9c4',
						'label' => 'Result Images',
						'name' => 'result_images',
						'type' => 'repeater',
						'sub_fields' => array (
							array (
								'key' => 'field_561547d0dc9c5',
								'label' => 'Age Group',
								'name' => 'age_group',
								'type' => 'taxonomy',
								'column_width' => 40,
								'taxonomy' => 'age_groups',
								'field_type' => 'select',
								'allow_null' => 0,
								'load_save_terms' => 0,
								'return_format' => 'object',
								'multiple' => 0,
							),
							array (
								'key' => 'field_5615481bdc9c6',
								'label' => 'Winner Image',
								'name' => 'winner_image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'url',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_56154847dc9c7',
								'label' => 'Runner-Up Image',
								'name' => 'runnerup_image',
								'type' => 'image',
								'column_width' => '',
								'save_format' => 'url',
								'preview_size' => 'thumbnail',
								'library' => 'all',
							),
							array (
								'key' => 'field_561579d1a9036',
								'label' => 'Flipbox Back Winner',
								'name' => 'flipbox_back_winner',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '',
								'toolbar' => 'full',
								'media_upload' => 'no',
							),
							array (
								'key' => 'field_561579d1a3690',
								'label' => 'Flipbox Back Runner',
								'name' => 'flipbox_back_runner',
								'type' => 'wysiwyg',
								'column_width' => '',
								'default_value' => '',
								'toolbar' => 'full',
								'media_upload' => 'no',
							),
						),
						'row_min' => '',
						'row_limit' => '',
						'layout' => 'row',
						'button_label' => 'Add Result Images',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'event',
							'order_no' => 0,
							'group_no' => 0,
						),
						array (
							'param' => 'page_template',
							'operator' => '==',
							'value' => '',
							'order_no' => 1,
							'group_no' => 0,
						),
					),
					array(
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'basketball',
							'order_no' => 0,
							'group_no' => 1,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
			register_field_group(array (
				'id' => 'acf_team-nick-name',
				'title' => 'Team Information',
				'fields' => array (
					array (
						'key' => 'field_561d18fa084f3',
						'label' => 'Row Open',
						'name' => 'row_open',
						'type' => 'row',
						'row_type' => 'row_open',
						'col_num' => 4,
					),
					array (
						'key' => 'field_562919db6cd91',
						'label' => 'Season',
						'name' => 'season',
						'type' => 'select',
						'choices' => aaysc_tournament_common::get_seasons(),
						'default_value' => '',
						'allow_null' => 0,
						'multiple' => 0,
					),
					array (
						'key' => 'field_562918dc6cd8d',
						'label' => 'Sport',
						'name' => 'sport',
						'type' => 'select',
						'choices' => aaysc_tournament_common::get_sports(),
						'default_value' => '',
						'allow_null' => 0,
						'multiple' => 0,
					),
					array (
						'key' => 'field_5629192e6cd8e',
						'label' => 'Divison',
						'name' => 'divison_id',
						'type' => 'taxonomy',
						'taxonomy' => 'age_groups',
						'field_type' => 'checkbox',
						'allow_null' => 0,
						'load_save_terms' => 0,
						'return_format' => 'id',
						'multiple' => 0,
					),
					array (
						'key' => 'field_5614075fe9db5',
						'label' => 'Nick Name',
						'name' => 'nick_name',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Team Nick Name',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_561d1bddfce7e',
						'label' => 'Row Close',
						'name' => 'row_close',
						'type' => 'row',
						'row_type' => 'row_close',
						'col_num' => 4,
					),
					array (
						'key' => 'field_561d18RD284f3',
						'label' => 'Row Open',
						'name' => 'row_open',
						'type' => 'row',
						'row_type' => 'row_open',
						'col_num' => 4,
					),
					array (
						'key' => 'field_562919b16cd8f',
						'label' => 'City',
						'name' => 'city',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Team City',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_562919c56cd90',
						'label' => 'State',
						'name' => 'state',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Team State',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_56291aab6cd92',
						'label' => 'Price',
						'name' => 'price',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Team Price',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_56291ace6cd93',
						'label' => 'Status',
						'name' => 'status',
						'type' => 'true_false',
						'message' => '',
						'default_value' => 1,
					),
					array (
						'key' => 'field_561d1bhhfce7e',
						'label' => 'Row Close',
						'name' => 'row_close',
						'type' => 'row',
						'row_type' => 'row_close',
						'col_num' => 4,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'team',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
			register_field_group(array (
				'id' => 'acf_division-relationship-tournament',
				'title' => 'Division Relationship Tournament',
				'fields' => array (
					array (
						'key' => 'field_561d4f6c54938',
						'label' => 'Division Type',
						'name' => 'division_type',
						'type' => 'select',
						'choices' => array (
							'baseball' => 'Baseball',
							'basketball' => 'Basketball',
						),
						'default_value' => '',
						'allow_null' => 0,
						'multiple' => 0,
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'ef_taxonomy',
							'operator' => '==',
							'value' => 'age_groups',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	/**
	 * Register the init function for public user.
	 *
	 * @since 1.5.0
	 * @param [type] $[name] [<description>]
	 */
	public function init_public(){
		// add new coach from shortcode
		$this->plugin_shortcode->aaysc_add_new_memeber();
	}

	// public function aaysc_upgrade(){
	// 	global $wpdb;
	// 	$db_version = get_option( "aaysc_db_version" );

	// 	if ($db_version != $this->get_version()) {
	// 		$charset_collate = $wpdb->get_charset_collate();
	// 		$aaysc_sport = $wpdb->prefix . 'sport';
	// 		$sqlSport = "CREATE TABLE $aaysc_sport (
	// 			id bigint(20) NOT NULL AUTO_INCREMENT,
	// 			title varchar(255) NOT NULL,
	// 			post_type varchar(255) NOT NULL,
	// 			image bigint(20) NOT NULL,
	// 			status tinyint(1) DEFAULT 0 NOT NULL,
	// 			time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	// 			UNIQUE KEY id (id),
	// 			PRIMARY KEY id (id)
	// 		) $charset_collate;";
	// 		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	// 		dbDelta( $sqlSport );
	// 		update_option( "aaysc_db_version", "1.1" );
	// 	}
	// }

}

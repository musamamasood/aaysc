<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/includes
 * @author     Your Name <email@example.com>
 */
class Aaysc_Tournament_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		global $wpdb;
		add_option( "aaysc_db_version", "1.0" );
		$charset_collate = $wpdb->get_charset_collate();
		$pools 		     = $wpdb->prefix . 'pools';
		$pool_schedule  = $wpdb->prefix . 'pool_schedule';
		$pool_scoreboard= $wpdb->prefix . 'pool_scoreboard';

		$sqlPool = "CREATE TABLE $pools (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			title varchar(255) NOT NULL,
			age_group varchar(255) NOT NULL,
			tid bigint(20) NOT NULL,
			status tinyint(1) DEFAULT 0 NOT NULL,
			time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			location varchar(255) NOT NULL,
			UNIQUE KEY id (id),
			PRIMARY KEY id (id)
		) $charset_collate;";
		$sqlSchedule = "CREATE TABLE $pool_schedule (
			id bigint(20) NOT NULL AUTO_INCREMENT,
			team_a_id bigint(20) NOT NULL,
			team_b_id bigint(20) NOT NULL,
			team_a_score bigint(20) DEFAULT -1 NOT NULL,
			team_b_score bigint(20) DEFAULT -1 NOT NULL,
			pool_id bigint(20) NOT NULL,
			time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
			location varchar(255) NOT NULL,
			UNIQUE KEY id (id),
			PRIMARY KEY id (id)
		) $charset_collate;";
		$sqlScoreboard = "CREATE TABLE $pool_scoreboard (
			id bigint(20) NOT NULL,
		  	team_id bigint(20) NOT NULL,
		  	pool_id bigint(20) NOT NULL,
		  	wi bigint(20) NOT NULL,
			lo bigint(20) NOT NULL,
			tie bigint(20) NOT NULL,
			ra bigint(20) NOT NULL,
			rs bigint(20) NOT NULL,
			rd bigint(20) NOT NULL,
			ard float NOT NULL,
			pt float NOT NULL,
			  seed bigint(20) NOT NULL,
			  u_identifier bigint(20) NOT NULL,
			  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			UNIQUE KEY u_identifier (u_identifier),
			PRIMARY KEY id (id)
		) $charset_collate;";
		$sqlScoreboard = "CREATE TABLE $pool_scoreboard (
			id bigint(20) NOT NULL,
		  	team_id bigint(20) NOT NULL,
		  	pool_id bigint(20) NOT NULL,
		  	wi bigint(20) NOT NULL,
			lo bigint(20) NOT NULL,
			tie bigint(20) NOT NULL,
			ra bigint(20) NOT NULL,
			rs bigint(20) NOT NULL,
			rd bigint(20) NOT NULL,
			ard float NOT NULL,
			pt float NOT NULL,
			  seed bigint(20) NOT NULL,
			  u_identifier bigint(20) NOT NULL,
			  time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			UNIQUE KEY u_identifier (u_identifier),
			PRIMARY KEY id (id)
		) $charset_collate;";
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		//dbDelta( $sqlPool );
		//dbDelta( $sqlSchedule );
		dbDelta( $sqlScoreboard );
	}

}

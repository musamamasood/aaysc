<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
class Aaysc_Tournament_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		global $wpdb;
		//$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "pools" );
		//$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "pool_schedule" );
		$wpdb->query( "DROP TABLE IF EXISTS " . $wpdb->prefix . "pool_scoreboard" );
		delete_option("aaysc_db_version");
	}

}

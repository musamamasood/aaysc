<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/include
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/include
 * @author     Masood U <masood.u@allshoreresources.com>
 */
class Aaysc_Tournament_Shortcode {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Aaysc_Tournament    The ID of this plugin.
	 */
	private $Aaysc_Tournament;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
	static private $states;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Aaysc_Tournament       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Aaysc_Tournament, $version ) {

		$this->Aaysc_Tournament = $Aaysc_Tournament;
		$this->version = $version;
		self::$states = array('AL'=>'Alabama','AK'=>'Alaska','AZ'=>'Arizona','AR'=>'Arkansas','CA'=>'California','CO'=>'Colorado','CT'=>'Connecticut','DE'=>'Delaware','DC'=>'District of Columbia','FL'=>'Florida','GA'=>'Georgia','HI'=>'Hawaii','ID'=>'Idaho','IL'=>'Illinois','IN'=>'Indiana','IA'=>'Iowa','KS'=>'Kansas','KY'=>'Kentucky','LA'=>'Louisiana','ME'=>'Maine','MD'=>'Maryland','MA'=>'Massachusetts','MI'=>'Michigan','MN'=>'Minnesota','MS'=>'Mississippi','MO'=>'Missouri','MT'=>'Montana','NE'=>'Nebraska','NV'=>'Nevada','NH'=>'New Hampshire','NJ'=>'New Jersey','NM'=>'New Mexico','NY'=>'New York','NC'=>'North Carolina','ND'=>'North Dakota','OH'=>'Ohio','OK'=>'Oklahoma','OR'=>'Oregon','PA'=>'Pennsylvania','RI'=>'Rhode Island','SC'=>'South Carolina','SD'=>'South Dakota','TN'=>'Tennessee','TX'=>'Texas','UT'=>'Utah','VT'=>'Vermont','VA'=>'Virginia','WA'=>'Washington','WV'=>'West Virginia','WI'=>'Wisconsin','WY'=>'Wyoming');
	}

	/*
	 * Coach registration login form
	 *
	 * @since 	1.5.0
	 * $param 	null
	 */
	public function aaysc_coach_registration_form(){
		//only show the registration form to non-logged-in member
		if ( !is_user_logged_in() ) {
			$output = $this->aaysc_registration_form_fields();
		}else{
			$user_id = get_current_user_id();
			$output  = $this->aaysc_registration_form_fields($user_id);
		}
		return $output;
	}

	/*
	 * Coach Registration form fields
	 *
	 * @since 	1.5.0
	 * $param   null
	 */
	public function aaysc_registration_form_fields($user_id = false){
		if($user_id){
			$coach = aaysc_tournament_common::get_coachdata($user_id);
		}
		ob_start(); ?>
			<div class="row" id="aaysc-coach-registration">
				<div class="col-sm-12">
					<h2>Coach Registration</h2>
					<hr>
					<?php aaysc_tournament_common::aaysc_show_error_messages(); ?>
					<form method="post" id="CreateCoachAccountForm" action="" novalidate="novalidate">
			            <div class="panel panel-tcs">
			                <div class="panel-body">
				                <div style="display:none;" id="errorMessageBox" class="alert validation-alert">
					               <h4>Please correct the following errors:</h4>
					               <ul id="errorList" style="display: none;"></ul>
					            </div>
			                    <div class="row">
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="FirstName">First Name*</label>
			                            <input type="text" value="<?=$coach->first_name;?>" placeholder="First Name" class="form-control" name="aaysc_user_first" id="FirstName">
			                        </div>
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="LastName">Last Name*</label>
			                            <input type="text" value="<?=$coach->last_name;?>" placeholder="Last Name" class="form-control" name="aaysc_user_last" id="LastName">
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="Address1">Address 1*</label>
			                            <input type="text" value="<?=$coach->address1;?>" placeholder="Address 1" class="form-control" name="aaysc_user_address1" id="Address1">
			                        </div>
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="Address2">Address 2</label>
			                            <input type="text" value="<?=$coach->address2;?>" placeholder="Address 2" class="form-control" name="aaysc_user_address2" id="Address2">
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="City">City*</label>
			                            <input type="text" value="<?=$coach->city;?>" placeholder="City" class="form-control" name="aaysc_user_city" id="City">
			                        </div>
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="state">State*</label>
			                            <select name="aaysc_user_state" id="State" class="form-control">
			                                <option selected="selected" disabled>Select State</option>
		                                	<?php foreach (self::$states as $key => $state): $sel = '';?>
		                                		<?php if($coach->state == $key){
		                                			$sel = ' selected="selected" ';
		                                		}?>
		                                		<option <?=$sel;?> value="<?=$key;?>"><?=$state;?></option>
		                                	<?php endforeach; ?>
			                            </select>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="Zip">Zip*</label>
			                            <input type="text" value="<?=$coach->zip_code;?>" placeholder="Zipcode" class="form-control" name="aaysc_user_zip" id="Zip">
			                        </div>
			                        <div class="col-md-6 control-group">
			                            <label class="control-label" for="MobilePhone">Mobile Phone*</label>
			                            <input type="text" value="<?=$coach->mob_phone;?>" placeholder="(xxx) xxx-xxxx" class="form-control phoneMask" name="aaysc_user_phone" id="MobilePhone">
			                        </div>
			                    </div>
			                    <div class="row-list">
			                        <h5>Login Information: </h5>
			                        <p class="text">Your Username will be your Email Address.  Please remember your Username and Password. You must have this information in order to access your account in the future.</p>
			                        <div class="row">
			                            <div class="col-md-6 control-group">
			                                <label class="control-label" for="UserName">User Name*</label>
			                                <input type="text" value="<?=$coach->user_login;?>" placeholder="Coach User Name" class="form-control" name="aaysc_user_login" id="UserName">
			                            </div>
			                            <div class="col-md-6 control-group">
			                                <label class="control-label" for="UserEmail">User Email*</label>
			                                <input type="text" value="<?=$coach->user_email;?>" placeholder="Coach User Email" class="form-control" name="aaysc_user_email" id="UserEmail">
			                            </div>
			                        </div>
			                        <div class="row">
			                            <div class="col-md-6 control-group">
			                                <label for="Password" class="control-label">Password*</label>
			                                <div class="controls">
			                                    <input type="password" placeholder="Password" name="aaysc_user_pass" id="Password" class="form-control">
			                                </div>
			                            </div>
			                            <div class="col-md-6 control-group">
			                                <label for="ConfirmPassword" class="control-label">Confirm Password*</label>
			                                <div class="controls">
			                                    <input type="password" placeholder="Confirm Password" name="aaysc_user_pass_confirm" id="ConfirmPassword" class="form-control">
			                                </div>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			            <div class="row">
		                    <div class="col-sm-6">
		                        <p class="text-muted"><small>*Required Field</small></p>
		                    </div>
		                    <div class="col-sm-6">
		                        <p class="text-right">
		                        	<input type="hidden" name="aaysc_register_nonce" value="<?=wp_create_nonce('aaysc-register-nonce'); ?>"/>
		                            <p class="form-submit text-right"><input type="submit" value="<?=($coach)?'Update':'Register';?>" class="submit" id="proceed" name="submit"> </p>
		                        </p>
		                    </div>
		                </div>
			        </form>
		        </div>
	        </div>
		<?php
		return ob_get_clean();
	}

	/*
	 * Registration modal for processing form.
	 *
	 * @since 	1.5.0
	 * $param   $role
	 */
	public function aaysc_add_new_memeber($role = 'coach'){
		if (isset( $_POST["aaysc_user_login"] ) && wp_verify_nonce($_POST['aaysc_register_nonce'], 'aaysc-register-nonce')) {
			aaysc_tournament_common::debug('-----aaysc-register-nonce verified-------');
			$user_login		= $_POST["aaysc_user_login"];
			$user_email		= $_POST["aaysc_user_email"];
			$user_first 	= $_POST["aaysc_user_first"];
			$user_last	 	= $_POST["aaysc_user_last"];
			$user_pass		= $_POST["aaysc_user_pass"];
			$pass_confirm 	= $_POST["aaysc_user_pass_confirm"];
			$user_address1	= $_POST["aaysc_user_address1"];
			$user_address2	= $_POST["aaysc_user_address2"];
			$user_city		= $_POST["aaysc_user_city"];
			$user_state		= $_POST["aaysc_user_state"];
			$user_zip		= $_POST["aaysc_user_zip"];
			$user_phone		= $_POST["aaysc_user_phone"];

			if(username_exists($user_login) && !is_user_logged_in()) {
				// Username already registered
				aaysc_tournament_common::aaysc_errors()->add('username_unavailable', __('Username already taken'));
			}
			if(!validate_username($user_login) && !is_user_logged_in()) {
				// invalid username
				aaysc_tournament_common::aaysc_errors()->add('username_invalid', __('Invalid username'));
			}
			if($user_login == '') {
				// empty username
				aaysc_tournament_common::aaysc_errors()->add('username_empty', __('Please enter a username'));
			}
			if(!is_email($user_email)) {
				//invalid email
				aaysc_tournament_common::aaysc_errors()->add('email_invalid', __('Invalid email'));
			}
			if(email_exists($user_email) && !is_user_logged_in()) {
				//Email address already registered
				aaysc_tournament_common::aaysc_errors()->add('email_used', __('Email already registered'));
			}
			if($user_pass == '') {
				// passwords do not match
				aaysc_tournament_common::aaysc_errors()->add('password_empty', __('Please enter a password'));
			}
			if($user_pass != $pass_confirm) {
				// passwords do not match
				aaysc_tournament_common::aaysc_errors()->add('password_mismatch', __('Passwords do not match'));
			}

			$errors = aaysc_tournament_common::aaysc_errors()->get_error_messages();
			// only create the user in if there are no errors
			if(empty($errors)) {
				if (is_user_logged_in()) {
					$user_id = get_current_user_id();
					$updated_user_id = wp_update_user(array(
							'ID'				=> $user_id,
							'user_pass'	 		=> $user_pass,
							'user_email'		=> $user_email,
							'first_name'		=> $user_first,
							'last_name'			=> $user_last,
							'user_registered'	=> date('Y-m-d H:i:s')
						)
					);
				}else{
					$new_user_id = wp_insert_user(array(
							'user_login'		=> $user_login,
							'user_pass'	 		=> $user_pass,
							'user_email'		=> $user_email,
							'first_name'		=> $user_first,
							'last_name'			=> $user_last,
							'user_registered'	=> date('Y-m-d H:i:s'),
							'role'				=> $role
						)
					);
				}
				update_user_meta( $new_user_id, 'address1', $user_address1 );
                update_user_meta( $new_user_id, 'address2', $user_address2 );
                update_user_meta( $new_user_id, 'city', $user_city );
                update_user_meta( $new_user_id, 'state', $user_state );
                update_user_meta( $new_user_id, 'zip_code', $user_zip );
                update_user_meta( $new_user_id, 'mob_phone', $user_phone );
                aaysc_tournament_common::debug("-----aaysc-user-inert-------$new_user_id");
				if($new_user_id) {
					// send an email to the admin alerting them of the registration
					wp_new_user_notification($new_user_id);
				    // @mail(get_option('admin_email'), 'New Team Registration!', "New team ( $team->team_name ) has been registered successfully.");
					$subj = 'New Coach Registration!';
					$body = "New Coach ( $user_first $user_last) has been registered successfully.";
					wp_mail( get_option('admin_email'), $subj, $body );
					// log the new user in
					wp_setcookie($user_login, $user_pass, true);
					$user = wp_set_current_user($new_user_id, $user_login);

					do_action('wp_login', $user_login, $user);
				}elseif ($updated_user_id) {
					wp_setcookie($user_login, $user_pass, true);
					$user = wp_set_current_user($updated_user_id, $user_login);

					do_action('wp_login', $user_login, $user);
				}
				wp_redirect(home_url()); exit;
			}

		}
	}
}

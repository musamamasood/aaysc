(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-specific JavaScript source
	 * should reside in this file.
	 *
	 * Note that this assume you're going to use jQuery, so it prepares
	 * the $ function reference to be used within the scope of this
	 * function.
	 *
	 * From here, you're able to define handlers for when the DOM is
	 * ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * Or when the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and so on.
	 *
	 * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
	 * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
	 * be doing this, we should try to minimize doing that in our own work.
	 */
	$(document).ready(function(){
		$('#prepare-pool').click(function(){
			var data = {
				'action': 'prepare_pool',
				't_id': aaysc.t_id,
				'pools': $('.pools-list').val(),
			}
			$.post(aaysc.ajaxurl, data, function(response){
				console.log('Got this from the server: ' + response);
				window.location.reload(true);
			});
			return false;
		});
		$('#publish-pool').click(function(){
			var data = {
				'action': 'publish_pool',
				't_id': aaysc.t_id,
				'pools': $('.pools-list').val(),
			}
			$.post(aaysc.ajaxurl, data, function(response){
				console.log('Got this from the server: ' + response);
				window.location.reload(true);
			});
			return false;
		});
		//Bracket section.
		$('#prepare-bracket').click(function(){
			var data = {
				'action': 'prepare_bracket',
				't_id': aaysc.t_id,
				'nonce': aaysc.nonce,
				'brackets': $('.bracket-list').val(),
			}
			$.post(aaysc.ajaxurl, data, function(response){
				console.log('Got this from the server: ' + response);
				window.location.reload(true);
			});
			return false;
		});
		$('#publish-bracket').click(function(){
			var data = {
				'action': 'publish_bracket',
				't_id': aaysc.t_id,
				'nonce': aaysc.nonce,
				'brackets': $('.bracket-list').val(),
			}
			$.post(aaysc.ajaxurl, data, function(response){
				console.log('Got this from the server: ' + response);
				window.location.reload(true);
			});
			return false;
		});
		$('.delete-registration').click(function(event){
			if(confirm("Are you sure you want to delete this?")){
		        var data = {
					'action': 'delete_registration',
					't_id': aaysc.t_id,
					'id': $(this).attr('id'),
				}
				$.post(aaysc.ajaxurl, data, function(response){
					//console.log('Got this from the server: ' + response);
					window.location.reload(true);
				});
		    }
		    else{
		        event.preventDefault();
		    }
		});
		$('.datetimepicker').datetimepicker({
		  format:'Y-m-d H:i:s',
		});
	});

})( jQuery );

function open_popup(id){
	jQuery(document).ready(function(){
		jQuery(".overlay").show();
		jQuery( "#dialog_" + id ).dialog({
			minWidth: 900,
			dialogClass: "wp-dialog",
			close: function( event, ui ) { jQuery(".overlay").hide();  }
		});
	});
}

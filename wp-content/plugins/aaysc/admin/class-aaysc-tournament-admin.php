<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/admin
 * @author     Your Name <email@example.com>
 */
class Aaysc_Tournament_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Aaysc_Tournament    The ID of this plugin.
	 */
	private $Aaysc_Tournament;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version, $db, $tb_pools, $tb_pool_schedule, $tb_brackets, $tb_bracket_schedule, $tb_bracket_scoreboard, $tb_t_registration, $tId, $post, $is_tournament;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Aaysc_Tournament       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Aaysc_Tournament, $version ) {

		global $wpdb;
		global $post;
		$this->Aaysc_Tournament = $Aaysc_Tournament;
		$this->version = $version;
		$this->db = $wpdb;
		$this->post = $post;
		$this->tId =	(isset($_GET['post'])) ? $_GET['post'] : '';
		$this->tb_pools = $this->db->prefix.'pools';
		$this->tb_pool_schedule = $this->db->prefix.'pool_schedule';
		$this->tb_pool_scoreboard = $this->db->prefix.'pool_scoreboard';
		$this->tb_brackets = $this->db->prefix.'brackets';
		$this->tb_bracket_schedule = $this->db->prefix.'bracket_schedule';
		$this->tb_bracket_scoreboard = $this->db->prefix.'bracket_scoreboard';
		$this->tb_t_registration = $this->db->prefix.'t_registration';
		$this->tournament_registered();
		$this->is_tournament = aaysc_tournament_common::is_tournament();
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aaysc_Tournament_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aaysc_Tournament_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Aaysc_Tournament, plugin_dir_url( __FILE__ ) . 'css/aaysc-tournament-admin.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'datetimepicker', plugin_dir_url( __FILE__ ) . 'css/jquery.datetimepicker.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aaysc_Tournament_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aaysc_Tournament_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		global $post;
		$tid = ( isset($post->ID) )? $post->ID : null;
		$aaysc = array(
			't_id' 		=> $tid,
			'nonce' 	=> wp_create_nonce( 'aaysc-backend' ),
			'admin_url' => admin_url(),
			'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
		);
		wp_enqueue_script( $this->Aaysc_Tournament, plugin_dir_url( __FILE__ ) . 'js/aaysc-tournament-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'datetimepicker', plugin_dir_url( __FILE__ ) . 'js/jquery.datetimepicker.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'jquery-ui-dialog' );
		wp_enqueue_style( 'wp-jquery-ui-dialog' );
		wp_localize_script( $this->Aaysc_Tournament, 'aaysc', $aaysc );
	}
	// Registered New Tournament Types
	private function tournament_registered(){
		if(class_exists('CPT')){
			// basketball post type
			$basketball = new CPT(array(
			    'post_type_name' => 'basketball',
			    'singular' => 'Basketball',
			    'plural' => 'Basketball',
			    'slug' => 'basketball',
			), array(
				'capability_type' => 'page',
				'hierarchical' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-admin-site',
			    'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
			));
			$basketball->columns(array(
			    'cb' => '<input type="checkbox" />',
			    'title' => __('Title'),
			    'post_id' => __('Tournament ID'),
			    'author' => __('Author'),
			    'icon' => __('Thumbnail'),
			    'date' => __('Date')
			));
			// added division in tournamet
			$basketball->register_taxonomy(array(
			    'taxonomy_name' => 'age_groups',
			    'singular' => 'Division',
			    'plural' => 'Divisions',
			    'slug' => 'age_groups'
			), array(
				'hierarchical' => true,
			));
			// added state in tournamet
			$basketball->register_taxonomy(array(
			    'taxonomy_name' => 'state',
			    'singular' => 'State',
			    'plural' => 'States',
			    'slug' => 'state'
			), array(
				'hierarchical' => true,
			));

			// basketball post type
			$baseball = new CPT(array(
			    'post_type_name' => 'event',
			    'singular' => 'Baseball',
			    'plural' => 'Baseball',
			    'slug' => 'event',
			), array(
				'capability_type' => 'page',
				'hierarchical' => true,
				'has_archive' => true,
				'menu_icon' => 'dashicons-admin-site',
			    'supports' => array('title', 'editor', 'thumbnail', 'page-attributes')
			));
			$baseball->columns(array(
			    'cb' => '<input type="checkbox" />',
			    'title' => __('Title'),
			    'post_id' => __('Tournament ID'),
			    'author' => __('Author'),
			    'icon' => __('Thumbnail'),
			    'date' => __('Date')
			));
			// added division in tournamet
			// added state in tournamet
			$baseball->register_taxonomy(array(
			    'taxonomy_name' => 'age_groups',
			    'singular' => 'Division',
			    'plural' => 'Divisions',
			    'slug' => 'age_groups'
			), array(
				'hierarchical' => true,
			));
			$baseball->register_taxonomy(array(
			    'taxonomy_name' => 'state',
			    'singular' => 'State',
			    'plural' => 'States',
			    'slug' => 'state'
			), array(
				'hierarchical' => true,
			));
			// team registration
			$teams = new CPT(array(
			    'post_type_name' => 'team',
			    'singular' => 'Team',
			    'plural' => 'Teams',
			    'slug' => 'team',
			), array(
				'public' => true,
				'publicly_queryable' => true,
				'show_ui'=> true,
				'query_var' => true,
				'menu_icon' => 'dashicons-groups',
				'rewrite'=> true,
				'capability_type' => 'post',
				'hierarchical' => false,
				'menu_position' => null,
				'supports' => array('title','editor','thumbnail','author')
			));
		}
	}

	#added by Masood : 2015-08-06
	public static function tournament_add_children( $post_id, $post ) {
        $tounament_type = get_post_type( $post_id );
        if( 0 == $post->post_parent ){
            $children =& get_children(
                array(
                    'post_parent' => $post_id,
                    'post_type' => $tounament_type
                )
            );
            if( empty( $children ) ){ //POOL SCHEDULE / GAMEDAY
            	$titles = array('Pool Schedule / Gameday', 'Championship Play', 'Tournament Results / Scoreboard', 'Who\'s Coming', 'Facilities & Directions', 'Itinerary', 'Tournament Rules', 'Tournament History');
                foreach ($titles as $title) {
                	$child = array(
                		'post_type' 	=> $tounament_type,
                		'post_title' 	=> $title,
                    	'post_content' 	=> '',
                    	'post_status' 	=> 'publish',
                    	'post_parent' 	=> $post_id,
                    	'post_author' 	=> 1
                    );
                	$pid = wp_insert_post( $child );
                	// Add template meta for page
                	if ( ! add_post_meta( $pid, '_wp_page_template', 'first-time-user-template.php', true ) ) {
							update_post_meta ( $pid, '_wp_page_template', 'first-time-user-template.php' );
					}
                }
			global $wp_rewrite;
			$wp_rewrite->flush_rules( false );
            }
        }
	}

	public function get_terms_filter( $terms, $taxonomies, $args ){
		global $post_type;
		global $pagenow;
		if ( $pagenow != 'edit-tags.php' ||  $taxonomies[0] != 'age_groups') return $terms;
		if ( ! is_array($terms) && count($terms) < 1 ) return $terms;
		if ( ! is_admin() ) return $terms;
		$filtered_terms = array();
		foreach ( $terms as $term ){
			if ( get_field('division_type', $term) == 'baseball' && $post_type == 'event' ) {
				$filtered_terms[] = $term;
			}elseif ( get_field('division_type', $term) == 'basketball' && $post_type == 'basketball') {
				$filtered_terms[] = $term;
			}
		}
		if (in_array($post_type, $this->is_tournament) === true) return $filtered_terms;
		else return $terms;
	}

	public function prepare_pool(){
		//global $wpdb;
		$poolData  = ($_POST['pools']) ? $_POST['pools']: false;
		$poolsList = explode(',', $poolData);
		$tId = ($_POST['t_id']) ? $_POST['t_id']: false;
		// remove the old record. $tId
		$poolExit = $this->db->get_results("SELECT * FROM $this->tb_pools where tid = '$tId'", ARRAY_A);
		if($poolExit){
			$this->db->delete( $this->$tb_pools, array( 'tid' => $tId ) );
			foreach ($poolExit as $key => $p) {
				$this->db->delete( $this->tb_pool_schedule, array( 'pool_id' => $p[id] ) );
			}
		}
		$startDate 	= new DateTime( get_field('start_date', $tId) );
		$startDate->modify('+1 day');
		$startDate->modify('+6 hour');
		foreach ($poolsList as $pool) {
			$title = explode('_', $pool);
			$this->db->insert($this->tb_pools,
			    array(
			      'title'  	  => $title[0].$title[3],
				  'age_group' => $pool,
				  'tid' 	  => $tId,
		    	)
		  	);
		  	$id = $this->db->insert_id;
 			// -----
   			$teams = $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE age='$pool' and t_id = '$tId' order by rank_id", ARRAY_A);
 			$teamPools = array();
 			#return false if team is 0 or less then 3
 			if ( ($total = $this->db->num_rows) == 0 && $total < 3) return false;
 			// check team is 3
 			if( $total == 3) {
 				#When teams are equal to 3
 				// $pool1 = $teams;
 				// $pool2 = array_chunk($teams, $total / 2);

 				// for($i=0; $i < $total/2; $i++){
 				// 	if( ($t1 = array_shift($pool1)) && ($t2 = array_pop($pool1)) ){
 				// 		$startDate->modify('+2 hour');
					// 	$date  = $startDate->format('Y-m-d H:i:s');
 				// 		$teamPools[] = array($t1[team_id], $t2[team_id], $id, $date);
 				// 	}
 				// }
 				// for($i=0; $i < $total/2; $i++){
 				// 	if( ($l1 = array_shift($pool2[0])) && ($l2 = array_shift($pool2[1])) ){
 				// 		$startDate->modify('+2 hour');
					// 	$date  = $startDate->format('Y-m-d H:i:s');
 				// 		$teamPools[] = array($l1[team_id], $l2[team_id], $id, $date);
 				// 	}
 				// }
 			}elseif( $total == 4) {
 				#When teams are equal to 4
 				$pool1 = array_splice($teams,(count($teams)/2));
			    $pool2 = $teams;
			    for ($i=0; $i < count($pool2)+count($pool1)-1; $i++){
			        for ($j=0; $j<count($pool2); $j++){
			            $startDate->modify('+2 hour');
						$date  = $startDate->format('Y-m-d H:i:s');
 						$teamPools[] = array($pool2[$j][team_id], $pool1[$j][team_id], $id, $date);
			        }
			        if(count($pool2)+count($pool1)-1 > 2){
			            array_unshift($pool1,array_shift(array_splice($pool2,1,1)));
			            array_push($pool2,array_pop($pool1));
			        }
			    }
			    # create championship match with TBD.
			    //$teamPools[] = array('-1', '-1', $id, $date);
 			}elseif( $total >= 5) {
 				#When teams are more/equal to 5
 				if( $total % 2 == 0) {
	 				$pool1 = $teams;
	 				$pool2 = array_chunk($teams, $total / 2);

	 				for($i=0; $i < $total/2; $i++){
	 					if( ($t1 = array_shift($pool1)) && ($t2 = array_pop($pool1)) ){
	 						$startDate->modify('+2 hour');
							$date  = $startDate->format('Y-m-d H:i:s');
	 						$teamPools[] = array($t1[team_id], $t2[team_id], $id, $date);
	 					}
	 				}
	 				for($i=0; $i < $total/2; $i++){
	 					if( ($l1 = array_shift($pool2[0])) && ($l2 = array_shift($pool2[1])) ){
	 						$startDate->modify('+2 hour');
							$date  = $startDate->format('Y-m-d H:i:s');
	 						$teamPools[] = array($l1[team_id], $l2[team_id], $id, $date);
	 					}
	 				}
	 			}else{
	 				$pool1 = $teams;
	 				$pool2 = array_chunk($teams, $total / 2);

	 				for($i=0; $i < $total/2; $i++){
	 					if( ($t1 = array_shift($pool1)) && ($t2 = array_pop($pool1))){
	 						$startDate->modify('+2 hour');
							$date  = $startDate->format('Y-m-d H:i:s');
	 						$teamPools[] = array($t1[team_id], $t2[team_id], $id, $date);
	 					}
	 				}
	 				for($i=0; $i < $total/2; $i++){
	 					if( ($l1 = array_shift($pool2[0])) && ($l2 = array_pop($pool2[1])) ){
	 						$startDate->modify('+2 hour');
							$date  = $startDate->format('Y-m-d H:i:s');
	 						$teamPools[] = array($l1[team_id], $l2[team_id], $id, $date);
	 					}
	 				}
	 				$startDate->modify('+2 hour');
					$date  = $startDate->format('Y-m-d H:i:s');
	 				$teamPools[] = array($t1[team_id], $pool2[2][0][team_id], $id, $date);
	 			}
	 		}
 			foreach ($teamPools as $teamPool) {
 				$this->db->insert($this->tb_pool_schedule,
				    array(
				      'team_a_id'  	  	=> $teamPool[0],
					  'team_b_id' 		=> $teamPool[1],
					  'team_a_score' 	=> 0,
					  'team_b_score' 	=> 0,
					  'pool_id' 	  	=> $teamPool[2],
					  'time' 	  		=> $teamPool[3],
			    	)
			  	);
 			}
		}
		var_dump($pool1);
		wp_die();
	}

	// Publish Pool Data
	public function publish_pool(){
		$poolData  = ($_POST['pools']) ? $_POST['pools']: false;
		$poolsList = explode(',', $poolData);
		$tId = ($_POST['t_id']) ? $_POST['t_id']: false;
		foreach ($poolsList as $pool) {
			$this->db->update($this->db->prefix.'pools',
				array(
					'status' => '1'
				),
				array( 'age_group' => $pool, 'tid' => $tId )
			);
		}
		$allTeams = $this->db->get_results("SELECT distinct team_b_id as team_id, pool_id FROM $this->tb_pool_schedule as a1 inner join $this->tb_pools as ps on ps.id=a1.pool_id where ps.tid = $tId UNION SELECT distinct team_a_id as team_id, pool_id FROM $this->tb_pool_schedule as a2 inner join $this->tb_pools as ps on ps.id=a2.pool_id where ps.tid = $tId order by pool_id", OBJECT);
		print_r($allTeams);
		foreach ($allTeams as $team) {
			if ($team->team_id == '-1') continue;
			$this->db->insert($this->tb_pool_scoreboard,
				array('team_id'=>$team->team_id,'pool_id'=>$team->pool_id, 'wi'=> 0, 'lo'=> 0, 'tie'=> 0, 'ra'=>0,'rs'=> 0,'rd'=> 0,'ard'=> 0,'pt'=> 0,'seed'=> 0,'u_identifier'=>$team->team_id.$team->pool_id),
				array('%d','%d','%d','%d','%d','%d','%d','%d','%f','%f','%d','%d')
			);
		}
	}

	#prepare brackets
	public function prepare_bracket(){
		$poolData  = ($_POST['brackets']) ? $_POST['brackets']: false;
		$poolsList = explode(',', $poolData);
		$tId = ($_POST['t_id']) ? $_POST['t_id']: false;
		// remove the old record. $tId
		$poolExit = $this->db->get_results("SELECT * FROM $this->tb_brackets where tid = '$tId'", ARRAY_A);
		if($poolExit){
			$this->db->delete( $this->tb_brackets, array( 'tid' => $tId ) );
			foreach ($poolExit as $key => $p) {
				$this->db->delete( $this->tb_bracket_schedule, array( 'bracket_id' => $p[id] ) );
			}
			// $this->db->show_errors();
			// $this->db->print_error();
		}
		$startDate 	= new DateTime( get_field('start_date', $tId) );
		$startDate->modify('+1 day');
		$startDate->modify('+6 hour');
		// further work to add title
		foreach ($poolsList as $pool) {
 			// -----
   			$teams = $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE age='$pool' and t_id = '$tId' order by rank_id", ARRAY_A);
 			$teamPools = array();
 			#return false if team is 0 or less then 3
 			if ( ($total = $this->db->num_rows) == 0 && $total < 3) return false;

 			$title = explode('_', $pool);
			$this->db->insert($this->tb_brackets,
			    array(
				  'tid' 	  => $tId,
				  'age_group' => $pool,
				  'status'    => 0,
			      //'title'  	  => $title[0].$title[3],
		    	)
		  	);
		  	$id = $this->db->insert_id;
 			// check team is 3
 			if( $total >= 5) {
 				#When teams are more/equal to 5
 				// if( $total % 2 == 0) {
	 			// 	$pool1 = $teams;
	 			// 	$pool2 = array_chunk($teams, $total / 2);

	 			// 	for($i=0; $i < $total/2; $i++){
	 			// 		if( ($t1 = array_shift($pool1)) && ($t2 = array_pop($pool1)) ){
	 			// 			$startDate->modify('+2 hour');
					// 		$date  = $startDate->format('Y-m-d H:i:s');
	 			// 			$teamPools[] = array($t1[team_id], $t2[team_id], $id, $date);
	 			// 		}
	 			// 	}
	 			// 	for($i=0; $i < $total/2; $i++){
	 			// 		if( ($l1 = array_shift($pool2[0])) && ($l2 = array_shift($pool2[1])) ){
	 			// 			$startDate->modify('+2 hour');
					// 		$date  = $startDate->format('Y-m-d H:i:s');
	 			// 			$teamPools[] = array($l1[team_id], $l2[team_id], $id, $date);
	 			// 		}
	 			// 	}
	 			// }else{
	 			// 	$pool1 = $teams;
	 			// 	$pool2 = array_chunk($teams, $total / 2);

	 			// 	for($i=0; $i < $total/2; $i++){
	 			// 		if( ($t1 = array_shift($pool1)) && ($t2 = array_pop($pool1))){
	 			// 			$startDate->modify('+2 hour');
					// 		$date  = $startDate->format('Y-m-d H:i:s');
	 			// 			$teamPools[] = array($t1[team_id], $t2[team_id], $id, $date);
	 			// 		}
	 			// 	}
	 			// 	for($i=0; $i < $total/2; $i++){
	 			// 		if( ($l1 = array_shift($pool2[0])) && ($l2 = array_pop($pool2[1])) ){
	 			// 			$startDate->modify('+2 hour');
					// 		$date  = $startDate->format('Y-m-d H:i:s');
	 			// 			$teamPools[] = array($l1[team_id], $l2[team_id], $id, $date);
	 			// 		}
	 			// 	}
	 			// 	$startDate->modify('+2 hour');
					// $date  = $startDate->format('Y-m-d H:i:s');
	 			// 	$teamPools[] = array($t1[team_id], $pool2[2][0][team_id], $id, $date);
	 			// }
	 			$date  = $startDate->format('Y-m-d H:i:s');
	 			$teamPools[] = array('-1', '-1', $id, $date, '1');
	 			$teamPools[] = array('-1', '-1', $id, $date, '2');
	 			$teamPools[] = array('-1', '-1', $id, $date, '2');
	 			$teamPools[] = array('-1', '-1', $id, $date, '3');
	 		}elseif( $total <= 4 && $total >=3 ) {
	 			// championship match when teams are 3 or 4.
	 			$date  		 = $startDate->format('Y-m-d H:i:s');
	 			$teamPools[] = array('-1', '-1', $id, $date, '1');
	 		}
 			foreach ($teamPools as $teamPool) {
 				$this->db->insert($this->tb_bracket_schedule,
				    array(
				      'team_a_id'  	  	=> $teamPool[0],
					  'team_b_id' 		=> $teamPool[1],
					  'team_a_score' 	=> 0,
					  'team_b_score' 	=> 0,
					  'bracket_id' 	  	=> $teamPool[2],
					  'time' 	  		=> $teamPool[3],
					  'level' 	  		=> $teamPool[4],
			    	)
			  	);
 			}
		}
		var_dump($pool);
		wp_die();
	}

	// Publish bracket data
	public function publish_bracket(){
		$poolData  = ($_POST['brackets']) ? $_POST['brackets']: false;
		$poolsList = explode(',', $poolData);
		$tId 	   = ($_POST['t_id']) ? $_POST['t_id']: false;
		$nonce 	   = ($_POST['nonce']) ? $_POST['nonce']: false;
		if ( ! wp_verify_nonce( $nonce, 'aaysc-backend' ) ) return false;
		// Publish bracket championship matches.
		foreach ($poolsList as $pool) {
			$this->db->update($this->tb_brackets,
				array(
					'status' => '1'
				),
				array( 'age_group' => $pool, 'tid' => $tId )
			);
		}

		$allTeams = $this->db->get_results("SELECT distinct team_b_id as team_id, bracket_id FROM $this->tb_bracket_schedule as a1 inner join $this->tb_brackets as bs on bs.id=a1.bracket_id where bs.tid = $tId UNION SELECT distinct team_a_id as team_id, bracket_id FROM $this->tb_bracket_schedule as a2 inner join $this->tb_brackets as bs on bs.id=a2.bracket_id where bs.tid = $tId order by bracket_id", OBJECT);

		foreach ($allTeams as $team) {
			if ($team->team_id == '-1') continue;
			$this->db->insert($this->tb_bracket_scoreboard,
				array('team_id'=>$team->team_id,'bracket_id'=>$team->bracket_id, 'wi'=> 0, 'lo'=> 0, 'ra'=>0,'rs'=> 0,'rd'=> 0,'ard'=> 0, 'u_identifier'=>$team->team_id.$team->bracket_id),
				array('%d','%d','%d','%d','%d','%d','%d','%f','%d')
			);
			$this->db->show_errors();
		}
	}

	// Metabox for event pools
	public function aaysc_add_meta_box($post_type, $post) {
		if( $post->post_parent ) return;
		if( in_array($post_type, $this->is_tournament) === false) return;
		$screens = array( $post_type );
		foreach ( $screens as $screen ) {
			add_meta_box(
				'register_teams',
				'Registered Teams',
				array($this, 'meta_box_registered_teams'),
				$screen
			);
			add_meta_box(
				'age-groups',
				'Age Groups',
				array($this, 'meta_box_age_group'),
				$screen
			);
			add_meta_box(
				'pool_schedule_id',
				'Pool Schedule',
				array($this, 'meta_box_pool_items'),
				$screen
			);
			add_meta_box(
				'pool_scoreboard',
				'Pool Scoreboard',
				array($this, 'meta_box_pool_scoreboard'),
				$screen
			);
			add_meta_box(
				'pool_bracket_id',
				'Bracket Schedule',
				array($this, 'meta_box_bracket_items'),
				$screen
			);
		}
	}

	public function meta_box_registered_teams(){
			$age_groups = 	array();
			$term_children = aaysc_tournament_common::term_children();
			$registered_teams = $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE t_id='$this->tId' order by cast(age as UNSIGNED)", OBJECT);
			if ( count( $registered_teams ) == 0 ){
				echo 'No team register yet!';
			} else {
				foreach ($registered_teams as $value):
            		if ( !in_array($value->age, $age_groups) ) {
            			$age_groups[] = $value->age;
            		}
            	endforeach;
            ?>
            <table class="wp-list-table widefat fixed striped" width="100%">
                <tr style="background-color:#2EA2CC;color:#FFF;height:35px;">
                    <td><span class="box1">#</span></td>
                    <td><span class="box1">Team</span></td>
                    <td><span class="box1">Coach</span></td>
                    <td><span class="box1">P</span></td>
                    <td><span class="box1">Payment Status</span></td>
                    <td><span class="box1">Status</span></td>
                    <td><span class="box1">Action</span></td>
                </tr>
            	<input type="hidden" name="pools" class="pools-list" value="<?=implode(',', $age_groups);?>">
            	<?php
            	foreach ($age_groups as $value): $counter = 0;
				foreach( $registered_teams as $rt ):
					if($value == $rt->age): $counter++;
						if($counter == 1){?>
							<tr style="background-color:#F1F1F1;color:#000;"><td colspan="7">
							<?php echo $rt->age; ?>
							<input name="save" type="submit" style="float: right;" class="button button-primary button-large" id="publish" value="Update">
							</td></tr>
                		<?php } ?>
                <tr style="color:#000;">
                    <td><input name="rt[<?=$rt->id;?>][rank_id]" value="<?=$rt->rank_id;?>" size="5"></td>
                    <td><a href="javascript:void(0);" onclick="return open_popup('<?=$rt->team_id;?>');" ><?=$rt->team_name; ?></a></td>
                    <td><?=$rt->coach_name; ?></td>
                    <td><select style="width: 100%;" name="rt[<?=$rt->id;?>][age]">
                    		<option disabled selected>Select Age Group</option>
                        	<?php foreach ($term_children as $v) {
                        		$cleanName = aaysc_tournament_common::clean_age_group( $v->name ); ?>
                    		<option <?=($cleanName == $rt->age)?'selected':'';?> value="<?=$cleanName;?>"><?=$v->name;?></option>
                    	<?php } ?>
                    	</select></td>
                    <td><?=($rt->payment_status == 1 )?"Paid":"Pending";?></td>
                    <td><input type="checkbox" name="rt[<?=$rt->id;?>][payment]" <?=($rt->payment_status == 1)?"checked":'';?> value="<?=$rt->team_id ?>" /> Mark Paid</td>
                    <td><button id="<?=$rt->id;?>" class="delete-registration button button-danger button-large">Trash</button></td>
				</tr>
				<?php endif; endforeach; endforeach; ?>
				<tr>
					<td colspan="7"><button style="float: right;margin-left: 10px;" class="button button-primary button-large" id="publish-pool">Publish</button>
					<button style="float: right;" class="button button-primary button-large" id="prepare-pool">Prepare</button></td>
				</tr>
            </table>
            <div class="overlay"></div>
            <?php }
			if( $registered_teams ) {
				foreach( $registered_teams as $registered_team ) { ?>
                <div id="dialog_<?=$registered_team->team_id; ?>" title="<?php echo $registered_team->team_name; ?>'s Detail" class="popup_class">
                 	<table class="wp-list-table widefat fixed striped" width="100%">
                        <tr style="background-color:#2EA2CC;color:#FFF;height:35px;">
                             <td><span class="box1">Address</span></td>
                             <td><span class="box1">City</span></td>
                             <td><span class="box1">State</span></td>
                             <td><span class="box1">Zip code</span></td>
                             <td><span class="box1">Mobile</span></td>
                        </tr>
                        <tr style="color:#000;">
                             <td><span class="box"><?php echo get_user_meta( $registered_team->user_id , 'address1', true ); ?></span></td>
                             <td><span class="box"><?php echo get_user_meta( $registered_team->user_id , 'city', true ); ?></span></td>
                             <td><span class="box"><?php echo get_user_meta( $registered_team->user_id , 'state', true ); ?></span></td>
                             <td><span class="box"><?php echo get_user_meta( $registered_team->user_id , 'zip_code', true ); ?></span></td>
                             <td><span class="box"><?php echo get_user_meta( $registered_team->user_id , 'mob_phone', true ); ?></span></td>
                        </tr>
                 	</table>
                </div>
			<?php } } ?>
	<?php
	}
	public function meta_box_pool_items(){
		$tid		=	$_GET['post'];
		$age_groups = 	array();
		$schedule_teams = $this->db->get_results("SELECT p.title, p.age_group, p.location, ps.* FROM $this->tb_pool_schedule as ps INNER JOIN $this->tb_pools as p on p.id = ps.pool_id and p.tid = '$tid' order by cast(p.age_group as UNSIGNED), ps.id", OBJECT);
		$teams 		= $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE t_id = '$tid' OR t_id = '-1'", OBJECT);
		if ( count( $schedule_teams ) == 0 ){
			echo 'No Pool Schedule Yet!';
		} else {
			?>
            <table class="wp-list-table widefat fixed striped" width="100%">
                <tr style="background-color:#2EA2CC;color:#FFF;height:35px;">
                    <td width="20"><span class="box1">#</span></td>
                    <td width="50"><span class="box1">Title</span></td>
                    <td><span class="box1">Team A</span></td>
                    <td width="50"><span class="box1">Score</span></td>
                    <td><span class="box1">Team B</span></td>
                    <td width="50"><span class="box1">Score</span></td>
                    <td><span class="box1">Location</span></td>
                    <td><span class="box1">Time</span></td>
                </tr>
                <?php
                	foreach ($schedule_teams as $value):
                		if ($value->age_group == 'TBD') continue;
                		if ( !in_array($value->age_group, $age_groups) ) {
                			$age_groups[] = $value->age_group; }
                	endforeach;
                	foreach ($age_groups as $value) { $counter = 0;
						foreach( $schedule_teams as $st ) {
							if($value == $st->age_group): $counter++;
								if($counter == 1){?>
									<tr style="background-color:#F1F1F1;color:#000;"><td colspan="8">
									<?=$st->age_group;?>
									<input name="save" type="submit" style="float: right;" class="button button-primary button-large" id="publish" value="Update"></td></tr>
	                    	<?php } ?>
	                    <input type="hidden" name="brackets" class="bracket-list" value="<?=implode(',', $age_groups);?>">
	                    <tr style="color:#000;">
	                        <td><?=$st->id;?></td>
	                        <td><?=$st->title;?></td>
	                        <td><select style="width: 100%;" name="st[<?=$st->id;?>][teama]" autocomplete="off">
	                        	<?php foreach ($teams as $t) { ?>
		                        	<?php if($t->age == $st->age_group OR $t->age == 'TBD'): ?>
		                        		<option <?=($t->team_id == $st->team_a_id)?'selected="selected"':'';?> value="<?=$t->team_id;?>"><?=Aaysc_Tournament_Common::getTeamData($t->team_id)->team_name;?></option>
		                        	<?php endif; ?>
	                        	<?php } ?>
	                        	</select>
	                        </td>
	                        <td><?=$st->team_a_score; ?></td>
	                        <td><select style="width: 100%;" name="st[<?=$st->id;?>][teamb]" autocomplete="off">
	                        	<?php foreach ($teams as $t) { ?>
		                        	<?php if($t->age == $st->age_group OR $t->age == 'TBD'): ?>
		                        		<option <?=($t->team_id == $st->team_b_id)?'selected="selected"':'';?> value="<?=$t->team_id;?>"><?=Aaysc_Tournament_Common::getTeamData($t->team_id)->team_name;?></option>
		                        	<?php endif; ?>
	                        	<?php } ?>
	                        	</select>
	                        </td>
	                        <td><?=$st->team_b_score; ?></td>
	                        <td><input type="text" name="st[<?=$st->id;?>][location]" value="<?=$st->location;?>" style="width: 100%;"></td>
	                        <td><input type="text" name="st[<?=$st->id;?>][time]" value="<?=$st->time;?>" style="width: 100%;" class="datetimepicker"></td>
						</tr>
					<?php endif;
                	}
                }
				?>
				<tr>
					<td colspan="8"><button style="float: right;margin-left: 10px;" class="button button-primary button-large" id="publish-bracket">Publish Bracket</button><button style="float: right;" class="button button-primary button-large" id="prepare-bracket">Prepare Bracket</button></td>
				</tr>
            </table>
	<?php
		}
	}

	public function meta_box_age_group(){
		global $post_type;
		$groups = get_terms( 'age_groups', 'orderby=count&hide_empty=0&parent=0' );
		foreach( $groups as $age){
			if ( get_field('division_type', $age) == 'baseball' && $post_type == 'event' ) {
				$is_exist	=	get_post_meta( $_GET['post'], 'age_group_parent_' . $age->term_id, true );
				$checked	=	'';
				$value		=	'';
				if( $is_exist != '' ){
					$checked	=	"checked=checked";
					$value	=	$is_exist;
				} ?>
				<input type="checkbox" name="parent_<?=$age->term_id;?>" <?=$checked;?> id="parent_<?=$age->term_id;?>"/>
				<?=$age->name;?>:
				<input type="text" name="parent_price_<?=$age->term_id;?>" placeholder="Price" value="<?=$value;?>" style="width: 30% !important;" />
				<?php
				$groups_child = get_terms( 'age_groups', "orderby=count&hide_empty=0&parent=" . $age->term_id );
				foreach($groups_child as $child ){
					$is_exist_child	=	get_post_meta( $_GET['post'], 'age_group_child_' . $age->term_id . '_' . $child->term_id , true );
					$checked_child	=	'';
					$value_child	=	'';
					if( $is_exist_child != '' ){
						$checked_child	=	"checked=checked";
						$value_child	=	$is_exist_child;
					} ?>
					<br /><br />
					<input style="margin-left:15px;" type="checkbox" <?php echo $checked_child; ?> name="child_<?php echo $age->term_id; ?>_<?php echo $child->term_id; ?>" onclick="return checkParent('<?php echo $age->term_id;  ?>');" />
					<?php echo $child->name; ?>
					<input type="text" name="child_price_<?php echo $child->term_id; ?>" value="<?php echo $value_child;?>" placeholder="Price" style="width: 30% !important;" />
					<?php
				}
				echo "<br /><br />";
			}elseif ( get_field('division_type', $age) == 'basketball' && $post_type == 'basketball' ) {
				$is_exist	=	get_post_meta( $_GET['post'], 'age_group_parent_' . $age->term_id, true );
				$checked	=	'';
				$value		=	'';
				if( $is_exist != '' ){
					$checked	=	"checked=checked";
					$value	=	$is_exist;
				} ?>
				<input type="checkbox" name="parent_<?=$age->term_id;?>" <?=$checked;?> id="parent_<?=$age->term_id;?>"/>
				<?=$age->name;?>:
				<input type="text" name="parent_price_<?=$age->term_id;?>" placeholder="Price" value="<?=$value;?>" style="width: 30% !important;" />
				<?php
				$groups_child = get_terms( 'age_groups', "orderby=count&hide_empty=0&parent=" . $age->term_id );
				foreach($groups_child as $child ){
					$is_exist_child	=	get_post_meta( $_GET['post'], 'age_group_child_' . $age->term_id . '_' . $child->term_id , true );
					$checked_child	=	'';
					$value_child	=	'';
					if( $is_exist_child != '' ){
						$checked_child	=	"checked=checked";
						$value_child	=	$is_exist_child;
					} ?>
					<br /><br />
					<input style="margin-left:15px;" type="checkbox" <?php echo $checked_child; ?> name="child_<?php echo $age->term_id; ?>_<?php echo $child->term_id; ?>" onclick="return checkParent('<?php echo $age->term_id;  ?>');" />
					<?php echo $child->name; ?>
					<input type="text" name="child_price_<?php echo $child->term_id; ?>" value="<?php echo $value_child;?>" placeholder="Price" style="width: 30% !important;" />
					<?php
				}
				echo "<br /><br />";
			}
		}
	}

	public function meta_box_pool_scoreboard(){
		$tid		=	$_GET['post'];
		$age_groups = 	array();
		$schedule_teams = $this->db->get_results("SELECT p.title, p.age_group, ps.* FROM $this->tb_pool_scoreboard as ps INNER JOIN $this->tb_pools as p on p.id = ps.pool_id and p.tid = '$tid' order by cast(p.age_group as UNSIGNED), ps.id", OBJECT);
		if ( count( $schedule_teams ) == 0 ){
			echo 'No Pool Scoreboard Generated Yet!';
		} else {
			?>
            <table class="wp-list-table widefat fixed striped" width="100%">
                <tr style="background-color:#2EA2CC;color:#FFF;height:35px;">
                    <td width="30"><span class="box1">Team ID</span></td>
                    <td><span class="box1">Team Name</span></td>
                    <td width="50"><span class="box1">W</span></td>
                    <td width="50"><span class="box1">L</span></td>
                    <td><span class="box1">Tie</span></td>
                    <td width="50"><span class="box1">P</span></td>
                    <td width="50"><span class="box1">RA</span></td>
                    <td><span class="box1">RS</span></td>
                    <td><span class="box1">RD</span></td>
                    <td><span class="box1">ARD</span></td>
                    <td><span class="box1">Seed</span></td>
                </tr>
                <?php
                	foreach ($schedule_teams as $value):
                		if ( !in_array($value->age_group, $age_groups) ) {
                			$age_groups[] = $value->age_group; }
                	endforeach;
                	foreach ($age_groups as $value) { $counter = 0;
						foreach( $schedule_teams as $ps ) {
							if($value == $ps->age_group): $counter++;
								if($counter == 1){?>
									<tr style="background-color:#F1F1F1;color:#000;"><td colspan="11">
									<?=$ps->age_group;?>
									<input name="save" type="submit" style="float: right;" class="button button-primary button-large" id="publish" value="Update"></td></tr>
	                    	<?php } ?>
	                    <tr style="color:#000;">
	                        <td><?=$ps->team_id;?></td>
	                        <td><?=aaysc_tournament_common::getTeamData($ps->team_id)->team_name;?></td>
	                        <td><?=$ps->wi;?></td>
	                        <td><?=$ps->lo;?></td>
	                        <td><?=$ps->tie;?></td>
	                        <td><?=$ps->pt;?></td>
	                        <td><?=$ps->ra;?></td>
	                        <td><?=$ps->rs;?></td>
	                        <td><?=$ps->rd;?></td>
	                        <td><?=$ps->ard;?></td>
	                        <td><input type="text" name="ps[<?=$ps->id;?>][seed]" value="<?=$ps->seed;?>" style="width: 100%;"></td>
						</tr>
					<?php endif;
                	}
                }
				?>
            </table>
	<?php
		}
	}

	public function meta_box_bracket_items(){

		$tid			=	$_GET['post'];
		$age_groups 	= 	array();
		$schedule_teams = $this->db->get_results("SELECT b.age_group, bs.* FROM $this->tb_bracket_schedule as bs INNER JOIN $this->tb_brackets as b on b.id = bs.bracket_id and b.tid = '$tid' order by cast(b.age_group as UNSIGNED),bs.id", OBJECT);
		$teams 			= $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE t_id = '$tid' OR t_id = '-1'", OBJECT);
		if ( count( $schedule_teams ) == 0 ){
			echo 'No Bracket Created Yet!';
		} else { ?>
	        <table class="wp-list-table widefat fixed striped" width="100%">
	            <tr style="background-color:#2EA2CC;color:#FFF;height:35px;">
	                <td width="20"><span class="box1">#</span></td>
	                <td><span class="box1">Team A</span></td>
	                <td width="50"><span class="box1">Team A Score</span></td>
	                <td><span class="box1">Team B</span></td>
	                <td width="50"><span class="box1">Team B Score</span></td>
	                <td width="50"><span class="box1">Round</span></td>
	                <td><span class="box1">Location</span></td>
	                <td><span class="box1">Time</span></td>
	            </tr>
	            <?php
	            	foreach ($schedule_teams as $value):
	            		if ($value->age_group == 'TBD') continue;
	            		if ( !in_array($value->age_group, $age_groups) ) {
	            			$age_groups[] = $value->age_group; }
	            	endforeach;
	            	foreach ($age_groups as $value) { $counter = 0;
						foreach( $schedule_teams as $bt ) {
							if($value == $bt->age_group): $counter++;
								if($counter == 1){?>
									<tr style="background-color:#F1F1F1;color:#000;"><td colspan="8">
									<?php echo $bt->age_group; ?>
									<input name="save" type="submit" style="float: right;" class="button button-primary button-large" id="publish" value="Update">
									</td></tr>
		                	<?php } ?>
		                <tr style="color:#000;">
		                    <td><?=$bt->id;?></td>
		                    <td><!-- <a href="javascript:void(0);" onclick="return open_popup('<?=$bt->team_a_id; ?>');" ><?=Aaysc_Tournament_Common::getTeamData($bt->team_a_id)->team_name;?></a> --><select style="width: 100%;" name="bt[<?=$bt->id;?>][teama]" autocomplete="off">
	                        	<?php foreach ($teams as $t) { ?>
		                        	<?php if($t->age == $bt->age_group OR $t->age == 'TBD'): ?>
		                        		<option <?=($t->team_id == $bt->team_a_id)?'selected="selected"':'';?> value="<?=$t->team_id;?>"><?=Aaysc_Tournament_Common::getTeamData($t->team_id)->team_name;?></option>
		                        	<?php endif; ?>
	                        	<?php } ?>
	                        	</select></td>
		                    <td><!--<input name="bt[<?=$bt->id;?>][teama]" value="<?=$bt->team_a_score; ?>" size="5">--><?=$bt->team_a_score; ?></td>
		                    <td><!-- <a href="javascript:void(0);" onclick="return open_popup('<?=$bt->team_b_id; ?>');" ><?=Aaysc_Tournament_Common::getTeamData($bt->team_b_id)->team_name;?></a> -->
		                    <select style="width: 100%;" name="bt[<?=$bt->id;?>][teamb]" autocomplete="off">
	                        	<?php foreach ($teams as $t) { ?>
		                        	<?php if($t->age == $bt->age_group OR $t->age == 'TBD'): ?>
		                        		<option <?=($t->team_id == $bt->team_b_id)?'selected="selected"':'';?> value="<?=$t->team_id;?>"><?=Aaysc_Tournament_Common::getTeamData($t->team_id)->team_name;?></option>
		                        	<?php endif; ?>
	                        	<?php } ?>
	                        	</select></td>
		                    <td><!--<input name="bt[<?=$bt->id;?>][teamb]" value="<?=$bt->team_b_score; ?>" size="5">--><?=$bt->team_b_score; ?></td>
		                    <td><?=$bt->level; ?></td>
		                    <td><input type="text" name="bt[<?=$bt->id;?>][location]" value="<?=$bt->location; ?>" style="width: 100%;" placeholder="Location"></td>
		                    <td><input type="text" name="bt[<?=$bt->id;?>][time]" value="<?=$bt->time;?>" style="width: 100%;" class="datetimepicker"></td>
						</tr>
				<?php 	endif;
	            	}
	            }
				?>
	        </table>
	<?php
		}
	}

	// Update Tournament from backend.
	public function tournament_update( $post_id ) {
	    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return $post_id;

	    if ( !wp_is_post_revision( $post_id )
	    && in_array(get_post_type( $post_id ), $this->is_tournament)
	    && 'auto-draft' != get_post_status( $post_id ) ) {
	        $post = get_post( $post_id );
	    	/*
	    	add clildren if tournament is added first time
	    	 */
	    	self::tournament_add_children($post_id, $post);
	    	$groups = get_terms( 'age_groups', 'orderby=count&hide_empty=0&parent=0' );
	    	$rt_results	= $this->db->get_results("SELECT * FROM $this->tb_t_registration WHERE t_id='$post_id' order by age", OBJECT);
	    	$st_results = $this->db->get_results("SELECT p.age_group, ps.* FROM $this->tb_pool_schedule as ps INNER JOIN $this->tb_pools as p ON ps.pool_id = p.id AND p.tid = $post_id", OBJECT);
	    	$ps_results = $this->db->get_results("SELECT p.title, p.age_group, ps.* FROM $this->tb_pool_scoreboard as ps INNER JOIN $this->tb_pools as p on p.id = ps.pool_id and p.tid = '$post_id' order by cast(p.age_group as UNSIGNED), ps.id", OBJECT);
	    	$bt_results	= $this->db->get_results("SELECT b.age_group, bs.* FROM $this->tb_bracket_schedule as bs INNER JOIN $this->tb_brackets as b on b.id = bs.bracket_id and b.tid = '$post_id'", OBJECT);

	    	foreach( $groups as $age) {
				if( isset( $_POST['parent_' . $age->term_id ] ) ){
					// debug the section
					aaysc_tournament_common::debug('Post # ' . $_POST['parent_price_' . $age->term_id ]);
					if ( ! add_post_meta( $post_id, 'age_group_parent_' . $age->term_id , $_POST['parent_price_' . $age->term_id ], true ) ) {
					   update_post_meta ( $post_id, 'age_group_parent_' . $age->term_id , $_POST['parent_price_' . $age->term_id ] );
					}
				   	$groups_child = get_terms( 'age_groups', "orderby=count&hide_empty=0&parent=" . $age->term_id );
				    foreach($groups_child as $child ){
					    if( isset( $_POST['child_' . $age->term_id . '_' . $child->term_id ] ) ){
					    	if ( ! add_post_meta( $post_id, 'age_group_child_' . $age->term_id . '_' . $child->term_id , $_POST['child_price_' .$child->term_id], true ) ) {
							   update_post_meta ( $post_id, 'age_group_child_' . $age->term_id . '_' . $child->term_id , $_POST['child_price_' .$child->term_id] );
							}
					    }else{
					    	delete_post_meta( $post_id, 'age_group_child_' . $age->term_id . '_' . $child->term_id , $_POST['child_price_' .$child->term_id] );
					    }
				    }
				}else{
					delete_post_meta( $post_id, 'age_group_parent_' . $age->term_id , $_POST['parent_price_' . $age->term_id ] );
				}
			}

	    	foreach ($st_results as $st) {
	    		$data  = array();
		    	$team_a_id = $_POST['st'][$st->id]['teama'];
		    	$team_b_id = $_POST['st'][$st->id]['teamb'];
		    	$location  = $_POST['st'][$st->id]['location'];
		    	$time 	   = $_POST['st'][$st->id]['time'];
		    	if( ($team_a_id) && $team_a_id != $st->team_a_id) $data['team_a_id'] = $team_a_id;
		    	if( ($team_b_id) && $team_b_id != $st->team_b_id) $data['team_b_id'] = $team_b_id;
		    	if( ($location)  && $location != $st->location) 	 $data['location'] = $location;
		    	if( ($time) 	 && $time != $st->time) 			 $data['time'] = $time;
		    	if($data){
		    		$res = $this->db->update(
						$this->tb_pool_schedule,
						$data,
						array( 'id' => $st->id )
					);
		    	}
	    	}
	    	foreach ($ps_results as $ps) {
	    		$data  = array();
		    	$seed = $_POST['ps'][$ps->id]['seed'];
		    	if( ($seed) 	 && $seed != $ps->seed) $data['seed'] = $seed;
		    	if($data){
		    		$res = $this->db->update(
						$this->tb_pool_scoreboard,
						$data,
						array( 'id' => $ps->id )
					);
		    	}
	    	}
	    	///
	    	foreach ($bt_results as $bt) {
	    		$data  = array();
	    		$team_a_id 	  = $_POST['bt'][$bt->id][teama];
		    	$team_b_id 	  = $_POST['bt'][$bt->id][teamb];
		    	$location  	  = $_POST['bt'][$bt->id][location];
		    	$time 	   	  = $_POST['bt'][$bt->id][time];
		    	if( ($team_a_id) && $team_a_id != $bt->team_a_id) $data['team_a_id'] = $team_a_id;
		    	if( ($team_b_id) && $team_b_id != $bt->team_b_id) $data['team_b_id'] = $team_b_id;
		    	if( ($location)  && $location != $bt->location) 	 $data['location'] = $location;
		    	if( ($time) 	 && $time != $bt->time) 			 $data['time'] = $time;
		    	if($data){
		    		$res = $this->db->update(
						$this->tb_bracket_schedule,
						$data,
						array( 'id' => $bt->id )
					);
		    	}
	    	}
	    	foreach ($rt_results as $rt) {
	    		$data  		  = array();
		    	$rank_id  	  = $_POST['rt'][$rt->id][rank_id];
		    	$age 	   	  = $_POST['rt'][$rt->id][age];
		    	$payment   	  = $_POST['rt'][$rt->id][payment];
		    	if( ($rank_id)  && $rank_id != $bt->rank_id) 	 $data['rank_id'] = $rank_id;
		    	if( ($age) 	 && $age != $bt->age) 			 $data['age'] = $age;
		    	if( ($payment) 	 && $payment != $bt->payment_status) $data['payment_status'] = $payment_status;
		    	if($data){
		    		$res = $this->db->update(
						$this->tb_t_registration,
						$data,
						array( 'id' => $rt->id )
					);
		    	}
	    	}
	    }
	}
	/* Functinality to delete the team fron backend. */
	public function delete_registration(){
		$id = (int)$_REQUEST['id'];
		$this->db->delete( $this->tb_t_registration, array( 'id' => $id ) );
		return; // add check to validation for delete process.
	}

	public function phpmailer_aaysc( $phpmailer ) {
	    $phpmailer->isSMTP();
	    $phpmailer->Host = 'smtpout.secureserver.net';
	    $phpmailer->SMTPAuth = true;
	    $phpmailer->Port = 80;
	    $phpmailer->Username = 'noreply@aaysc.com';
	    $phpmailer->Password = 'gogetit';
	    $phpmailer->From = "noreply@aaysc.com";
	    $phpmailer->FromName = "AAYSC";
	}

}

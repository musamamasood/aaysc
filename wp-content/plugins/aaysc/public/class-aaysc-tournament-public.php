<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Aaysc_Tournament
 * @subpackage Aaysc_Tournament/public
 * @author     Your Name <email@example.com>
 */
class Aaysc_Tournament_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Aaysc_Tournament    The ID of this plugin.
	 */
	private $Aaysc_Tournament;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version, $db, $tb_pools, $tb_pool_schedule, $tb_brackets, $tb_bracket_schedule, $tb_bracket_scoreboard, $tb_t_registration, $is_tournament;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $Aaysc_Tournament       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $Aaysc_Tournament, $version ) {

		global $wpdb;
		$this->Aaysc_Tournament = $Aaysc_Tournament;
		$this->version = $version;
		$this->db = $wpdb;
		$this->tb_pools = $this->db->prefix.'pools';
		$this->tb_pool_schedule = $this->db->prefix.'pool_schedule';
		$this->tb_pool_scoreboard = $this->db->prefix.'pool_scoreboard';
		$this->tb_brackets = $this->db->prefix.'brackets';
		$this->tb_bracket_schedule = $this->db->prefix.'bracket_schedule';
		$this->tb_bracket_scoreboard = $this->db->prefix.'bracket_scoreboard';
		$this->tb_t_registration = $this->db->prefix.'t_registration';
		$this->is_tournament = aaysc_tournament_common::is_tournament();
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aaysc_Tournament_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aaysc_Tournament_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Aaysc_Tournament, plugin_dir_url( __FILE__ ) . 'css/aaysc-tournament-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'sweetalert', plugin_dir_url( __FILE__ ) . 'css/sweetalert.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aaysc_Tournament_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aaysc_Tournament_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
 		global $post;
 		$aaysc = array(
			't_id' 		=> $post->ID,
			'nonce' 	=> wp_create_nonce( 'acf_nonce' ),
			'admin_url' => admin_url(),
			'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
		);
		wp_enqueue_script( $this->Aaysc_Tournament, plugin_dir_url( __FILE__ ) . 'js/aaysc-tournament-public.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'sweetalert', plugin_dir_url( __FILE__ ) . 'js/sweetalert.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'slides', plugin_dir_url( __FILE__ ) . 'js/jquery.slides.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->Aaysc_Tournament, 'aaysc', $aaysc );
	}

	/**
	 * Register the templates for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function tournament_templates($page_template) {
		#global $post_type;
		$post_type = get_post_type();
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Aaysc_Tournament_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Aaysc_Tournament_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		if ( is_page( 'Pool Schedule / Gameday' ) ) {
	        $page_template = dirname( __FILE__ )  . '/templates/template-pool.php';
	    }elseif ( is_page( 'Tournament Results / Scoreboard' ) ) {
	    	$page_template = dirname( __FILE__ )  . '/templates/template-scoreboard.php';
	    }elseif ( is_page( 'Championship Play' ) ) {
	    	$page_template = dirname( __FILE__ )  . '/templates/template-bracket.php';
	    }elseif ( is_page( "Who's Coming" ) ) {
	    	$page_template = dirname( __FILE__ )  . '/templates/template-coming.php';
	    }elseif ( is_page( 'Dashboard' ) ) {
	    	$page_template = dirname( __FILE__ )  . '/templates/template-dashboard.php';
	    }elseif ( is_page( 'Tournament Rules' ) ) {
	    	$page_template = dirname( __FILE__ )  . '/templates/template-rules.php';
	    }elseif ( in_array( $post_type, $this->is_tournament ) && !is_archive()){
        	$page_template = get_stylesheet_directory() . '/single-event.php';
	    }elseif ( in_array( $post_type, $this->is_tournament ) && is_archive()){
        	$page_template = get_stylesheet_directory() . '/archive-event.php';
	    }
	    return $page_template;
	}

	/**
	 * Disable admin bar from front-end for all users except administrator.
	 *
	 * @since    1.0.0
	 */
	public function remove_admin_bar() {
		if (!current_user_can('administrator') && !is_admin()) {
		  show_admin_bar(false);
		}
	}

	/**
	 * Update score from front end for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function pool_scoreboard() {
		$teamA = ($_POST['teamA']) ? $_POST['teamA'] : null;
		$teamB = ($_POST['teamB']) ? $_POST['teamB'] : null;
		$scoreA = ($_POST['scoreA'] != '') ? $_POST['scoreA'] : null;
		$scoreB = ($_POST['scoreB'] != '') ? $_POST['scoreB'] : null;
		$sID = ($_POST['sID']) ? $_POST['sID'] : null;
		$tID = ($_POST['tID']) ? $_POST['tID'] : null;
		$pID = ($_POST['pID']) ? $_POST['pID'] : null;
		if(($scoreA == '') || ($scoreB == '')) return false;

		$update = array( 'team_a_score' => $scoreA, 'team_b_score' => $scoreB );
		global $wpdb;
		$wpdb->update($wpdb->prefix.'pool_schedule',
			$update,
			array( 'id' => $sID )
		);
		// Insert data in scoreboard
		$this->scoreboard($teamA, $pID, $sID);
		$this->scoreboard($teamB, $pID, $sID);

		$rs_pt = array();
		$rs_wl = array('pool_id'=>$pID);
		// figure out issue when we have team score as 0, then how it affect on score.
		$total_games_a = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_pool_schedule WHERE (team_a_id = '$teamA' OR team_b_id = '$teamA') and (team_a_score != '0' OR team_b_score != '0') and pool_id=".$rs_wl['pool_id'], OBJECT );

		$total_games_b = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_pool_schedule WHERE (team_a_id = '$teamB' OR team_b_id = '$teamB') and (team_a_score != '0' OR team_b_score != '0') and pool_id=".$rs_wl['pool_id'], OBJECT );

		if( $scoreA  > $scoreB ){
			$rs_wl['win_team_id'] 	= $teamA;
			$rs_wl['loss_team_id'] 	= $teamB;
		}elseif ($scoreA < $scoreB){
			$rs_wl['win_team_id'] 	= $teamB;
			$rs_wl['loss_team_id'] 	= $teamA;
		}elseif ($scoreA == $scoreB){
			$wpdb->query("UPDATE $this->tb_pool_scoreboard SET tie=tie+1 where (team_id=". $teamA ." OR team_id=".$teamB.") and pool_id=".$rs_wl['pool_id']);
		}
		/* Update wi and lo column for teams */
		$wpdb->query("UPDATE $this->tb_pool_scoreboard SET wi=wi+1 where team_id=".$rs_wl['win_team_id']." and pool_id=".$rs_wl['pool_id']);
		$wpdb->query("UPDATE $this->tb_pool_scoreboard SET lo=lo+1 where team_id=".$rs_wl['loss_team_id']." and pool_id=".$rs_wl['pool_id']);
		/* Update pt column for teams */
		// if( isset($rs_pt[$teamA]) ){
		// 	$pt = 0;
			$earn_points = $wpdb->get_row("SELECT wi,tie FROM $this->tb_pool_scoreboard where team_id=".$teamA." and pool_id=".$rs_wl['pool_id'], OBJECT );
			$rs_pt[$teamA]  = ($earn_points->wi) ? $earn_points->wi * 1 : 0;
			$rs_pt[$teamA] += ($earn_points->tie) ? $earn_points->tie * 0.500 : 0;
			$pt = ($total_games_a->total_game_played) ? $rs_pt[$teamA] / $total_games_a->total_game_played : 0;
			$wpdb->query("UPDATE $this->tb_pool_scoreboard SET pt=$pt where team_id=".$teamA." and pool_id=".$rs_wl['pool_id']);
		// }
		// if ( isset($rs_pt[$teamB]) ) {
			// $pt = 0;
			$earn_points = $wpdb->get_row("SELECT wi,tie FROM $this->tb_pool_scoreboard where team_id=".$teamB." and pool_id=".$rs_wl['pool_id'], OBJECT );
			$rs_pt[$teamB]  = ($earn_points->wi) ? $earn_points->wi * 1 : 0;
			$rs_pt[$teamB] += ($earn_points->tie) ? $earn_points->tie * 0.500 : 0;
			$pt = ($total_games_b->total_game_played) ? $rs_pt[$teamB] / $total_games_b->total_game_played : 0;
			$wpdb->query("UPDATE $this->tb_pool_scoreboard SET pt=$pt where team_id=".$teamB." and pool_id=".$rs_wl['pool_id']);
		// }
		/* Ording based on pt | tie then  */
		$rs_scoreboard_arr = $wpdb->get_results("SELECT * FROM $this->tb_pool_scoreboard where pool_id=".$rs_wl[pool_id]." order by pt DESC", ARRAY_A ); //, ard
		$pt = array(); $ard = array(); $seed = 0;
		foreach ($rs_scoreboard_arr as $key => $row) {
		    $pt[$key]  = $row['pt'];
		    $ard[$key] = $row['ard'];
		}
		array_multisort($pt, SORT_DESC, $ard, SORT_DESC, $rs_scoreboard_arr);
		foreach ($rs_scoreboard_arr as $rs_scoreboard) { $seed = $seed+1;
			$wpdb->update( $this->tb_pool_scoreboard,
				array( 'seed' => $seed ),
				array( 'id' => $rs_scoreboard['id'] )
			);
		}
		echo true;
		wp_die();
	}

	public function scoreboard($tID, $pID, $sID){
		global $wpdb;

		$rs_score = $wpdb->get_row("SELECT SUM(team_a_score) AS score1, (SELECT SUM(team_b_score) AS score FROM $this->tb_pool_schedule WHERE team_b_id = '$tID' AND pool_id = '$pID' AND (team_a_score <> -1 OR team_b_score <> -1)) AS score2 FROM $this->tb_pool_schedule WHERE team_a_id = '$tID' AND pool_id = '$pID' AND (team_a_score <> -1 OR team_b_score <> -1)", OBJECT );

		$ra_score = $wpdb->get_row("SELECT SUM(team_a_score) AS score1, (SELECT SUM(team_b_score) AS score FROM $this->tb_pool_schedule WHERE team_a_id = '$tID' AND pool_id = '$pID' AND (team_a_score <> -1 OR team_b_score <> -1)) AS score2 FROM $this->tb_pool_schedule WHERE team_b_id = '$tID' AND pool_id = '$pID' AND (team_a_score <> -1 OR team_b_score <> -1)", OBJECT );
		$total_games = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_pool_schedule WHERE (team_a_id = '$tID' OR team_b_id = '$tID') and (team_a_score != '0' OR team_b_score != '0') and pool_id=".$pID, OBJECT );


		$RS 	= (($rs_score->score1) && $ra_score->score1 >= 0) ? $rs_score->score1 : 0;
		$RS 	+= (($rs_score->score2) && $ra_score->score2 >= 0) ? $rs_score->score2 : 0;

		$RA 	= (($ra_score->score1) && $ra_score->score1 >= 0) ? $ra_score->score1 : 0;
		$RA 	+= (($ra_score->score2) && $ra_score->score2 >= 0) ? $ra_score->score2 : 0;

		$RD 	= $RS - $RA;
		$ARD 	= ($total_games->total_game_played)? $RD / $total_games->total_game_played : 0;
		$ARD	= number_format($ARD, 2, '.', '');

		$wpdb->update( $this->tb_pool_scoreboard,
			array('team_id'=>$tID,'pool_id'=>$pID, 'ra'=>$RA,
				'rs'=> $RS,'rd'=> $RD,'ard'=> $ARD),
			array('u_identifier'=>$tID.$pID),
			array('%d','%d','%d','%d','%d','%f')
		);
	}

	/**
	 * Update score from front end for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function bracket_scoreboard() {
		$teamA = ($_POST['teamA']) ? $_POST['teamA'] : null;
		$teamB = ($_POST['teamB']) ? $_POST['teamB'] : null;
		$scoreA = ($_POST['scoreA'] != '') ? $_POST['scoreA'] : null;
		$scoreB = ($_POST['scoreB'] != '') ? $_POST['scoreB'] : null;
		$sID = ($_POST['sID']) ? $_POST['sID'] : null;
		$tID = ($_POST['tID']) ? $_POST['tID'] : null;
		$bID = ($_POST['bID']) ? $_POST['bID'] : null;
		if(($scoreA == '') || ($scoreB == '')) return false;

		$update = array( 'team_a_score' => $scoreA, 'team_b_score' => $scoreB );
		global $wpdb;
		$this->db->update($this->tb_bracket_schedule,
			$update,
			array( 'id' => $sID )
		);
		// Insert data in scoreboard
		$this->scoreboard_calulate($teamA, $bID, $sID);
		$this->scoreboard_calulate($teamB, $bID, $sID);

		$rs_pt = array();
		$rs_wl = array('bracket_id'=>$bID);

		$total_games_a = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_bracket_schedule WHERE (team_a_id = '$teamA' OR team_b_id = '$teamA') and (team_a_score != '0' OR team_b_score != '0') and bracket_id=".$rs_wl['bracket_id'], OBJECT );

		$total_games_b = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_bracket_schedule WHERE (team_a_id = '$teamB' OR team_b_id = '$teamB') and (team_a_score != '0' OR team_b_score != '0') and bracket_id=".$rs_wl['bracket_id'], OBJECT );

		if( $scoreA  > $scoreB ){
			$rs_wl['win_team_id'] 	= $teamA;
			$rs_wl['loss_team_id'] 	= $teamB;
			$rs_pt[$teamA] = 1;
		}elseif ($scoreA < $scoreB){
			$rs_wl['win_team_id'] 	= $teamB;
			$rs_wl['loss_team_id'] 	= $teamA;
			$rs_pt[$teamB] = 1;
		}

		/* Update wi and lo column for teams */
		$wpdb->query("UPDATE $this->tb_bracket_scoreboard SET wi=wi+1 where team_id=".$rs_wl['win_team_id']." and bracket_id=".$rs_wl['bracket_id']);
		$wpdb->query("UPDATE $this->tb_bracket_scoreboard SET lo=lo+1 where team_id=".$rs_wl['loss_team_id']." and bracket_id=".$rs_wl['bracket_id']);
		/* Update pt column for teams */
		if( $rs_pt[$teamA] ){
			$pt = 0;
			$pt = ($total_games_a->total_game_played) ? $rs_pt[$teamA] / $total_games_a->total_game_played : 0;
			$wpdb->query("UPDATE $this->tb_bracket_scoreboard SET pt=pt+$pt where team_id=".$teamA." and bracket_id=".$rs_wl['bracket_id']);
		}
		if ( $rs_pt[$teamB] ) {
			$pt = 0;
			$pt = ($total_games_b->total_game_played) ? $rs_pt[$teamB] / $total_games_b->total_game_played : 0;
			$wpdb->query("UPDATE $this->tb_bracket_scoreboard SET pt=pt+$pt where team_id=".$teamB." and bracket_id=".$rs_wl['bracket_id']);
		}
		echo true;
		wp_die();
	}

	public function scoreboard_calulate($tID, $bID, $sID){
		global $wpdb;

		$rs_score = $wpdb->get_row("SELECT SUM(team_a_score) AS score1, (SELECT SUM(team_b_score) AS score FROM $this->tb_bracket_schedule WHERE team_b_id = '$tID' AND bracket_id = '$bID' AND (team_a_score <> -1 OR team_b_score <> -1)) AS score2 FROM $this->tb_bracket_schedule WHERE team_a_id = '$tID' AND bracket_id = '$bID' AND (team_a_score <> -1 OR team_b_score <> -1)", OBJECT );

		$ra_score = $wpdb->get_row("SELECT SUM(team_a_score) AS score1, (SELECT SUM(team_b_score) AS score FROM $this->tb_bracket_schedule WHERE team_a_id = '$tID' AND bracket_id = '$bID' AND (team_a_score <> -1 OR team_b_score <> -1)) AS score2 FROM $this->tb_bracket_schedule WHERE team_b_id = '$tID' AND bracket_id = '$bID' AND (team_a_score <> -1 OR team_b_score <> -1)", OBJECT );
		$total_games = $wpdb->get_row("SELECT COUNT(*) AS total_game_played FROM $this->tb_bracket_schedule WHERE (team_a_id = '$tID' OR team_b_id = '$tID') and (team_a_score != '0' OR team_b_score != '0') and bracket_id=".$bID, OBJECT );

		$RS 	= (($rs_score->score1) && $ra_score->score1 >= 0) ? $rs_score->score1 : 0;
		$RS 	+= (($rs_score->score2) && $ra_score->score2 >= 0) ? $rs_score->score2 : 0;

		$RA 	= (($ra_score->score1) && $ra_score->score1 >= 0) ? $ra_score->score1 : 0;
		$RA 	+= (($ra_score->score2) && $ra_score->score2 >= 0) ? $ra_score->score2 : 0;

		$RD 	= $RS - $RA;
		$ARD 	= ($total_games->total_game_played)? $RD / $total_games->total_game_played : 0;
		$ARD	= number_format($ARD, 2, '.', '');

		$wpdb->update($this->tb_bracket_scoreboard,
			array('team_id'=>$tID,'bracket_id'=>$bID, 'ra'=>$RA,
				'rs'=> $RS,'rd'=> $RD,'ard'=> $ARD, 'u_identifier'=>$tID.$bID),
			array('u_identifier'=>$tID.$bID),
			array('%d','%d','%d','%d','%d','%f', '%d')
		);
	}
}

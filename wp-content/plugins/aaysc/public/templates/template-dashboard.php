<?php
/**
 * Template Name: Dashboard
 *
 * Developed by: Masood U.
 */
if ( (!is_user_logged_in() && !current_user_can('administrator')) ||  (!current_user_can('coach') && !is_user_logged_in()) ) {
	wp_redirect( get_page_link(1794) );
}
get_header();
?>
<div class="container">
	<div class="col-md-9 flow_sm dashboard">
		<?php
		global $wpdb;
		$user_id = get_current_user_id();
		$coach = aaysc_tournament_common::get_coachdata( $user_id );
		$coach->name = $coach->user_firstname.' '.$coach->user_lastname; ?>
		<h1 style="float:left;">
			<span class="red"><?=$coach->name?></span> (<?=$coach->roles[0]?>) -
			<span style="text-transform: capitalize;"><?=$coach->city;?></span>, <?=$coach->state;?>
		</h1>

		<!-- Button trigger modal -->
		<a class="btn btn-primary" href="#aaysc-aadteam" data-toggle="modal" style="float: right;margin-top: 10px;">Add Team</a>

		<?php
		$sports = aaysc_tournament_common::get_sports();
		$age_groups	= 	array(); $tournametTime = array();
		$user_id = 6;
		//$aaysc_teams = $wpdb->get_results("SELECT * from $wpdb->posts where post_type = 'team' and post_author = $user_id", OBJECT);
		$args = array(
			'post_type' => 'team',
			'posts_per_page'   => -1,
			'author'   => $user_id,
			'meta_key' => 'sport',
			'orderby' => 'meta_value',
		 );
		$aaysc_teams = get_posts( $args );
		?>
		<div class="panel-group" id="accordion">
			<?php $counter = 0; ?>
			<?php foreach ( $aaysc_teams as $post ): setup_postdata( $post ); $counter++;
					$sport = get_field('sport');
					$post_id = get_the_id();
					$division = get_term( get_field('divison_id')[0], 'age_groups'); ?>
			<div class="panel panel-default <?=$sport;?>">
				<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter;?>">
					<h3 class="panel-title">
						<img src="<?=plugins_url( "/img/$sport.jpg", dirname(__FILE__) );?>">
						<span class="team-name"><?=$sports[$sport];?></span>
						<span><?php the_title();?></span>
						<span class="divi">(10U)</span>
					</h3>
				</div>
				<div id="collapse<?=$counter;?>" class="panel-collapse collapse <?=($counter == 1)? 'in': ''; ?>">
					<div class="panel-body">
					<div class="row">
				        <div class="col-md-12">
				            <div class="panel with-nav-tabs panel-primary">
				                <div class="panel-heading">
			                        <ul class="nav nav-tabs">
			                            <li class="active"><a href="#tab1team<?=$counter;?>" data-toggle="tab">Team Info</a></li>
			                            <li><a href="#tab2team<?=$counter;?>" data-toggle="tab">Roster History</a></li>
			                            <li><a href="#tab3team<?=$counter;?>" data-toggle="tab">Manager History</a></li>
			                            <li><a href="#tab3team<?=$counter;?>" data-toggle="tab">Tournament History</a></li>
			                            <li><a href="#tab3team<?=$counter;?>" data-toggle="tab">Player Stats</a></li>
			                        </ul>
				                </div>
				                <div class="panel-body">
				                    <div class="tab-content">
				                        <div class="tab-pane fade in active" id="tab1team<?=$counter;?>">
				                        	<div class="alert alert-danger" role="alert">Warning! The <?php the_title();?> have 1 or more Tournaments that has not beed paid</div>
				                        	<div class="row">
				                        		<div class="col-md-5">
				                        			<table class="table table-bordered">
												      <thead><tr><th colspan="2">Team Information</th></tr></thead>
												      <tbody>
												        <tr><th scope="row">Team Manager</th><td><?=$coach->name?></td></tr>
												        <tr>
												          <th scope="row">Team City/State</th>
												          <td><?=(get_field('city'))?get_field('city'):$coach->city;?> / <?=(get_field('state'))?get_field('state'):$coach->state;?></td></tr>
												        <tr><th scope="row">Classification</th>
												          <td><?=$division->name;?></td>
												        </tr>
												      </tbody>
												    </table>
				                        		</div>
				                        		<div class="col-md-7">
				                        			<?php if( has_post_thumbnail() ) {
					                        			the_post_thumbnail('full', array( 'class' => 'alignleft' ));
				                        			} ?>
				                        		</div>
				                        	</div>
				                        </div>
				                        <div class="tab-pane fade" id="tab2team<?=$counter;?>">Roster History</div>
				                        <div class="tab-pane fade" id="tab3team<?=$counter;?>">Manager History</div>
				                        <div class="tab-pane fade" id="tab4team<?=$counter;?>">Tournament History</div>
				                        <div class="tab-pane fade" id="tab5team<?=$counter;?>">Player Stats</div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>

					<?php foreach ($tournametTime as $time): ?>
					<div class="accordion-heading"><i class="fa fa-calendar"></i> <?=$time;?></div>
					<div class="row">
						<?php foreach ($schedule_teams as $schedule_team): ?>
						<?php $c = date( 'l, M d, Y', strtotime( $schedule_team->time ) );?>
						<?php if(($age_group == $schedule_team->age_group)
								&& $time == $c ):?>
						<div class="col-md-4">
							<table class="table pool-data" data-height="299">
							<thead><tr>
								<th data-field="id">TEAM</th>
								<th data-field="name">Game Time: <?=date( 'h:i A', strtotime( $schedule_team->time ) );?></th>
								<th data-field="price">R</th>
							</tr></thead>
							<tbody>
							<?php $teamDataA = getTeamData($schedule_team->team_a_id);
								  $teamDataB = getTeamData($schedule_team->team_b_id);?>
							<tr>
						        <td>A<?=$teamDataA->rank_id;?></td>
						        <td style="line-height:15px;vertical-align: middle;"><span class="red"><?=$teamDataA->coach_name;?></span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="score-editing" name="result-a-<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>" value="<?=$schedule_team->team_a_score;?>" size="1">
						    	<?php else: ?>
						    		<?=$schedule_team->team_a_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td>A<?=$teamDataB->rank_id;?></td>
						        <td style="line-height:15px;vertical-align: middle;" ><span class="red"><?=$teamDataB->team_name;?></span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="score-editing" name="result-b-<?=$schedule_team->id;?>-<?=$schedule_team->team_b_id;?>" value="<?=$schedule_team->team_b_score;?>" size="1">
						    	<?php else: ?>
						    		<?=$schedule_team->team_b_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td colspan="3">Hartmann Park, Field#3</td>
						    </tr>
						    </tbody>
							</table>
						</div>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<?php endforeach; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>
	<?php get_footer(); ?>
	<!-- Modal -->
	<div id="aaysc-aadteam" class="modal fade" tabindex="-1">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button class="close" type="button" data-dismiss="modal">×</button>
						<h4 class="modal-title">Register Team</h4>
				</div>
				<div class="modal-body">
					<div class="row">
                        <div class="col-md-12 control-group">
                            <label class="control-label" for="TeamName">Team Name*</label>
                            <input type="text" value="" placeholder="Team Name" class="form-control" name="TeamName" id="TeamName">
                        </div>
                        <div class="col-md-6 control-group">
                            <label class="control-label" for="sport">Sport*</label>
                            <select name="SportId" id="SportId" class="form-control">
                            	<option selected="selected" disabled="disabled">Select Sport</option>
                            	<?php foreach ($sports as $posttype => $port): ?>
                            		<option value="<?=$posttype;?>"><?=$port;?></option>
                            	<?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6 control-group">
                        <!-- <select id="AgeGroupId" name="AgeGroupId" class="form-control"> -->
                        <?php
                        if ($ageClass) $ageClass = aaysc_tournament_common::clean_age_group($ageClass);
                        $term_children = aaysc_tournament_common::term_children();
                        ?>
                            <label class="control-label" for="type">Division*</label>
                            <input id="AgeGroupId" name="AgeGroupId" class="form-control">
                            <!-- <select id="AgeGroupId" name="AgeGroupId" class="form-control">
                                <option selected="selected">Select Division</option>
                                <?php foreach ($term_children as $children): ?>
                                    <?php $clean_name = aaysc_tournament_common::clean_age_group($children->name);?>
                                    <option <?=($ageClass==$clean_name)?'selected':'';?> value="<?=$clean_name;?>"><?=$children->name;?></option>
                                <?php endforeach; ?>
                            </select> -->
                            <input type="hidden" value="<?php echo $tournamentID; ?>"  name="tournament_id" id="tournament_id">
                            <input type="hidden" value="<?php echo $price; ?>"  name="price" id="price">
                        </div>
                    </div>
				</div>
				<div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button>
					<button class="btn btn-primary" type="button">Save changes</button></div>
				</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<!-- Columns End -->

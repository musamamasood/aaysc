<?php
get_header();
$tournamentID     = wp_get_post_parent_id( get_the_ID() );
$tournamentID 	  = ( $tournamentID == 0 ) ? get_the_ID() : $tournamentID;
global $wpdb;
$prefix = $wpdb->prefix;
$tb_pool = $prefix.'pools';
$tb_pool_schedule = $prefix.'pool_schedule';
$tb_pool_scoreboard = $prefix.'pool_scoreboard';
?>
<div class="container">
	<div class="col-md-9 flow_sm pool-schedule">
		<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
			<h2 class="pagehead"><?=get_the_title( $tournamentID );  ?></h2>
			<div>
				<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
				<span class="headvenue"><?=get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
				<br>
				<span class="headdate">
					<?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
				</span>
			</div>
		</div>
		<?php if(have_posts()): while(have_posts()): the_post(); ?>

			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>

		<?php endwhile; endif; ?>

		<?php
		$age_groups	= 	array(); $tournametTime = array(); $rs_scores = array();
		$schedule_teams = $wpdb->get_results( "SELECT p.id as pid, p.tid, p.title, p.location, p.age_group, ps.* FROM $tb_pool_schedule as ps INNER JOIN $tb_pool as p on p.id = ps.pool_id and p.tid = '$tournamentID' AND p.status = '1' order by cast(p.age_group as UNSIGNED), ps.time", OBJECT ); #$prefix_pools.id
		foreach ($schedule_teams as $value):
			$time = date( 'l, M d, Y', strtotime( $value->time ) );
			if ( !in_array($value->age_group, $age_groups) ) {
				$age_groups[] = $value->age_group;
				$rs_scores[$value->age_group] = $wpdb->get_results( "SELECT * from $tb_pool_scoreboard WHERE pool_id = '$value->pid' order by seed", OBJECT ); }
			if ( !in_array($time, $tournametTime) ) {
				$tournametTime[] = $time; }
		endforeach; ?>
		<div class="panel-group" id="accordion">
			<?php $counter = 0; ?>
			<?php foreach ($age_groups as $age_group): $counter++; ?>
			<div class="panel panel-default">
				<div class="panel-heading accordion-toggle <?=($counter != 1)? 'collapsed': ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter;?>">
					<h4 class="panel-title">
						<?=Aaysc_Tournament_Common::formateTitle($age_group);?>
					</h4>
				</div>
				<div id="collapse<?=$counter;?>" class="panel-collapse collapse <?=($counter == 1)? 'in': ''; ?>">
					<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
						<div class="element_size_100">
						<div class="pix-content-wrap">
						<div class="points-table fullwidth">
						<table class="table table-condensed table_D3D3D3">
							<thead>
								<tr>
									<th><span class="box1">Team #</span></th>
									<th><span class="box1">Team Name</span></th>
									<th><span class="box1">Coach Name</span></th>
									<th><span class="box1">City, State</span></th>
									<th><span class="box1">W</span></th>
									<th><span class="box1">L</span></th>
									<th><span class="box1">T</span></th>
									<th><span class="box1">PCT</span></th>
									<th><span class="box1">RA</span></th>
									<th><span class="box1">RS</span></th>
									<th><span class="box1">RD</span></th>
									<th><span class="box1">ARD</span></th>
									<th><span class="box1">SEED</span></th>
								</tr>
							</thead>
							<tbody>
							<?php $scores = $rs_scores[$age_group]; ?>
							<?php foreach ($scores as $score): ?>
							<?php
							if($score->team_id == -1) continue;
							$teamData 	= Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID);
							$team_name	= (get_field('nick_name',$score->team_id))? get_field('nick_name',$score->team_id):$teamData->team_name;
							$coach_name = $teamData->coach_name;
							$rank_id 	= $teamData->rank_id;
							$user_id 	= $teamData->user_id;
							$city 		= (get_user_meta($user_id, 'city', true)) ?get_user_meta($user_id, 'city', true) . ', '.get_user_meta($user_id, 'state', true):'';
							?>
								<tr>
									<td>A<?=$rank_id;?></td>
									<td class="red"><?=$team_name;?></td>
									<td><?=$coach_name;?></td>
									<td><?=$city;?></td>
									<td><?=$score->wi;?></td>
									<td><?=$score->lo;?></td>
									<td><?=$score->tie;?></td>
									<td><?=Aaysc_Tournament_Common::float_format($score->pt);?></td>
									<td><?=$score->ra;?></td>
									<td><?=$score->rs;?></td>
									<td><?=$score->rd;?></td>
									<td><?=$score->ard;?></td>
									<td><?=$score->seed;?> in A</td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						</div>
						</div>
						</div>
						</div>
					</div>

					<?php foreach ($tournametTime as $time): ?>
					<div class="accordion-heading"><i class="fa fa-calendar"></i> <?=$time;?></div>
					<div class="row">
						<?php foreach ($schedule_teams as $schedule_team): ?>
						<?php $c = date( 'l, M d, Y', strtotime( $schedule_team->time ) );?>
						<?php if(($age_group == $schedule_team->age_group)
								&& $time == $c ):?>
						<div class="col-md-4">
							<table class="table pool-data" data-height="299">
							<thead><tr>
								<th data-field="id">TEAM</th>
								<th data-field="name">Game Time: <?=date( 'h:i A', strtotime( $schedule_team->time ) );?></th>
								<th data-field="price">R</th>
							</tr></thead>
							<tbody>
							<?php $teamDataA = Aaysc_Tournament_Common::getTeamData($schedule_team->team_a_id, $tournamentID);
								  $teamDataB = Aaysc_Tournament_Common::getTeamData($schedule_team->team_b_id, $tournamentID);
								  $team_name_a	= (get_field('nick_name',$schedule_team->team_a_id))? get_field('nick_name',$schedule_team->team_a_id):$teamDataA->team_name;
								  $team_name_b	= (get_field('nick_name',$schedule_team->team_b_id))? get_field('nick_name',$schedule_team->team_b_id):$teamDataB->team_name;
								  ?>
							<tr>
						        <td>A<?=$teamDataA->rank_id;?></td>
						        <td style="line-height:15px;vertical-align: middle;"><span class="red"><?=$team_name_a;?></span><br><span class="black">(<?=$teamDataA->coach_name;?>)</span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="result<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>" name="result<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>" value="<?=($schedule_team->team_a_score == -1)?0:$schedule_team->team_a_score;?>" size="1">
						    	<?php else: ?>
						    		<?=($schedule_team->team_a_score == -1)?0:$schedule_team->team_a_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td>A<?=$teamDataB->rank_id;?></td>
						        <td style="line-height:15px;vertical-align: middle;" ><span class="red"><?=$team_name_b;?></span><br><span class="black">(<?=$teamDataB->coach_name;?>)</span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="result<?=$schedule_team->id;?>-<?=$schedule_team->team_b_id;?>" name="result<?=$schedule_team->id;?>-<?=$schedule_team->team_b_id;?>" value="<?=($schedule_team->team_b_score == -1)?0:$schedule_team->team_b_score;?>" size="1">
						    	<?php else: ?>
						    		<?=($schedule_team->team_b_score == -1)?0:$schedule_team->team_b_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td colspan="3" style="vertical-align: middle;"><?=$schedule_team->location?>
						        	<?php if(get_current_user_id() == 1):?>
						        		<button class="pool-score-editing pull-right" name="result-<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>-<?=$schedule_team->team_b_id;?>-<?=$schedule_team->pid;?>">Go</button>
						        	<?php endif; ?>
						        </td>
						    </tr>
						    </tbody>
							</table>
						</div>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<?php endforeach; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>

	<?php get_footer(); ?>
<!-- Columns End -->

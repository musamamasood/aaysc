<?php
get_header();
$tournamentID = wp_get_post_parent_id( get_the_ID() );
$tournamentID = ( $tournamentID == 0 ) ? get_the_ID() : $tournamentID;
global $wpdb;
$prefix = $wpdb->prefix;
$tb_pool 				= $prefix.'pools';
$tb_pool_schedule 		= $prefix.'pool_schedule';
$tb_pool_scoreboard 	= $prefix.'pool_scoreboard';
$tb_bracket 		 	= $prefix.'brackets';
$tb_bracket_schedule 	= $prefix.'bracket_schedule';
$tb_bracket_scoreboard 	= $prefix.'bracket_scoreboard';
?>
<div class="container">
	<div class="col-md-9 flow_sm pool-scoreboard">
		<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
			<h2 class="pagehead"><?=get_the_title( $tournamentID );  ?></h2>
			<div>
				<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
				<span class="headvenue"><?=get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
				<br>
				<span class="headdate">
					<?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
				</span>
			</div>
		</div>

		<?php if(have_posts()): while(have_posts()): the_post(); ?>

			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>

		<?php endwhile; endif; ?>

		<?php
		$flipboxes = array();
		if( have_rows('result_images', $tournamentID) ): while( have_rows('result_images', $tournamentID) ): the_row();

			$age_group 			   = aaysc_tournament_common::clean_age_group( get_sub_field('age_group')->name );
			$flipboxes[$age_group] = array( 'winner' => get_sub_field('winner_image'), 'runner' => get_sub_field('runnerup_image'),  'flipbox_back_winner' => get_sub_field('flipbox_back_winner'), 'flipbox_back_runner' => get_sub_field('flipbox_back_runner') );
			$championship = $wpdb->get_row("SELECT bs.* from $tb_bracket_schedule as bs INNER JOIN $tb_bracket as b on b.id = bs.bracket_id and b.tid = '$tournamentID' and b.age_group = '$age_group' order by level desc limit 1", OBJECT );
			// team a data
			if($championship){

				$teamData  = Aaysc_Tournament_Common::getTeamData($championship->team_a_id, $tournamentID);
				$championship->team_a_name = (get_field('nick_name',$championship->team_a_id))?get_field('nick_name',$championship->team_a_id):$teamData->team_name;
				$championship->team_a_coach   = $teamData->coach_name;
				// team b data
				$teamData  = Aaysc_Tournament_Common::getTeamData($championship->team_b_id, $tournamentID);
				$championship->team_b_name = (get_field('nick_name',$championship->team_b_id))?get_field('nick_name',$championship->team_b_id):$teamData->team_name;
				$championship->team_b_coach   = $teamData->coach_name;

				$flipboxes[$age_group][winner_team] = max( array($championship->team_a_score, $championship->team_a_name, $championship->team_a_coach), array($championship->team_b_score, $championship->team_b_name, $championship->team_b_coach) );
				$flipboxes[$age_group][runner_team] = min( array($championship->team_a_score, $championship->team_a_name, $championship->team_a_coach), array($championship->team_b_score, $championship->team_b_name, $championship->team_b_coach) );
			}
		endwhile; endif; ?>
		<?php
		// fetch all register teams for events:
		$age_groups	= 	array();
		$schedule_teams = $wpdb->get_results("SELECT ts.team_id, sum( ts.wi ) as wi, sum( ts.lo ) as lo, sum( ts.tie ) as ti, sum( ts.ra ) as ra, sum( ts.rs ) as rs, sum( ts.rd ) as rd, sum( ts.ard ) as ard, ts.age_group FROM (SELECT ps.team_id, ps.wi,ps.lo, ps.tie, ps.ra, ps.rs, ps.rd, ps.ard, ps.u_identifier, p.age_group FROM $tb_pool_scoreboard as ps INNER JOIN $tb_pool as p ON ps.pool_id = p.id AND p.tid = '$tournamentID' UNION SELECT bs.team_id, bs.wi, bs.lo, 0 as tie, bs.ra, bs.rs, bs.rd, bs.ard, bs.u_identifier, b.age_group FROM $tb_bracket_scoreboard as bs INNER JOIN $tb_bracket as b ON bs.bracket_id = b.id AND b.tid = '$tournamentID') ts group by ts.team_id, ts.age_group order by cast(age_group as UNSIGNED), wi DESC, ra DESC", OBJECT);
		foreach ($schedule_teams as $value):
			if ( !in_array($value->age_group, $age_groups) ) {
				$age_groups[] = $value->age_group;
			}
		endforeach;
		?>
		<?php
		// fetch all teams data
		$scoreboard = array(); $sc = 0;
		if($schedule_teams): foreach ($schedule_teams as $score):
			if($score->team_id == -1) continue;
			$teamData  = Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID);
			$user_id   = Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID)->user_id;
			$team_name = (get_field('nick_name',$score->team_id))?get_field('nick_name',$score->team_id):$teamData->team_name;
			$scoreboard[$score->age_group][$sc][team_name]   = $team_name;
			$scoreboard[$score->age_group][$sc][coach_name]  = $teamData->coach_name;
			$scoreboard[$score->age_group][$sc][rank_id] 	 = Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID)->rank_id;
			$scoreboard[$score->age_group][$sc][city] 		 = (get_user_meta($user_id, 'city', true)) ?get_user_meta($user_id, 'city', true) . ', '.get_user_meta($user_id, 'state', true):'';
			$scoreboard[$score->age_group][$sc][score]       = $score;
			$sc++;
		endforeach; endif; ?>
		<div class="panel-group" id="accordion">
			<?php $counter = 0; ?>
			<?php foreach ($age_groups as $age_group): $counter++; ?>
			<div class="panel panel-default">
				<div class="panel-heading accordion-toggle <?=($counter != 1)? 'collapsed': ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter;?>">
					<h4 class="panel-title"><?=Aaysc_Tournament_Common::formateTitle($age_group);?></h4>
				</div>
				<div id="collapse<?=$counter;?>" class="panel-collapse collapse <?=($counter == 1)? 'in': ''; ?>">
					<div class="panel-body">
					<?php if($flipboxes[$age_group]):?>
						<div class="aaysc-flip-boxes flip-boxes row">
						  <!-- first flipbox -->
						  <div class="aaysc-flip-box-wrapper col-lg-6 col-md-6 col-sm-6">
						    <div class="aaysc-flip-box">
						      <div class="flip-box-inner-wrapper" style="min-height: 259px;">
						        <div class="flip-box-front" style="background-color:#fff;border-color:#e0e0e0;border-radius:0px;border-style:solid;border-width:1px;color:#747474;">
						          <div class="flip-box-front-inner">
						            <div class="flip-box-grafix flip-box-image">
						              <img src="<?=$flipboxes[$age_group][winner]; ?>" width="300" height="174" alt="Winner">
						            </div>
						            <?=$flipboxes[$age_group][winner_team][1];?> - <?=$flipboxes[$age_group][winner_team][2];?>
						            <h2 class="flip-box-heading" style="color:#de2026;" data-fontsize="16" data-lineheight="27">Champion</h2>
						          </div>
						        </div>
						        <div class="flip-box-back" style="border: 1px solid rgb(224, 224, 224); border-radius: 0px; color: rgb(255, 255, 255); min-height: 259px; background-color: rgb(51,51,51);">
						          <div class="flip-box-back-inner" style="margin-top: 52px;">
						            <?=$flipboxes[$age_group][flipbox_back_winner]; ?>
						          </div>
						        </div>
						      </div>
						    </div>
						  </div>
						  <!-- second flipbox -->
						  <div class="aaysc-flip-box-wrapper col-lg-6 col-md-6 col-sm-6">
						    <div class="aaysc-flip-box">
						      <div class="flip-box-inner-wrapper" style="min-height: 259px;">
						        <div class="flip-box-front" style="background-color:#fff;border-color:#e0e0e0;border-radius:0px;border-style:solid;border-width:1px;color:#747474;">
						          <div class="flip-box-front-inner">
						            <div class="flip-box-grafix flip-box-image">
						              <img src="<?=$flipboxes[$age_group][runner]; ?>" width="300" height="174" alt="runner-up">
						            </div>
						            <?=$flipboxes[$age_group][runner_team][1];?> - <?=$flipboxes[$age_group][runner_team][2];?>
						            <h2 class="flip-box-heading" style="color:#de2026;" data-fontsize="18" data-lineheight="27">Runner-Up</h2>
						          </div>
						        </div>
						        <div class="flip-box-back" style="border: 1px solid rgb(224, 224, 224); border-radius: 0px; color: rgb(255, 255, 255); min-height: 259px; background-color: rgb(51,51,51);">
						          <div class="flip-box-back-inner" style="margin-top: 52px;">
						            <?=$flipboxes[$age_group][flipbox_back_runner]; ?>
						          </div>
						        </div>
						      </div>
						    </div>
						  </div>
						</div>
						<?php endif; ?>
						<div class="row">
							<div class="col-md-12">
							<div class="element_size_100">
							<div class="pix-content-wrap">
							<div class="points-table fullwidth">
							<table class="table table-condensed table_D3D3D3">
								<thead>
									<tr>
										<th><span class="box1">Team #</span></th>
										<th><span class="box1">Team Name</span></th>
										<th><span class="box1">Coach Name</span></th>
										<th><span class="box1">City, State</span></th>
										<th><span class="box1">W</span></th>
										<th><span class="box1">L</span></th>
										<th><span class="box1">T</span></th>
										<th><span class="box1">RA</span></th>
										<th><span class="box1">RS</span></th>
										<th><span class="box1">RD</span></th>
										<th><span class="box1">ARD</span></th>
									</tr>
								</thead>
								<tbody>
								<?php if($scoreboard[$age_group]): foreach ($scoreboard[$age_group] as $score): ?>
									<tr>
										<td>A<?=$score[rank_id];?></td>
										<td class="red"><?=$score[team_name];?></td>
										<td><?=$score[coach_name];?></td>
										<td><?=$score[city];?></td>
										<td><?=$score[score]->wi;?></td>
										<td><?=$score[score]->lo;?></td>
										<td><?=$score[score]->ti;?></td>
										<td><?=$score[score]->ra;?></td>
										<td><?=$score[score]->rs;?></td>
										<td><?=$score[score]->rd;?></td>
										<td><?=aaysc_tournament_common::float_format($score[score]->ard,1);?></td>
									</tr>
								<?php endforeach; endif; ?>
								</tbody>
							</table>
							</div>
							</div>
							</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>

	<?php get_footer(); ?>
<!-- Columns End -->

<?php
get_header();
$tournamentID = wp_get_post_parent_id( get_the_ID() );
$tournamentID = ( $tournamentID == 0 ) ? get_the_ID() : $tournamentID;
?>
<div class="container">
	<div class="col-md-9 flow_sm tournament-rules">
		<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
			<h2 class="pagehead"><?=get_the_title($tournamentID);?></h2>
			<div>
				<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
				<span class="headvenue"><?=get_post_meta($tournamentID,'event_venue', true ); ?></span>
				<br>
				<span class="headdate">
					<?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
			<?php if(have_posts()): while(have_posts()): the_post();?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>
			<?php endwhile; else: ?>
				<p>TOURNAMENT RULES IS COMING SOON!</p>
			<?php endif; ?>
			</div>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>

	<?php get_footer(); ?>
<!-- Columns End -->

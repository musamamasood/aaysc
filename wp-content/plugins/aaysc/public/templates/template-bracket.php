<?php
get_header();
$tournamentID = wp_get_post_parent_id( get_the_ID() );
$tournamentID = ( $tournamentID == 0 ) ? get_the_ID() : $tournamentID;
global $wpdb;
$prefix 				= $wpdb->prefix;
$tb_brackets 		 	= $prefix.'brackets';
$tb_bracket_schedule 	= $prefix.'bracket_schedule';
$tb_bracket_scoreboard 	= $prefix.'bracket_scoreboard';
?>
<div class="container">
	<div class="col-md-9 flow_sm pool-schedule">
		<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
			<h2 class="pagehead"><?php echo get_the_title( $tournamentID );  ?></h2>
			<div>
				<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
				<span class="headvenue"><?php echo get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
				<br>
				<span class="headdate">
					<?php echo date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?php echo date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
				</span>
			</div>
		</div>

		<?php if(have_posts()): while(have_posts()): the_post(); ?>

			<h1><?php the_title(); ?></h1>
			<?php the_content(); ?>

		<?php endwhile; endif; ?>

		<?php
		$age_groups	= 	array(); $tournametTime = array(); $rs_scores = array();
		$brackets = array();
		$schedule_teams = $wpdb->get_results("SELECT b.id as bid, b.tid, b.age_group, bs.* FROM $tb_bracket_schedule as bs INNER JOIN $tb_brackets as b on b.id = bs.bracket_id and b.tid = '$tournamentID' order by cast(b.age_group as UNSIGNED),bs.level, b.time", OBJECT);
		foreach ($schedule_teams as $value):
			$time = date( 'l, M d, Y', strtotime( $value->time ) );
			if ( !in_array($value->age_group, $age_groups) ) {
				$age_groups[] = $value->age_group;
				$rs_scores[$value->age_group] = $wpdb->get_results("SELECT * from $tb_bracket_scoreboard WHERE bracket_id = '$value->bid' ORDER BY wi DESC", OBJECT );
				$brackets[$value->age_group]  = array();
			}
			if ( !in_array($time, $tournametTime) ) {
				$tournametTime[] = $time; }
			/* */
			if ( !array_key_exists( $value->level, $brackets[$value->age_group] ) ) {
				$brackets[$value->age_group][$value->level] 	= array();
				$brackets[$value->age_group][$value->level][] 	= $value;
			}else{
				$brackets[$value->age_group][$value->level][] 	= $value;
			}
		endforeach; ?>
		<div class="panel-group" id="accordion">
			<?php $counter = 0; ?>
			<?php foreach ($age_groups as $age_group): $counter++; ?>
			<div class="panel panel-default">
				<div class="panel-heading accordion-toggle <?=($counter != 1)? 'collapsed': ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter;?>">
						<h4 class="panel-title"><?=aaysc_tournament_common::formateTitle($age_group);?></h4>
				</div>
				<div id="collapse<?=$counter;?>" class="panel-collapse collapse <?=($counter == 1)? 'in': ''; ?>">
					<div class="panel-body">
				    <?php $round = 0; $winner_team; $temp = count($brackets[$age_group]); //need to fix for bracket design?>
					<div id="bracket" class="bracket-level-<?=$temp;?>">
				    <?php foreach ($brackets[$age_group] as $levels) { $round++; $match = 0; ?>
				    	<?php foreach ($levels as $bracket) { $match++;?>
				    		<div id="round<?=$round;?>" class="round">
						        <div class="region region1">
						        	<div id="match<?=$match;?>" class="<?=$bracket->level;?> match m<?php if($bracket->level=='1' && $temp == 3){echo $match+1;}else{echo $match;}?>">
						    		<p class="slot slot1 team_">
						    			<?php $teama = aaysc_tournament_common::getTeamData($bracket->team_a_id, $tournamentID); ?>
						    			<?php $team_name_a	= (get_field('nick_name',$bracket->team_a_id))? get_field('nick_name',$bracket->team_a_id):$teama->team_name;?>
						    			<?=$team_name_a; ?>
							            <span class="seed ">
							            	<em class="score"><?=$bracket->team_a_score;?></em>
							            </span>
						            </p>
								    <p class="slot slot2 team_">
								    	<?php $teamb = aaysc_tournament_common::getTeamData($bracket->team_b_id, $tournamentID); ?>
								    	<?php $team_name_b	= (get_field('nick_name',$bracket->team_b_id))? get_field('nick_name',$bracket->team_b_id):$teamb->team_name;?>
						    			<?=$team_name_b; ?>
							            <span class="seed ">
							                <em class="score"><?=$bracket->team_b_score;?></em>
							            </span>
						    		</p>
						    		<?php $winner_team = max( array($bracket->team_a_score, $team_name_a), array($bracket->team_b_score, $team_name_b) );?>
									</div>
						    	</div>
							</div>
				    	<?php } ?>
				    <?php } ?>
					<div id="round<?=++$round;?>" class="round">
					<div class="match" style="border: 0px;">
					  <p style="border-bottom: 1px solid;margin: 0;position: absolute;left: 0;right: 0;     padding: 0 4px;
					    text-align: center;margin-top: -21px;"><?=$winner_team[1];?></p>
					    <p style="
					    margin: 0;position: absolute;left: 0;right: 0;padding: 0 4px;text-align: center;color: red;font-weight: bold;">Champions</p>
					</div>
			        </div>
					</div><!--bracket end-->


					<div class="row">
						<div class="col-md-12">
						<div class="element_size_100">
						<div class="pix-content-wrap">
						<div class="points-table fullwidth">
						<table class="table table-condensed table_D3D3D3">
							<thead>
								<tr>
									<th><span class="box1">Team #</span></th>
									<th><span class="box1">Team Name</span></th>
									<th><span class="box1">Coach Name</span></th>
									<th><span class="box1">City, State</span></th>
									<th><span class="box1">W</span></th>
									<th><span class="box1">L</span></th>
									<th><span class="box1">RA</span></th>
									<th><span class="box1">RS</span></th>
									<th><span class="box1">RD</span></th>
									<th><span class="box1">ARD</span></th>
								</tr>
							</thead>
							<tbody>
							<?php $scores = $rs_scores[$age_group]; ?>
							<?php foreach ($scores as $score): ?>
							<?php
							if($score->team_id == '-1') continue;
							$teamData 	= Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID);
							$team_name	= (get_field('nick_name',$score->team_id))? get_field('nick_name',$score->team_id):$teamData->team_name;
							$coach_name = $teamData->coach_name;
							$rank_id 	= $teamData->rank_id;
							$user_id 	= $teamData->user_id;
							$city 		= (get_user_meta($user_id, 'city', true)) ? get_user_meta($user_id, 'city', true) . ', '.get_user_meta($user_id, 'state', true):'';
							?>
								<tr>
									<td>A<?=$rank_id;?></td>
									<td class="red"><?=$team_name;?></td>
									<td><?=$coach_name;?></td>
									<td><?=$city;?></td>
									<td><?=$score->wi;?></td>
									<td><?=$score->lo;?></td>
									<td><?=$score->ra;?></td>
									<td><?=$score->rs;?></td>
									<td><?=$score->rd;?></td>
									<td><?=$score->ard;?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						</div>
						</div>
						</div>
						</div>
					</div>
					<?php foreach ($tournametTime as $time): ?>
					<div class="accordion-heading"><i class="fa fa-calendar"></i> <?=$time;?></div>
					<div class="row">
						<?php foreach ($schedule_teams as $schedule_team): ?>
						<?php $c = date( 'l, M d, Y', strtotime( $schedule_team->time ) );?>
						<?php if(($age_group == $schedule_team->age_group)
								&& $time == $c ):?>
						<?php if ($schedule_team->team_a_id): ?>
						<div class="col-md-4">
							<table class="table pool-data" data-height="299">
							<thead><tr>
								<th data-field="id">TEAM</th>
								<th data-field="name">Game Time: <?=date( 'h:i A', strtotime( $schedule_team->time ) );?></th>
								<th data-field="price">R</th>
							</tr></thead>
							<tbody>
							<?php $teamDataA = aaysc_tournament_common::getTeamData($schedule_team->team_a_id, $tournamentID);
								  $teamDataB = aaysc_tournament_common::getTeamData($schedule_team->team_b_id, $tournamentID);
								  $team_name_a	= (get_field('nick_name',$schedule_team->team_a_id))? get_field('nick_name',$schedule_team->team_a_id):$teamDataA->team_name;
								  $team_name_b	= (get_field('nick_name',$schedule_team->team_b_id))? get_field('nick_name',$schedule_team->team_b_id):$teamDataB->team_name;
							?>
							<tr>
						        <td>A<?=$teamDataA->rank_id;?></td>
						        <td style="line-height:15px;vertical-align: middle;"><span class="red"><?=$team_name_a;?></span><br><span class="black">(<?=$teamDataA->coach_name;?>)</span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="result<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>" name="result<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>" value="<?=($schedule_team->team_a_score == -1)?0:$schedule_team->team_a_score;?>" size="1">
						    	<?php else: ?>
						    		<?=($schedule_team->team_a_score == -1)?0:$schedule_team->team_a_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td>A</td>
						        <td style="line-height:15px;vertical-align: middle;" ><span class="red"><?=$team_name_b;?></span><br><span class="black">(<?=$teamDataB->coach_name;?>)</span></td>
						        <td><?php if(get_current_user_id() == 1):?>
						        <input type="text" class="result<?=$schedule_team->id;?>-<?=$schedule_team->team_b_id;?>" name="result<?=$schedule_team->id;?>-<?=$schedule_team->team_b_id;?>" value="<?=($schedule_team->team_b_score == -1)?0:$schedule_team->team_b_score;?>" size="1">
						    	<?php else: ?>
						    		<?=($schedule_team->team_b_score == -1)?0:$schedule_team->team_b_score;?>
						    	<?php endif; ?></td>
						    </tr><tr>
						        <td colspan="3"><?=$schedule_team->location?>
						        	<?php if(get_current_user_id() == 1):?>
						        		<button class="bracket-score-editing pull-right" name="result-<?=$schedule_team->id;?>-<?=$schedule_team->team_a_id;?>-<?=$schedule_team->team_b_id;?>-<?=$schedule_team->bid;?>">Go</button>
						        	<?php endif; ?>
						        </td>
						    </tr>
						    </tbody>
							</table>
						</div>
						<?php endif; ?>
						<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<?php endforeach; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>

	<?php get_footer(); ?>
<!-- Columns End -->

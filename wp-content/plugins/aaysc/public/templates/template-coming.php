<?php
get_header();
$tournamentID = wp_get_post_parent_id( get_the_ID() );
$tournamentID = ( $tournamentID == 0 ) ? get_the_ID() : $tournamentID;
global $wpdb;
$prefix 			= $wpdb->prefix;
$tb_t_registration 	= $prefix.'t_registration';
?>
<div class="container">
	<div class="col-md-9 flow_sm pool-schedule">
		<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
			<h2 class="pagehead"><?php echo get_the_title( $tournamentID );  ?></h2>
			<div>
				<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
				<span class="headvenue"><?php echo get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
				<br>
				<span class="headdate">
					<?php echo date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?php echo date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
				</span>
			</div>
		</div>
		<?php $entryDeadline = aaysc_tournament_common::entry_deadline($tournamentID);?>
		<h1><?=get_the_title($tournamentID);?>- <small style="vertical-align: middle;">Entry Deadline is <?=$entryDeadline->format("m/d/Y"); ?></small></h1>

		<?php
		$age_groups	= 	array(); $teams 		= array(); $display = false;
		if(strtotime($entryDeadline->format("M d, Y")) >= strtotime(date("M d, Y"))) $display = true;
		$results = $wpdb->get_results("SELECT * from $wpdb->postmeta where post_id = '$tournamentID' and meta_key like 'age_group_parent%'", OBJECT);
		foreach ($results as $v):
			$tem = array(); $total_teams = 0;
			$arr = explode('_', $v->meta_key);
			$tem['parent'] = get_term_by('id', $arr[3], 'age_groups');
			$tem['children'] = get_terms('age_groups', 'orderby=count&hide_empty=0&parent='.$arr[3]);
			foreach ($tem['children'] as $t) {
				$term_change = aaysc_tournament_common::clean_age_group( $t->name );
	            $teams = $wpdb->get_var("SELECT COUNT(*) FROM $tb_t_registration where t_id='$tournamentID' AND age_group='$term_change'");
	            $total_teams += $teams;
			}
			$age_groups[] = array( "parent" => $tem['parent'], "children" => $tem['children'], "teams" => $total_teams );
		endforeach;
		?>
		<div class="panel-group" id="accordion">
			<?php $counter = 0; ?>
			<?php foreach ($age_groups as $age_group): $counter++; ?>
			<div class="panel panel-default">
				<div class="panel-heading accordion-toggle <?=($counter != 1)? 'collapsed': ''; ?>" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$counter;?>">
					<h4 class="panel-title">
						<?=$age_group[parent]->name;?>
						<span class="red" style="text-transform: none;">(<?php echo ($age_group[teams] > 1)?"$age_group[teams] Teams":"$age_group[teams] Team";?>)</span>
						</h4>
				</div>
				<div id="collapse<?=$counter;?>" class="panel-collapse collapse <?=($counter == 1)? 'in': ''; ?>">
					<div class="panel-body">
					<?php foreach ($age_group['children'] as $agc): ?>
						<?php
                            $term_change = aaysc_tournament_common::clean_age_group( $agc->name );
                            $teams = $wpdb->get_results("SELECT * FROM $tb_t_registration where t_id='$tournamentID' AND age_group='$term_change'", OBJECT);
						?>
					<div class="row">
						<div class="col-md-12">
						<div class="element_size_100">
						<div class="accordion-heading"><?=$agc->name;?>
						<?php if($display): ?>
						<small style="margin-top: 3px;" class="pull-right"><a href="<?=get_page_link( $tournamentID );?>" class="red">Register Here</a></small>
						<?php endif; ?>
						</div>
						<?php if($teams): ?>
						<div class="points-table fullwidth">
						<table class="table table-condensed table_D3D3D3">
							<thead>
								<tr>
									<th><span class="box1">Age</span></th>
									<th><span class="box1">Class</span></th>
									<th><span class="box1">Team Name</span></th>
									<th><span class="box1">Coach Name</span></th>
									<th><span class="box1">City</span></th>
									<th><span class="box1">State</span></th>
									<!-- <th><span class="box1">W</span></th>
									<th><span class="box1">L</span></th>
									<th><span class="box1">T</span></th> -->
								</tr>
							</thead>
							<tbody>
							<?php foreach ($teams as $team): ?>
							<?php
							$class 		= explode( '_', $team->age_group); //print_r($class);
							$teamData 	= Aaysc_Tournament_Common::getTeamData($score->team_id, $tournamentID);
							$team_name 	= $teamData->team_name;
							$rank_id 	= $teamData->rank_id;
							$user_id 	= $teamData->user_id;
							$city 		= (get_user_meta($team->user_id, 'city', true)) ? get_user_meta($team->user_id, 'city', true):'';
							$state 		= (get_user_meta($team->user_id, 'state', true)) ? get_user_meta($team->user_id, 'state', true):'';
							?>
								<tr>
									<td><?=$class[0];?></td>
									<td><?=$class[3];?></td>
									<td span class="red"><?=$team->team_name;?></td>
									<td><?=$team->coach_name;?></td>
									<td><?=$city;?></td>
									<td><?=$state;?></td>
									<!-- <td><?=$score->wi;?></td>
									<td><?=$score->lo;?></td>
									<td><?=$score->tie;?></td> -->
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
						</div>
						<?php endif; ?>
						</div>
						</div>
					</div>
					<?php endforeach; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
		<aside class="col-md-3" id="tour_nav_aside">
			<?php dynamic_sidebar('sidebar-1'); ?>
		</aside>
	</div>

	<?php get_footer(); ?>
<!-- Columns End -->

/**
 * CssUserAgent (cssua.js) v2.1.28
 * http://cssuseragent.org
 *
 * Copyright (c)2006-2014 Stephen M. McKamey.
 * Licensed under The MIT License.
 */
/*jshint smarttabs:true, regexp:false, browser:true */
/*globals aaysc,swal */
/*jshint -W030 */

'use strict';
/**
 * @type {Object}
 */
var cssua = (

/**
 * @param html {Object} root DOM element
 * @param userAgent {string} browser userAgent string
 * @return {Object}
 */
function(html, userAgent, sa) {

	/**
	 * @const
	 * @type {string}
	 */
	var PREFIX = ' ua-';

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_Platform = /\s*([\-\w ]+)[\s\/\:]([\d_]+\b(?:[\-\._\/]\w+)*)/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_Version = /([\w\-\.]+[\s\/][v]?[\d_]+\b(?:[\-\._\/]\w+)*)/g;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_BlackBerry = /\b(?:(blackberry\w*|bb10)|(rim tablet os))(?:\/(\d+\.\d+(?:\.\w+)*))?/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_Silk = /\bsilk-accelerated=true\b/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_FluidApp = /\bfluidapp\b/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_desktop = /(\bwindows\b|\bmacintosh\b|\blinux\b|\bunix\b)/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_mobile = /(\bandroid\b|\bipad\b|\bipod\b|\bwindows phone\b|\bwpdesktop\b|\bxblwp7\b|\bzunewp7\b|\bwindows ce\b|\bblackberry\w*|\bbb10\b|\brim tablet os\b|\bmeego|\bwebos\b|\bpalm|\bsymbian|\bj2me\b|\bdocomo\b|\bpda\b|\bchtml\b|\bmidp\b|\bcldc\b|\w*?mobile\w*?|\w*?phone\w*?)/;

	/**
	 * @const
	 * @type {RegExp}
	 */
	var R_game = /(\bxbox\b|\bplaystation\b|\bnintendo\s+\w+)/;

	/**
	 * The root CssUserAgent
	 * @type {Object}
	 */
	var cssua = {

		parse:
			/**
			 * @param uaStr {string}
			 * @return {Object}
			 */
			function(uaStr, sa) {

				/**
				 * @type {Object}
				 */
				var ua = {};
				if (sa) {
					ua.standalone = sa;
				}

				uaStr = (''+uaStr).toLowerCase();
				if (!uaStr) {
					return ua;
				}

				var i, count, raw = uaStr.split(/[()]/);
				for (var j=0, rawCount=raw.length; j<rawCount; j++) {
					if (j%2) {
						// inside parens covers platform identifiers
						var platforms = raw[j].split(';');
						for (i=0, count=platforms.length; i<count; i++) {
							if (R_Platform.exec(platforms[i])) {
								var key = RegExp.$1.split(' ').join('_'),
									val = RegExp.$2;

								// if duplicate entries favor highest version
								if ((!ua[key] || parseFloat(ua[key]) < parseFloat(val))) {
									ua[key] = val;
								}
							}
						}

					} else {
						// outside parens covers most version identifiers
						var uas = raw[j].match(R_Version);
						if (uas) {
							for (i=0, count=uas.length; i<count; i++) {
								var parts = uas[i].split(/[\/\s]+/);
								if (parts.length && parts[0] !== 'mozilla') {
									ua[parts[0].split(' ').join('_')] = parts.slice(1).join('-');
								}
							}
						}
					}
				}

				if (R_mobile.exec(uaStr)) {
					// mobile device indicators
					ua.mobile = RegExp.$1;
					if (R_BlackBerry.exec(uaStr)) {
						delete ua[ua.mobile];
						ua.blackberry = ua.version || RegExp.$3 || RegExp.$2 || RegExp.$1;
						if (RegExp.$1) {
							// standardize non-tablet blackberry
							ua.mobile = 'blackberry';
						} else if (ua.version === '0.0.1') {
							// fix playbook 1.0 quirk
							ua.blackberry = '7.1.0.0';
						}
					}

				} else if (R_desktop.exec(uaStr)) {
					// desktop OS indicators
					ua.desktop = RegExp.$1;

				} else if (R_game.exec(uaStr)) {
					// game console indicators
					ua.game = RegExp.$1;
					var game = ua.game.split(' ').join('_');

					if (ua.version && !ua[game]) {
						ua[game] = ua.version;
					}
				}

				// platform naming standardizations
				if (ua.intel_mac_os_x) {
					ua.mac_os_x = ua.intel_mac_os_x.split('_').join('.');
					delete ua.intel_mac_os_x;

				} else if (ua.cpu_iphone_os) {
					ua.ios = ua.cpu_iphone_os.split('_').join('.');
					delete ua.cpu_iphone_os;

				} else if (ua.cpu_os) {
					ua.ios = ua.cpu_os.split('_').join('.');
					delete ua.cpu_os;

				} else if (ua.mobile === 'iphone' && !ua.ios) {
					ua.ios = '1';
				}

				// UA naming standardizations
				if (ua.opera && ua.version) {
					ua.opera = ua.version;
					// version/XXX refers to opera
					delete ua.blackberry;

				} else if (R_Silk.exec(uaStr)) {
					ua.silk_accelerated = true;

				} else if (R_FluidApp.exec(uaStr)) {
					ua.fluidapp = ua.version;
				}

				if (ua.applewebkit) {
					ua.webkit = ua.applewebkit;
					delete ua.applewebkit;

					if (ua.opr) {
						ua.opera = ua.opr;
						delete ua.opr;
						delete ua.chrome;
					}

					if (ua.safari) {
						if (ua.chrome || ua.crios || ua.opera || ua.silk || ua.fluidapp || ua.phantomjs || (ua.mobile && !ua.ios)) {
							delete ua.safari;

						} else if (ua.version && !ua.rim_tablet_os) {
							ua.safari = ua.version;

						} else {
							ua.safari = ({
								'419': '2.0.4',
								'417': '2.0.3',
								'416': '2.0.2',
								'412': '2.0',
								'312': '1.3',
								'125': '1.2',
								'85': '1.0'
							})[parseInt(ua.safari, 10)] || ua.safari;
						}
					}

				} else if (ua.msie || ua.trident) {
					if (!ua.opera) {
						// standardize Internet Explorer
						ua.ie = ua.msie || ua.rv;
					}
					delete ua.msie;

					if (ua.windows_phone_os) {
						// standardize window phone
						ua.windows_phone = ua.windows_phone_os;
						delete ua.windows_phone_os;

					} else if (ua.mobile === 'wpdesktop' || ua.mobile === 'xblwp7' || ua.mobile === 'zunewp7') {
						ua.mobile = 'windows desktop';
						ua.windows_phone = (+ua.ie < 9) ? '7.0' : (+ua.ie < 10) ? '7.5' : '8.0';
						delete ua.windows_nt;
					}

				} else if (ua.gecko || ua.firefox) {
					ua.gecko = ua.rv;
				}

				if (ua.rv) {
					delete ua.rv;
				}
				if (ua.version) {
					delete ua.version;
				}

				return ua;
			},

		format:
			/**
			 * @param ua {Object}
			 * @return {string}
			 */
			function (ua) {
				/**
				 * @param b {string} browser key
				 * @param v {string} browser value
				 * @return {string} formatted CSS classes
				 */
				function format(b, v) {
					b = b.split('.').join('-');

					/**
					 * @type {string}
					 */
					var css = PREFIX+b;
					if (typeof v === 'string') {
						v = v.split(' ').join('_').split('.').join('-');
						var i = v.indexOf('-');
						while (i > 0) {
							// loop through chopping last '-' to end off
							// concat result onto return string
							css += PREFIX+b+'-'+v.substring(0, i);
							i = v.indexOf('-', i+1);
						}
						css += PREFIX+b+'-'+v;
					}
					return css;
				}

				/**
				 * @type {string}
				 */
				var	uaCss = '';
				for (var b in ua) {
					if (b && ua.hasOwnProperty(b)) {
						uaCss += format(b, ua[b]);
					}
				}

				// user-agent classNames
				return uaCss;
			},

		encode:
			/**
			 * Encodes parsed userAgent object as a compact URI-Encoded key-value collection
			 * @param ua {Object}
			 * @return {string}
			 */
			function(ua) {
				var query = '';
				for (var b in ua) {
					if (b && ua.hasOwnProperty(b)) {
						if (query) {
							query += '&';
						}
						query += encodeURIComponent(b)+'='+encodeURIComponent(ua[b]);
					}
				}
				return query;
			}
	};

	/**
	 * @const
	 * @type {Object}
	 */
	cssua.userAgent = cssua.ua = cssua.parse(userAgent, sa);

	/**
	 * @const
	 * @type {string}
	 */
	var ua = cssua.format(cssua.ua)+' js';

	// append CSS classes to HTML node
	if (html.className) {
		html.className = html.className.replace(/\bno-js\b/g, '') + ua;

	} else {
		html.className = ua.substr(1);
	}

	return cssua;

})(document.documentElement, navigator.userAgent, navigator.standalone);

(function( $ ) {

	// set flip boxes equal front/back height
	$.fn.fusion_calc_flip_boxes_height = function() {var flip_box = $( this );
		var outer_height, height, top_margin = 0;

		flip_box.find( '.flip-box-front' ).css( 'min-height', '' );
		flip_box.find( '.flip-box-back' ).css( 'min-height', '' );
		flip_box.find( '.flip-box-front-inner' ).css( 'margin-top', '' );
		flip_box.find( '.flip-box-back-inner' ).css( 'margin-top', '' );
		flip_box.css( 'min-height', '' );

		setTimeout( function() {
			if( flip_box.find( '.flip-box-front' ).outerHeight() > flip_box.find( '.flip-box-back' ).outerHeight() ) {
				height = flip_box.find( '.flip-box-front' ).height();
				if( cssua.ua.ie && cssua.ua.ie.substr(0, 1) == '8' ) {
					outer_height = flip_box.find( '.flip-box-front' ).height();
				} else {
					outer_height = flip_box.find( '.flip-box-front' ).outerHeight();
				}
				top_margin = ( height - flip_box.find( '.flip-box-back-inner' ).outerHeight() ) / 2;

				flip_box.find( '.flip-box-back' ).css( 'min-height', outer_height );
				flip_box.css( 'min-height', outer_height );
				flip_box.find( '.flip-box-back-inner' ).css( 'margin-top', top_margin );
			} else {
				height = flip_box.find( '.flip-box-back' ).height();
				if( cssua.ua.ie && cssua.ua.ie.substr(0, 1) == '8' ) {
					outer_height = flip_box.find( '.flip-box-back' ).height();
				} else {
					outer_height = flip_box.find( '.flip-box-back' ).outerHeight();
				}
				top_margin = ( height - flip_box.find( '.flip-box-front-inner' ).outerHeight() ) / 2;

				flip_box.find( '.flip-box-front' ).css( 'min-height', outer_height );
				flip_box.css( 'min-height', outer_height );
				flip_box.find( '.flip-box-front-inner' ).css( 'margin-top', top_margin );
			}

			if( cssua.ua.ie && cssua.ua.ie.substr(0, 1) == '8' ) {
				flip_box.find( '.flip-box-back' ).css( 'height', '100%' );
			}

		}, 100 );
	};

	/*
	 * Pool Score Editing
	 */
	$('.pool-score-editing').click(function(){
		var selector= $(this);
		var name 	= selector.attr('name');
		var nameArr = name.split('-');
		var data = {
			'action': 'pool_scoreboard',
			'teamA'	: nameArr[2],
			'teamB'	: nameArr[3],
			'scoreA': $('.result'+nameArr[1]+'-'+nameArr[2]).val(),
			'scoreB': $('.result'+nameArr[1]+'-'+nameArr[3]).val(),
			'sID'	: nameArr[1],
			'tID'	: aaysc.t_id,
			'pID'	: nameArr[4],
		};
		console.log();
		swal({   title: 'Are you sure?',
				 text: 'You will not be able to recover this imaginary file!',
				 type: 'warning',
				 showCancelButton: true,
				 confirmButtonColor: '#DD6B55',
				 confirmButtonText: 'Yes, Proceed!',
				 cancelButtonText: 'No, Stop!',
				 closeOnConfirm: false,
				 closeOnCancel: false
				},
				 function(isConfirm){
				 	if (isConfirm) {
				 		$.post(aaysc.ajaxurl, data, function(response){
							if(response == 1){
								swal('Success!', 'Score submitted Successfully!', 'success');
							}else{
								swal('Error', 'Score submitted Failed!', 'error');
							}
						});
				 	} else {
				 		swal('Cancelled', 'Your imaginary file is safe :)', 'error');
				 	}
				 });
		return false;
	});
		/*
		 * Bracket Score Editing
		 */
		$('.bracket-score-editing').click(function(){
			var selector= $(this),
				name 	= selector.attr('name'),
				nameArr = name.split('-');
			var data = {
				'action': 'bracket_scoreboard',
				'teamA'	: nameArr[2],
				'teamB'	: nameArr[3],
				'scoreA': $('.result'+nameArr[1]+'-'+nameArr[2]).val(),
				'scoreB': $('.result'+nameArr[1]+'-'+nameArr[3]).val(),
				'sID'	: nameArr[1],
				'tID'	: aaysc.t_id,
				'bID'	: nameArr[4],
			};
			$.post(aaysc.ajaxurl, data, function(response){
				console.log('Got this from the server: ' + response);

				if(response == 1){
					swal({ title:'Score submitted Successfully!',
						   type: 'success',
						   timer: 2000});
				}else{
					swal({ title:'Score submitted Failed!',
						   type: 'error',
						   timer: 2000});
				}
			});
			return false;
		});
		/*
		 * Event page slider gallery
		 */
		$('#tournament-gallery').slidesjs({
            width: 729,
            height: 300,
            start: 1
        });

		/*
		 *  Registeration page form validation
		 */
		$('#CreateCoachAccountForm').validate({
            onfocusout: function (element) {
                this.element(element);
            },
            rules: {
                TeamName: {
                    required: true,
                    minlength: 5
                },
                SportId: {
                    required: true,
                },
                UserName: {
                    required: true,
                    minlength: 5,
                    maxlength: 60
                },
                UserEmail: {
                    required: true,
                    email: true
                },
                FirstName: {
                    required: true,
                    minlength: 3
                },
                LastName: {
                    required: true,
                    minlength: 3
                },
                Address1: {
                    required: true,
                    minlength: 10
                },
                City: {
                    required: true,
                    minlength: 3
                },
                State: {
                    required: true,
                    minlength: 1
                },
                Zip: {
                    required: true,
                    minlength: 5
                },
                MobilePhone: {
                    required: true,
                    phoneUS: true
                },
                Password: {
                    required: true,
                    minlength: 8,
                    maxlength: 15
                },
                ConfirmPassword: {
                    required: true,
                    equalTo: '#Password'
                },
            },
            messages: {
                TeamName: 'Please enter team name.',
                UserEmail:'Please enter a valid email address.',
                UserName: 'Please provide valid Username.',
                FirstName:'Please provide First Name.',
                LastName: 'Please provide Last Name.',
                Address1: 'Please provide Address.',
                City: 	  'Please provide City.',
                State: 	  'Please Select State.',
                Zip: 	  'Please provide Zipcode.',
                Password: 'Password should be 8 characters long.',
            },
            errorElement: 'div',
            errorPlacement: function (error, element) {
                element.after(error);
            }
        });

		$('#SportId').on('change', function(){
			var sport = $(this).val(); console.log(sport);
			$('#dropdown').select2('destroy');
			$('#AgeGroupId').select2( {
				//data: data,
				minimumResultsForSearch: -1,
				placeholder: 'Search for a repository',
			    ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
			        url: 'https://api.github.com/search/repositories',
			        dataType: 'json',
			        quietMillis: 250,
			        data: function () {
			            return {
			                q: sport,
			            };
			        },
			        results: function (data) { // parse the results into the format expected by Select2.
			            // since we are using custom formatting functions we do not need to alter the remote JSON data
			            return { results: data.items };
			        },
			        cache: true
			    },
			    initSelection: function(element, callback) {
			        // the input tag has a value attribute preloaded that points to a preselected repository's id
			        // this function resolves that id attribute to an object that select2 can render
			        // using its formatResult renderer - that way the repository name is shown preselected
			        var id = $(element).val();
			        if (id !== '') {
			            $.ajax('https://api.github.com/repositories/' + id, {
			                dataType: 'json'
			            }).done(function(data) { callback(data); });
			        }
			    }
			});
		});
		$('#SportId').select2( {
			placeholder: 'Search Sport',
			minimumResultsForSearch: -1
		});
		// jQuery('#AgeGroupId').select2( {
		// 	//data: data,
		// 			minimumResultsForSearch: -1,
		// 			placeholder: "Search for a repository",
		//     ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
		//         url: "https://api.github.com/search/repositories",
		//         dataType: 'json',
		//         quietMillis: 250,
		//         data: function (term, page) {
		//             return {
		//                 q: term, // search term
		//             };
		//         },
		//         results: function (data, page) { // parse the results into the format expected by Select2.
		//             // since we are using custom formatting functions we do not need to alter the remote JSON data
		//             return { results: data.items };
		//         },
		//         cache: true
		//     },
		//     initSelection: function(element, callback) {
		//         // the input tag has a value attribute preloaded that points to a preselected repository's id
		//         // this function resolves that id attribute to an object that select2 can render
		//         // using its formatResult renderer - that way the repository name is shown preselected
		//         var id = $(element).val();
		//         if (id !== "") {
		//             $.ajax("https://api.github.com/repositories/" + id, {
		//                 dataType: "json"
		//             }).done(function(data) { callback(data); });
		//         }
		//     }
		// });

		//flipbox hover
		$('.aaysc-flip-box').mouseover(function() {
			$(this).addClass('hover');
		});

		$('.aaysc-flip-box').mouseout(function() {
			$(this).removeClass('hover');
		});

		// Flip Boxes
		$( '.flip-box-inner-wrapper' ).each( function() {
			$( this ).fusion_calc_flip_boxes_height();
		});
		$('.accordion-toggle').on('click', function () {
		    // Flip Boxes
			$( '.flip-box-inner-wrapper' ).each( function() {
				$( this ).fusion_calc_flip_boxes_height();
			});
		});
	});
})( jQuery );

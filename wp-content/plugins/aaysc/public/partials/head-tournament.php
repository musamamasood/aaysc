<div class="grad" style="padding:11.5px; margin-bottom:20px; ">
	<h2 class="pagehead"><?=get_the_title( $tournamentID );  ?></h2>
	<div>
		<button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
		<span class="headvenue"><?=get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
		<br>
		<span class="headdate">
			<?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
		</span>
	</div>
</div>

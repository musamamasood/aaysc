<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Aaysc_Tournament
 *
 * @wordpress-plugin
 * Plugin Name:       Aaysc Tournament
 * Plugin URI:        http://example.com/aaysc-tournament-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.5.0
 * Author:            Masood U.
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       aaysc
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Tournament types for aaysc
if ( ! defined( 'TOURNAMENT' ) ) {
	define('TOURNAMENT', "event;basketball");
}

// Tournament Season Enabled
if ( ! defined( 'SEASON' ) ) {
	define('SEASON', true);
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-aaysc-tournament-activator.php
 */
function activate_aaysc_tournament() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-aaysc-tournament-activator.php';
	Aaysc_Tournament_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-aaysc-tournament-deactivator.php
 */
function deactivate_aaysc_tournament() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-aaysc-tournament-deactivator.php';
	Aaysc_Tournament_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_aaysc_tournament' );
register_deactivation_hook( __FILE__, 'deactivate_aaysc_tournament' );


/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
if (version_compare(PHP_VERSION, '5.3.0') >= 0)
	require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';
else
	require plugin_dir_path( __FILE__ ) . 'vendor/jjgrainger/wp-custom-post-type-class/src/CPT.php';
require plugin_dir_path( __FILE__ ) . 'includes/class-aaysc-tournament.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_Aaysc_Tournament() {

	$plugin = new Aaysc_Tournament();
	$plugin->run();

}
run_Aaysc_Tournament();

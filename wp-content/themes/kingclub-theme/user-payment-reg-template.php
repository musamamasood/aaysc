<?php
/*
  Template Name: User Payment Reg
 */
$tournamentID = $_GET['tournament_id'];
$ageClass = $_GET['ageClass'];
$price = $_GET['price'];


if (!isset($_GET['logged_in'])) {
/// Saving user register data

    $UserName = $_POST['UserName'];
    $UserEmail = $_POST['UserEmail'];
    $Password = $_POST['Password'];
    $FirstName = $_POST['FirstName'];
    $LastName = $_POST['LastName'];
    $Address1 = $_POST['Address1'];
    $Address2 = $_POST['Address2'];
    $City = $_POST['City'];
    $State = $_POST['State'];
    $Zip = $_POST['Zip'];
    $TeamName = $_POST['TeamName'];
    $SportId = $_POST['SportId'];
    $AgeGroupId = $_POST['AgeGroupId'];
    $MobilePhone = $_POST['MobilePhone'];
    $tournamentID = $_POST['tournament_id'];
    $price = $_POST['price'];

    $return_url = site_url() . '/tournament-registration/?price=' . $price . '&tournament_id=' . $tournamentID . '&ageClass=' . $AgeGroupId;
     if ( username_exists( $UserName ) ){
         wp_redirect($return_url."&error=username_already_exits");exit;
     }
    $name = ucfirst($_POST['FirstName']) . " " . ucfirst($_POST['LastName']);

    $userdata = array(
        'user_login' => $UserName,
        'user_pass' => $Password,
        'user_email' => $UserEmail,
        'first_name' => $FirstName,
        'last_name' => $LastName,
        'role' => 'coach'
    );

    $user_id = wp_insert_user($userdata);
    //Inserting User Meta Data
    update_user_meta($user_id, 'address1', $Address1);
    update_user_meta($user_id, 'address2', $Address2);
    update_user_meta($user_id, 'city', $City);
    update_user_meta($user_id, 'state', $State);
    update_user_meta($user_id, 'zip_code', $Zip);
    update_user_meta($user_id, 'mob_phone', $MobilePhone);

    // Create team object
    $teamInfo = array();
    $teamInfo['post_title'] = $TeamName;
    $teamInfo['post_status'] = 'publish';
    $teamInfo['post_type'] = 'team';
    $teamInfo['post_author'] = $user_id;

    // Insert the team into the database
    $team_id = wp_insert_post($teamInfo);

    global $wpdb;
    $wpdb->insert('wp_t_registration', array(
        'user_id' => $user_id,
        'coach_name' => $name,
        'team_id' => $team_id,
        'team_name' => $TeamName,
        't_id' => $tournamentID,
        'rank_id' => 0,
        'age' => $AgeGroupId,
        'age_group' => $AgeGroupId,
        'price' => $price,
        'payment_status' => 0,
        'date' => date("Y/m/d")
    ));
    $current_user = wp_get_current_user();
    @mail($current_user->user_email, 'Team Registration!', "Your team $team->team_name has been registered successfully.");
    @mail(get_option('admin_email'), 'New Team Registration!', "New team ( $team->team_name ) has been registered successfully.");

    $creds = array();
    $creds['user_login'] = $UserName;
    $creds['user_password'] = $Password;
    $creds['remember'] = true;
    $user = wp_signon($creds, false);

    if (is_wp_error($user)) {
        echo $user->get_error_message();
    } else {
        wp_redirect(get_page_link(1857) . "?tournament_id=" . $tournamentID . "&ageClass=" . $ageClass . "&price=" . $price . "&user_id=" . $user->ID . "&logged_in=yes");
    }
} else {
    echo $user_id = $_GET['user_id'];
}

get_header();


wp_reset_query();
$width = 1100;
$height = 556;
$image_url = '';
if (post_password_required()) {
    echo '<div class="rich_editor_text">' . px_password_form() . '</div>';
} else {
    $px_meta_page = px_meta_page('px_page_builder');
    if (count($px_meta_page) > 0) {
        ?>
        <?php if ($px_meta_page->sidebar_layout->px_layout <> '' and $px_meta_page->sidebar_layout->px_layout <> "none" and $px_meta_page->sidebar_layout->px_layout == 'left') : ?>
            <aside class="col-md-3 up">
            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($px_meta_page->sidebar_layout->px_sidebar_left)) : endif; ?>

            </aside>
        <?php endif; ?>
        <div class="<?php echo px_meta_content_class(); ?> flow_sm">

            <h2 style="float:left; width:100%">
                You have registered successfully <br/>
                Click the button below to pay for this tournament</h3>
                <div style="float:left; width:100%; margin-bottom:25px;">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

                        <input name="cmd" type="hidden" value="_xclick" />
                        <input name="business" type="hidden" value="steve@aaysc.com" />
                        <input name="return" type="hidden" value="http://www.aaysc.com/success/?price=<?php echo $price; ?>&userID=<?php echo $user_id; ?>&tournament_id=<?php echo $tournamentID; ?>&ageClass=<?php echo $ageClass; ?>" />
                        <input name="item_name" type="hidden" value="Tournament Fee" />
                        <input name="amount" type="hidden" value="<?php echo $price; ?>" />
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="image" name="submit" border="0"
                               src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
                               alt="PayPal - The safer, easier way to pay online">
                    </form>
                </div>
        <?php
        wp_reset_query();
        $image_url = px_get_post_img_src($post->ID, $width, $height);
        if ($image_url <> '') {
            echo '<figure class="featured-img"><a href="' . get_permalink() . '" ><img src="' . $image_url . '" alt="" ></a></figure>';
        }
        if ($px_meta_page->page_content == "on" && get_the_content() <> '') {
            echo '<div class="rich_editor_text pix-content-wrap">';
            if ($px_meta_page->page_content == "on" && get_the_content() <> '') {
                the_content();
                ?>
                        <?php
                        wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'Rocky') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>'));
                    }
                    echo '</div>';
                }
                global $px_counter_node;
                foreach ($px_meta_page->children() as $px_node) {
                    if ($px_node->getName() == "blog") {
                        if (!isset($_SESSION["px_page_back"]) || isset($_SESSION["px_page_back"])) {
                            $_SESSION["px_page_back"] = get_the_ID();
                        }
                        $px_counter_node++;
                        get_template_part('page_blog', 'page');
                    } else if ($px_node->getName() == "gallery_albums") {
                        $px_counter_node++;
                        if ($px_node->px_gal_album_cat <> "") {
                            get_template_part('page_gallery_albums', 'page');
                        }
                    } else if ($px_node->getName() == "gallery") {
                        $px_counter_node++;
                        if ($px_node->album <> "" and $px_node->album <> "0") {
                            get_template_part('page_gallery', 'page');
                        }
                    } else if ($px_node->getName() == "slider") {
                        $px_counter_node++;
                        if ($px_node->slider <> "" and $px_node->slider <> "0") {
                            get_template_part('page_slider', 'page');
                        }
                    } else if ($px_node->getName() == "event") {
                        if (!isset($_SESSION["px_page_back_event"]) || isset($_SESSION["px_page_back_event"])) {
                            $_SESSION["px_page_back_event"] = get_the_ID();
                        }
                        $px_counter_node++;
                        get_template_part('page_event', 'page');
                    } elseif ($px_node->getName() == "team") {
                        $px_counter_node++;
                        get_template_part('page_team', 'page');
                    } elseif ($px_node->getName() == "map") {
                        $px_counter_node++;
                        echo px_map_page();
                    } elseif ($px_node->getName() == "fixtures") {
                        $px_counter_node++;
                        px_fixtures_page();
                    } elseif ($px_node->getName() == "contact") {
                        $px_counter_node++;
                        get_template_part('page_contact', 'page');
                    } elseif ($px_node->getName() == "column") {
                        $px_counter_node++;
                        px_column_page();
                    } elseif ($px_node->getName() == "pointtable") {
                        $px_counter_node++;
                        get_template_part('page_pointtable', 'page');
                    }
                }
                wp_reset_query();


                $tournamentID = $_GET['tournament_id'];
                ?>
        </div>


                <?php ?>
                <?php
                //


                if ($px_meta_page->sidebar_layout->px_layout <> '' or $px_meta_page->sidebar_layout->px_layout <> "none" or $px_meta_page->sidebar_layout->px_layout == 'right') :
                    ?>
            <aside class="col-md-3 down">

                    <?php
                    dynamic_sidebar('tour_register_sidebar');
                    ?>
            </aside>
        <?php endif; ?>

    <?php
    }else {

        px_page_title();
        ?>


        <div class="rich_editor_text pix-content-wrap">
            <?php
            while (have_posts()) : the_post();
                $image_url = px_get_post_img_src($post->ID, $width, $height);
                if ($image_url <> '') {
                    echo '<figure class="featured-img"><a href="' . get_permalink() . '" ><img src="' . $image_url . '" alt="" ></a></figure>';
                }
                the_content();
                wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'Rocky') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>'));
            endwhile;
            if (comments_open()) {
                comments_template('', true);
            }
            wp_reset_query();
            ?>
        </div>
        <?php
        }
    }

    get_footer();
    ?>
<!-- Columns End -->

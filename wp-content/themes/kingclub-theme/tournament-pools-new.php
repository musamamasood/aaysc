<?php
/*
Template Name: Tournament Pools New Template
*/

get_header();  
 
// Get Pools First
 $pools = get_terms( 'event-pool', array(
     'orderby'    => 'slug',
     'hide_empty' => 1
     ) );      


// Getting Coach First Name & Last Name
$user_info = get_userdata(13);
$name =  ucfirst($user_info->first_name)." ".ucfirst($user_info->last_name);

// Getting Coach's Team information
$args = array(
    'author'        =>  13, // I could also use $user_ID, right?
    'orderby'       =>  'post_date',
    'post_type'         =>  'team' 
    );
	
$coach_team_object = get_posts( $args );
 
$team_name = ucfirst($coach_team_object[0]->post_title) ;
$team_id = ucfirst($coach_team_object[0]->ID) ;

echo "<br/>";
echo  $name ;
echo "<br/>";
echo $team_name ;
echo "<br/>";
echo $team_id;

exit;

foreach($pools as $pool) 
{
// args
$args = array(
	'orderby' => 'px_event_from_date',
    'order' => 'ASC',
	'numberposts'	=> -1,
	'post_type'		=> 'events',
	 'tax_query' => array(
            'taxonomy' => 'event-pool',
            'field' => 'name',
            'terms' => $pool->name
        ),
	'meta_query'	=> array(
		'relation'		=> 'AND',
		array(
			'key'		=> 'fixture',
			'value'		=> 'match',
			'compare'	=> '='
		),
		array(
			'key'		=> 'select_tournament',
			'value'		=> 1492,
			'compare'	=> '='
		)
	)
);


// query
$the_query = new WP_Query( $args );

?>
<?php if( $the_query->have_posts() ): ?>
	<ul>
	<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<li>
				<?php
$term_list = wp_get_post_terms($post->ID, 'event-pool', array("fields" => "names"));

if (in_array($pool->name, $term_list)) {
	 the_title();
}else
{
	continue;
}


    // do something
				
				 ?>
		</li>
	<?php endwhile; ?>
	</ul>
<?php 
else:

echo "NO record found" ;

endif; 
 wp_reset_query();	
	 
}
	 exit; 
	 
	 

  //Getting pool base data
   
   $pools = get_terms( 'event-pool', array(
     'orderby'    => 'slug',
     'hide_empty' => 1
     ) ); 
	

    ?>
         <?php 
				 
		  $tournamentID = $_GET['tournament_id'] ;     
		 foreach( $pools as $pool ) {
	echo "<table class='pool_table table table-condensed table_D3D3D3'><thead><tr><th colspan='11' style='text-align:center;border-bottom:1px solid white'><span class='box1'>";
    echo $pool->name ;
	echo "</span></th></tr>";
    echo "<tr><th><span class='box1'>Game</span></th><th><span class='box1'>Day</span></th><th><span class='box1'>Date</span></th><th><span class='box1'>Time</span></th><th><span class='box1'>Venue</span></th>
	<th><span class='box1'>Field</span></th><th><span class='box1'>Team</span></th><th><span class='box1'>Score</span></th>
	<th><span class='box1'>Vs</span></th><th><span class='box1'>Score</span></th><th><span class='box1'>Team</span></th>
	</tr></thead>"; 
	$args = array(
            'event-pool' => $pool->name,
  //          'orderby' => 'title',
			'orderby' => 'px_event_from_date',
            'order' => 'ASC',
            'post_type' => 'events',
            'posts_per_page' => -1,
			'post_parent' => $tournamentID
        );
		wp_reset_query();
        $term_posts = new WP_Query($args);
        
		$index = 0 ;
		
        /** Do something with all of the posts */
        if($term_posts->have_posts()) : while ($term_posts->have_posts()) : $term_posts->the_post();
 	    
		$index ++ ;
		
                $id =  get_the_ID();
               				
				 $event_from_date = get_post_meta($id, "px_event_from_date", true);
                 $event_field =  get_post_meta($id,"event_field",true);
				 $team1_code =  get_post_meta($id,"team1_code",true);
                 $team2_code =  get_post_meta($id,"team2_code",true);
                 $team1_score =  get_post_meta($id,"team1_score",true);
                 $team2_score =  get_post_meta($id,"team2_score",true);

				 $post_xml = get_post_meta($id, "px_event_meta", true);	
                                if ( $post_xml <> "" ) {
                                    $px_event_meta = new SimpleXMLElement($post_xml);
                                    $team1_row = px_get_term_object($px_event_meta->var_pb_event_team1);
                                    $team2_row = px_get_term_object($px_event_meta->var_pb_event_team2);
                                }
				
			?><tbody>
			  <tr>
              <td><?php echo $index;  ?></td>
              <td><?php echo date_i18n('D', strtotime($event_from_date));  ?>
              </td>
              <td><?php echo date_i18n(get_option('date_format'), strtotime($event_from_date));  ?></td>
              <td> <?php
						print( date("g:i A", strtotime($px_event_meta->event_time)) );	
			   ?></td>
              <td><?php echo $px_event_meta->event_address;  ?></td>
              <td><?php echo $event_field ;  ?></td>
              <td><?php echo $team1_row->name ;  ?></td>
              <td><?php echo $team1_score ; ?></td>
              <td><?php echo $team1_code." ".$team2_code ;  ?></td>
              <td><?php echo $team2_score ;  ?></td>
              <td><?php echo $team2_row->name ; ?></td>

              </tr>  
              </tbody>
            <?php
		   
            endwhile;
        endif;
	 
echo "</table>";
}
   
	    
		  ?>
    <?php 


	
  //Getting pool base data

			
  
 get_footer();?>
<!-- Columns End -->
<?php
/*
Template Name: Login Template
*/

$tournamentID = $_GET['tournament_id'];
$ageClass = $_GET['ageClass'];
$price = $_GET['price'];


 if(is_user_logged_in())
 {
    if($tournamentID!="" && $ageClass!="" && $price!="")
      {
	   wp_redirect( get_page_link(1644)."?tournament_id=".$tournamentID."&ageClass=".$ageClass."&price=".$price );
	  	exit;
	  }
	  else
      {
	  wp_redirect(site_url());
	  exit;
	  }
 }
 get_header();
					wp_reset_query();
					$width =1100;
					$height = 556;
					$image_url ='';
					if (post_password_required()) {
						echo '<div class="rich_editor_text">'.px_password_form().'</div>';
					}else{
					$px_meta_page = px_meta_page('px_page_builder');
					if (count($px_meta_page) > 0) {
						 ?>
                         <?php if ( $px_meta_page->sidebar_layout->px_layout <> '' and $px_meta_page->sidebar_layout->px_layout <> "none" and $px_meta_page->sidebar_layout->px_layout == 'left') : ?>
                            <aside class="col-md-3">
                                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($px_meta_page->sidebar_layout->px_sidebar_left) ) : endif; ?>
                             </aside>
                        <?php endif; ?>
               	 		<div class="col-md-12">
      <div class="tab-content">

        <h2>Choose one of the two options below:</h2>
        <div class="row rowHack">
          <div class="col-md-6">
            <div class="panel panel-tcs">
              <div class="panel-body login_pg_boxes">
                <h3>Login</h3>
                <p class="text">Enter your email/username and password, then click login.</p>

				<?php


				  if( $px_meta_page->page_content == "on"  && get_the_content() <> '')
							{
 							echo '<div class="rich_editor_text pix-content-wrap">';
 								if( $px_meta_page->page_content == "on"  && get_the_content() <> ''){
									the_content();
									wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'Rocky' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
								}
 							echo '</div>';
						}


				?>

              </div>
            </div>

          </div>
          <div class="col-md-6">
            <div class="panel panel-tcs">
              <div class="panel-body login_pg_boxes">
                  <h3>First Time User</h3>
                <p class="text">Are you a player, parent or club coach and don't have an account?</p>
                <div class="row">
                  <div class="col-md-12 control-group">
                  <?php    if($tournamentID!="" && $ageClass!="" && $price!="") { ?>

                    <a href="<?php echo get_page_link(1811)."?tournament_id=".$tournamentID."&ageClass=".$ageClass."&price=".$price; ?>" class="join_now">Join Now</a>

                  <?php } else { ?>

                    <a href="<?php echo get_page_link(1811); ?>" class="join_now">Join Now</a>


				  <?php } ?>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    </div>
					<?php if ( $px_meta_page->sidebar_layout->px_layout <> '' and $px_meta_page->sidebar_layout->px_layout <> "none" and $px_meta_page->sidebar_layout->px_layout == 'right') : ?>
                            <aside class="col-md-3">
                                    <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($px_meta_page->sidebar_layout->px_sidebar_right) ) : endif; ?>
                             </aside>
                        <?php endif; ?>

             		<?php }else{

						px_page_title();
						 ?>


            		<div class="rich_editor_text pix-content-wrap">
					<?php while (have_posts()) : the_post();
							   $image_url = px_get_post_img_src($post->ID, $width, $height);
								if($image_url <> ''){
									echo '<figure class="featured-img"><a href="'.get_permalink().'" ><img src="'.$image_url.'" alt="" ></a></figure>';
								}
                            the_content();
							wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'Rocky' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) );
                        endwhile;
						if ( comments_open() ) {
					 		comments_template('', true);
						}
						wp_reset_query();
                    ?>
                	</div>
			<?php }
			}
		?>
<?php get_footer();?>
<!-- Columns End -->

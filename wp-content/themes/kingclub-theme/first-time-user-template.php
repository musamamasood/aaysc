<?php
/*
  Template Name: First Time User Template
*/
$tournamentID = ( isset($_GET['tournament_id']) ) ? $_GET['tournament_id'] : false;
$ageClass     = ( isset($_GET['ageClass']) ) ? $_GET['ageClass'] : false;
$price        = ( isset($_GET['price']) ) ? $_GET['price'] : false;
get_header(); ?>
    <div class="container">
        <div class="row">
        <?php if(have_posts()): while(have_posts()): the_post();?>
            <div class="col-md-12">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="col-md-12">
                <?php the_content(); ?>
            </div>
        <?php endwhile; endif; ?>
        </div>
    </div>
        <div class="col-md-12">
            <div class="tab-content">
                <?php //$action = "";
                // if ( $tournamentID != "" && $ageClass != "" && $price != "" ) {
                //     $pay = "YES";
                //     $paymentAction = get_page_link(1857) . "?tournament_id=" . $tournamentID . "&ageClass=" . $ageClass . "&price=" . $price;
                //     $action = $paymentAction;
                // } else {
                //     $registerAction = site_url() ."/register-user";
                //     $action = $registerAction;
                //     $pay = "NO";
                // }
                ?>
                <?php
                    if ( $_POST ):
                        $FirstName = ($_POST['FirstName'])?$_POST['FirstName']:'';
                        $LastName = ($_POST['LastName'])?$_POST['LastName']:'';
                        $Address1 = ($_POST['Address1'])?$_POST['Address1']:'';
                        $Address2 = ($_POST['Address2'])?$_POST['Address2']:'';
                        $City = $_POST['City'];
                        $State = $_POST['State'];
                        $Zip = $_POST['Zip'];
                        $MobilePhone = $_POST['MobilePhone'];
                        $UserName = $_POST['UserName'];
                        $UserEmail = $_POST['UserEmail'];
                        $Password = $_POST['Password'];

                        $userdata = array(
                            'user_login'  =>  $UserName,
                            'user_pass'   =>  $Password,
                            'user_email'  =>  $UserEmail,
                            'first_name' => $FirstName,
                            'last_name'  => $LastName,
                            'role' => 'coach'
                        );

                        $user_id = wp_insert_user( $userdata ) ;
                        //On success
                        if ( is_wp_error( $user_id ) ) {
                            $_REQUEST['error'] = "User Registration failed!";
                        }

                        //Inserting User Meta Data
                        update_user_meta( $user_id, 'address1', $Address1 );
                        update_user_meta( $user_id, 'address2', $Address2 );
                        update_user_meta( $user_id, 'city', $City );
                        update_user_meta( $user_id, 'state', $State );
                        update_user_meta( $user_id, 'zip_code', $Zip );
                        update_user_meta( $user_id, 'mob_phone', $MobilePhone );
                    else:
                ?>
                <form method="post" id="CreateCoachAccountForm" action="" novalidate="novalidate">
                    <div class="panel panel-tcs">
                        <div class="panel-body">
                            <?php if (isset($_REQUEST['error'])): ?>
                                <h4 style="width: 100%; display:inline-block; color: #FF0000; text-align: center; margin-bottom: 20px;"><?php echo str_replace( "_", " ",$_REQUEST['error']); ?></h4>
                            <?php endif; ?>

                            <div style="display:none;" id="errorMessageBox" class="alert validation-alert">
                                <h4>Please correct the following errors:</h4>
                                <ul id="errorList" style="display: none;"></ul>
                            </div>

                            <div class="row">
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="FirstName">First Name*</label>
                                    <input type="text" value="" placeholder="First Name" class="form-control" name="FirstName" id="FirstName">
                                </div>
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="LastName">Last Name*</label>
                                    <input type="text" value="" placeholder="Last Name" class="form-control" name="LastName" id="LastName">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="Address1">Address 1*</label>
                                    <input type="text" value="" placeholder="Address 1" class="form-control" name="Address1" id="Address1">
                                </div>
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="Address2">Address 2</label>
                                    <input type="text" value="" placeholder="Address 2" class="form-control" name="Address2" id="Address2">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="City">City*</label>
                                    <input type="text" value="" placeholder="City" class="form-control" name="City" id="City">
                                </div>
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="state">State*</label>
                                    <select name="State" id="State" class="form-control">
                                        <option selected="selected" disabled>Select State</option>
                                        <option value="AK">Alaska              </option>
                                        <option value="AL">Alabama             </option>
                                        <option value="AR">Arkansas            </option>
                                        <option value="AZ">Arizona             </option>
                                        <option value="CA">California          </option>
                                        <option value="CN">Canada               </option>
                                        <option value="CO">Colorado            </option>
                                        <option value="CT">Connecticut         </option>
                                        <option value="DC">Washington D.C.     </option>
                                        <option value="DE">Delaware            </option>
                                        <option value="FL">Florida             </option>
                                        <option value="GA">Georgia             </option>
                                        <option value="GU">Guam                 </option>
                                        <option value="HI">Hawaii              </option>
                                        <option value="IA">Iowa                </option>
                                        <option value="ID">Idaho               </option>
                                        <option value="IL">Illinois            </option>
                                        <option value="IN">Indiana             </option>
                                        <option value="KS">Kansas              </option>
                                        <option value="KY">Kentucky            </option>
                                        <option value="LA">Louisiana           </option>
                                        <option value="MA">Massachusetts       </option>
                                        <option value="MD">Maryland            </option>
                                        <option value="ME">Maine               </option>
                                        <option value="MI">Michigan            </option>
                                        <option value="MN">Minnesota           </option>
                                        <option value="MO">Missouri            </option>
                                        <option value="MS">Mississippi         </option>
                                        <option value="MT">Montana             </option>
                                        <option value="MX">Mexico               </option>
                                        <option value="NC">North Carolina      </option>
                                        <option value="ND">North Dakota        </option>
                                        <option value="NE">Nebraska            </option>
                                        <option value="NH">New Hampshire       </option>
                                        <option value="NJ">New Jersey          </option>
                                        <option value="NM">New Mexico          </option>
                                        <option value="NV">Nevada              </option>
                                        <option value="NY">New York            </option>
                                        <option value="OH">Ohio                </option>
                                        <option value="OK">Oklahoma            </option>
                                        <option value="OR">Oregon              </option>
                                        <option value="PA">Pennsylvania        </option>
                                        <option value="PR">Puerto Rico          </option>
                                        <option value="RI">Rhode Island        </option>
                                        <option value="SC">South Carolina      </option>
                                        <option value="SD">South Dakota        </option>
                                        <option value="TN">Tennessee           </option>
                                        <option value="TX">Texas               </option>
                                        <option value="UT">Utah                </option>
                                        <option value="VA">Virginia            </option>
                                        <option value="VE">Venezuela            </option>
                                        <option value="VT">Vermont             </option>
                                        <option value="WA">Washington          </option>
                                        <option value="WI">Wisconsin           </option>
                                        <option value="WV">West Virginia       </option>
                                        <option value="WY">Wyoming             </option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="Zip">Zip*</label>
                                    <input type="text" value="" placeholder="Zipcode" class="form-control" name="Zip" id="Zip">
                                </div>
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="MobilePhone">Mobile Phone*</label>
                                    <input type="text" value="" placeholder="(xxx) xxx-xxxx" class="form-control phoneMask" name="MobilePhone" id="MobilePhone">
                                </div>
                            </div>
                            <div class="row-list">
                                <h5>Login Information: </h5>
                                <p class="text">Your Username will be your Email Address.  Please remember your Username and Password. You must have this information in order to access your account in the future.</p>
                                <div class="row">
                                    <div class="col-md-6 control-group">
                                        <label class="control-label" for="UserName">User Name*</label>
                                        <input type="text" autofocus="" value="" placeholder="Coach User Name" class="form-control" name="UserName" id="UserName">
                                    </div>
                                    <div class="col-md-6 control-group">
                                        <label class="control-label" for="UserEmail">User Email*</label>
                                        <input type="text" value="" placeholder="Coach User Email" class="form-control" name="UserEmail" id="UserEmail">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 control-group">
                                        <label for="Password" class="control-label">Password*</label>
                                        <div class="controls">
                                            <input type="password" placeholder="Password" name="Password" id="Password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6 control-group">
                                        <label for="ConfirmPassword" class="control-label">Confirm Password*</label>
                                        <div class="controls">
                                            <input type="password" placeholder="Confirm Password" name="ConfirmPassword" id="ConfirmPassword" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="panel panel-tcs">
                        <div class="panel-body">
                            <h3>Team Information</h3>
                            <div class="row">
                                <div class="col-md-12 control-group">
                                    <label class="control-label" for="TeamName">Team Name*</label>
                                    <input type="text" value="" placeholder="Team Name" class="form-control" name="TeamName" id="TeamName">
                                </div>
                                <div class="col-md-6 control-group">
                                    <label class="control-label" for="sport">Sport*</label>
                                    <select name="SportId" id="SportId" class="form-control"><option value="">Select</option>
                                        <option value="baseball">Baseball</option>
                                        <option value="boys_basketball">Boys Basketball</option>
                                        <option value="drift">Drift </option>
                                        <option value="girls_basketball">Girls Basketball</option>
                                        <option value="girls_fastpitch">Girls Fastpitch</option>
                                        <option value="hockey_inline">Hockey-Inline</option>
                                        <option value="slowpitch">Slowpitch</option>
                                        <option value="soccer">Soccer</option>
                                        <option value="volleyball">Volleyball</option>
                                    </select>
                                </div>
                                <div class="col-md-6 control-group">
                                <?php
                                if ($ageClass) $ageClass = aaysc_tournament_common::clean_age_group($ageClass);
                                $term_children = aaysc_tournament_common::term_children();
                                ?>
                                    <label class="control-label" for="type">Division*</label>
                                    <select name="AgeGroupId">
                                        <option selected disabled>Select Division</option>
                                        <?php foreach ($term_children as $children): ?>
                                            <?php $clean_name = aaysc_tournament_common::clean_age_group($children->name);?>
                                            <option <?=($ageClass==$clean_name)?'selected':'';?> value="<?=$clean_name;?>"><?=$children->name;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" value="<?php echo $tournamentID; ?>"  name="tournament_id" id="tournament_id">
                                    <input type="hidden" value="<?php echo $price; ?>"  name="price" id="price">
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="text-muted"><small>*Required Field</small></p>
                        </div>
                        <div class="col-sm-6">
                            <p class="text-right">
                            <?php if ($pay == "NO") { ?>
                                <p class="form-submit text-right"><input type="submit" value="Register" class="submit" id="proceed" name="submit"> </p>
                            <?php } else { ?>
                                <p class="form-submit text-right"><input type="submit" value="Proceed" class="submit" id="proceed" name="submit"> </p>
                            <?php } ?>
                            </p>
                        </div>
                    </div>
                </form>
                <?php endif; ?>
            </div>
        </div>
<?php get_footer(); ?>

<?php
/*
  Template Name: Tournament Registeration Template
 */
$tournamentID = $_GET['tournament_id'];
$tournamentTitle = get_the_title( (int)$tournamentID );
$ageClass = $_GET['ageClass'];
$price = $_GET['price'];

if (!is_user_logged_in()) {
    if ($tournamentID != "" && $ageClass != "" && $price != "") {
        wp_redirect(get_page_link(1794) . "?tournament_id=" . $tournamentID . "&ageClass=" . $ageClass . "&price=" . $price);
        exit;
    } else {
        wp_redirect(get_page_link(1794));
        exit;
    }
} else {
    global $wpdb;
    $user_id = get_current_user_id();
    $check_team = $wpdb->get_row("SELECT * FROM wp_t_registration WHERE user_id = '$user_id' AND t_id='$tournamentID' AND age = '$ageClass'");
    if (empty($check_team)) {
        $args = array(
            'post_type' => 'team',
            'author' => $user_id
        );
        $team = new WP_Query($args);
        $current_user = wp_get_current_user();
        $res = $wpdb->insert('wp_t_registration', array(
            'user_id' => $user_id,
            'coach_name' => $current_user->user_firstname." ".$current_user->user_lastname,
            'team_id' => $team->post->ID,
            'team_name' => $team->post->post_title,
            't_id' => $tournamentID,
            'rank_id' => 0,
            'age' => $ageClass,
            'age_group' => $ageClass,
            'price' => $price,
            'date' => date ("Y-m-d"),
            'payment_status' => 0)
        );
        $message_template = file_get_contents(ABSPATH.'/wp-content/themes/kingclub-theme/email.html');
        $startdate = date( 'd/m/Y', strtotime( get_field('start_date', $tournamentID) ) );
        $enddate = date( 'd/m/Y', strtotime( get_field('end_date', $tournamentID) ) );
        $message = str_ireplace('[ageclass]', str_replace('_', ' ', $ageClass), $message_template);
        $message = str_ireplace('[startdate]', $startdate, $message);
        $message = str_ireplace('[enddate]', $enddate, $message);
        $message = str_ireplace('[tournamentname]', $tournamentTitle, $message);

        $headers  = 'From: AAYSC <steve@aaysc.com>' . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        wp_mail($current_user->user_email, 'Registration Confirmation', $message, $headers);
        //nicole@aaysc.com, ralph@aaysc.com, get_option('admin_email')
        wp_mail("masood.u@allshoreresources.com, steve@aaysc.com, nicole@aaysc.com, ralph@aaysc.com", 'New Team Registration!', "New team ( ".$team->post->post_title." ), age group: ($ageClass) and tournament: ($tournamentTitle) has been registered for Tournament successfully.");
        $payment = "unclear";
        $message = "Click the button below to pay for this tournament";
    } else {
        if ($check_team->payment_status==0){
            $payment = "unclear";
            $message = 'Sorry, you have already sign-up for the "Tournament", but your payment is due. Please click on the button below to pay for this tournament';
        }
        else{
            $payment=  "clear";
            $message = 'Sorry.  You have already registered for the "Tournament"';
        }

    }
}


get_header();

wp_reset_query();
$width = 1100;
$height = 556;
$image_url = '';
if (post_password_required()) {
    echo '<div class="rich_editor_text">' . px_password_form() . '</div>';
} else {
    $px_meta_page = px_meta_page('px_page_builder');
    if (count($px_meta_page) > 0) {
        ?>
        <?php if ($px_meta_page->sidebar_layout->px_layout <> '' and $px_meta_page->sidebar_layout->px_layout <> "none" and $px_meta_page->sidebar_layout->px_layout == 'left') : ?>
            <aside class="col-md-3 up">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar($px_meta_page->sidebar_layout->px_sidebar_left)) : endif; ?>

            </aside>
        <?php endif; ?>
        <div class="<?php echo px_meta_content_class(); ?> flow_sm">
            <header class="pix-heading-title">
                <p style="background-color: #e41d38 !important; color: #fff; padding: 0.2em 3em 0.2em 0.5em;" class="pix-heading-color pix-section-title">WARNING! THE KC SOX HAVE NOT PAID FOR THIS TOURNAMENT</p>
                <p style="float: right;padding-top: 10px; margin: 0;"><i class="fa fa-calendar"></i> <?php echo current_time( 'D, M d, Y' ); // Sun, July 30, 2015 ?></span>
            </header>
            <div class="element_size_100">
                <h4 style="float:left; width:100%;text-transform: initial;"><?php echo $message;?></h4>
                <h3 style="float:left; width:100%; color: #002D56;margin-bottom: 0;"><?php echo $tournamentTitle; ?></h3>
                <h3 style="float:left; width:100%; color: #e41d38;"><?php echo '(' . str_replace('_', ' ', $ageClass) . ')'; ?></h3>
            </div>
        </div>
        <div class="<?php echo px_meta_content_class(); ?> flow_sm">
            <?php
            if ($payment=="unclear"){
            ?>
            <div style="float:left; width:100%; margin-bottom:25px;">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">

                    <input name="cmd" type="hidden" value="_xclick" />
                    <input name="business" type="hidden" value="steve@aaysc.com" />
                    <input name="return" type="hidden" value="http://www.aaysc.com/success/?price=<?php echo $price; ?>&userID=<?php echo $user_id; ?>&tournament_id=<?php echo $tournamentID; ?>&ageClass=<?php echo $ageClass; ?>" />
                    <input name="item_name" type="hidden" value="Tournament Fee" />
                    <input name="amount" type="hidden" value="<?php echo $price; ?>" />
                    <input type="hidden" name="quantity" value="1">
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="submit" name="submit" border="0" value="Pay Now" style="color: #fff;background-color: #e41d38;padding: 0.5em;   border: none;text-transform: uppercase;">
                    <!-- <input type="image" name="submit" border="0"
                           src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
                           alt="PayPal - The safer, easier way to pay online"> -->
                    <a style="color: #fff;background-color: #002D56; padding: 0.5em 1em; text-transform: uppercase;" href="<?php echo get_page_link($tournamentID); ?>">Continue On</a>
                </form>
            </div>
            <?php
            }
            else{ ?>
            <?php }
            wp_reset_query();
            $image_url = px_get_post_img_src($post->ID, $width, $height);
            if ($image_url <> '') {
                echo '<figure class="featured-img"><a href="' . get_permalink() . '" ><img src="' . $image_url . '" alt="" ></a></figure>';
            }
            if ($px_meta_page->page_content == "on" && get_the_content() <> '') {
                echo '<div class="rich_editor_text pix-content-wrap">';
                if ($px_meta_page->page_content == "on" && get_the_content() <> '') {
                    the_content();
                    ?>
                    <?php
                    wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'Rocky') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>'));
                }
                echo '</div>';
            }
            global $px_counter_node;
            foreach ($px_meta_page->children() as $px_node) {
                if ($px_node->getName() == "blog") {
                    if (!isset($_SESSION["px_page_back"]) || isset($_SESSION["px_page_back"])) {
                        $_SESSION["px_page_back"] = get_the_ID();
                    }
                    $px_counter_node++;
                    get_template_part('page_blog', 'page');
                } else if ($px_node->getName() == "gallery_albums") {
                    $px_counter_node++;
                    if ($px_node->px_gal_album_cat <> "") {
                        get_template_part('page_gallery_albums', 'page');
                    }
                } else if ($px_node->getName() == "gallery") {
                    $px_counter_node++;
                    if ($px_node->album <> "" and $px_node->album <> "0") {
                        get_template_part('page_gallery', 'page');
                    }
                } else if ($px_node->getName() == "slider") {
                    $px_counter_node++;
                    if ($px_node->slider <> "" and $px_node->slider <> "0") {
                        get_template_part('page_slider', 'page');
                    }
                } else if ($px_node->getName() == "event") {
                    if (!isset($_SESSION["px_page_back_event"]) || isset($_SESSION["px_page_back_event"])) {
                        $_SESSION["px_page_back_event"] = get_the_ID();
                    }
                    $px_counter_node++;
                    get_template_part('page_event', 'page');
                } elseif ($px_node->getName() == "team") {
                    $px_counter_node++;
                    get_template_part('page_team', 'page');
                } elseif ($px_node->getName() == "map") {
                    $px_counter_node++;
                    echo px_map_page();
                } elseif ($px_node->getName() == "fixtures") {
                    $px_counter_node++;
                    px_fixtures_page();
                } elseif ($px_node->getName() == "contact") {
                    $px_counter_node++;
                    get_template_part('page_contact', 'page');
                } elseif ($px_node->getName() == "column") {
                    $px_counter_node++;
                    px_column_page();
                } elseif ($px_node->getName() == "pointtable") {
                    $px_counter_node++;
                    get_template_part('page_pointtable', 'page');
                }
            }
            wp_reset_query();
            $tournamentID = $_GET['tournament_id'];
            ?>
        </div>
        <?php
        if ($px_meta_page->sidebar_layout->px_layout <> '' or $px_meta_page->sidebar_layout->px_layout <> "none" or $px_meta_page->sidebar_layout->px_layout == 'right') :
            ?>
            <aside class="col-md-3 down">

                <?php
                dynamic_sidebar('tour_register_sidebar');
                ?>
            </aside>
        <?php endif; ?>

        <?php
    }else {
        px_page_title();
        ?>
        <div class="rich_editor_text pix-content-wrap">
            <?php
            while (have_posts()) : the_post();
                $image_url = px_get_post_img_src($post->ID, $width, $height);
                if ($image_url <> '') {
                    echo '<figure class="featured-img"><a href="' . get_permalink() . '" ><img src="' . $image_url . '" alt="" ></a></figure>';
                }
                the_content();
                wp_link_pages(array('before' => '<div class="page-links"><span class="page-links-title">' . __('Pages:', 'Rocky') . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>'));
            endwhile;
            if (comments_open()) {
                comments_template('', true);
            }
            wp_reset_query();
            ?>
        </div>
        <?php
    }
}

get_footer();
?>

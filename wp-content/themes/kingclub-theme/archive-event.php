<?php error_reporting(0);
/**
 * Template Name: Tournament Schedule
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

 get_header();
global	$wpdb;
$today	=	date('Y-m-d');
if(isset( $_POST['find_event'] ) )
{
        $areas                  =	array();
	$classifications	=	$_POST['classifications'];
	$areas1                  =	$_POST['locations'];
	$types                  =	$_POST['types'];
	$formats                =	$_POST['formats'];
	$years                  =	$_POST['seasons'];
	$s_date                 =	$_POST['start_date'];
	$e_date                 =	$_POST['end_date'];
	$s_date =   date("Y-m-d", strtotime($s_date));
	$e_date =   date("Y-m-d", strtotime($e_date));
	$posts	=	array();

	if( count($types) > 0 )
	{
		// get event TYPES posts
		$all_types	=	'';
		foreach($types as $type )
		{
			$all_types .= "'" . $type . "',";
		}
		$all_types	=	 substr($all_types, 0, -1);

		$query=	"SELECT * FROM $wpdb->postmeta WHERE meta_key = 'event_type' AND meta_value IN($all_types)";
		$dates_pids = $wpdb->get_results($query, OBJECT);

		if( count( $posts ) > 0 )
		{
			foreach( $dates_pids as $dpid )
			{
				if (in_array($dpid->post_id, $posts))
					array_push($posts,$dpid->post_id);
			}
		}
		else
		{
			foreach( $dates_pids as $dpid )
			{
				if (!in_array($dpid->post_id, $posts))
					array_push($posts,$dpid->post_id);
			}
		}
	}

	if(count($areas1) > 0)
        {
            foreach($areas1 as $state){
                $args = array(
                    'post_type' => 'event',
                    'tax_query' => array(
                        array(
                        'taxonomy' => 'state',
                        'field' => 'id',
                        'terms' => $state
                         )
                      )
                    );
                $query = new WP_Query( $args );

                if($query->have_posts()){

                    while($query->have_posts()){

                        $query->the_post();
                        $tpostid    =   get_the_ID();
                        if( count( $posts ) > 0 )
                        {
                            if (in_array($tpostid, $posts))
                                array_push($posts,$tpostid);
                        }
                        else
                        {
                             if (!in_array($tpostid, $posts))
                                array_push($posts,$tpostid);
                        }

                    }
                }
            }
        }
	// get event FORMAT posts
	if( count($formats) > 0 )
	{
		$all_formats	=	'';
		foreach($formats as $format )
		{
			$all_formats .= "'" . $format . "',";
		}
		$all_formats	=	 substr($all_formats, 0, -1);
		$query=	"SELECT * FROM $wpdb->postmeta WHERE meta_key = 'format' AND meta_value IN($all_formats)";
		$format_pids = $wpdb->get_results($query, OBJECT);
		if( count( $posts ) > 0 )
		{
			foreach( $format_pids as $fpid )
			{
				if (in_array($fpid->post_id, $posts))
					array_push($posts,$fpid->post_id);
			}
		}
		else
		{
			foreach( $format_pids as $fpid )
			{
				if (!in_array($fpid->post_id, $posts))
					array_push($posts,$fpid->post_id);
			}
		}
	}



	// get event Date Range posts
	if( $s_date != '' or $e_date !='' )
	{
		//$query=	"SELECT * FROM $wpdb->postmeta WHERE meta_value >= $s_date AND meta_value <= $e_date AND meta_key='start_date'";
		$query=	"SELECT ID FROM $wpdb->posts WHERE date(post_date) >= '$s_date' AND date(post_date) <= '$e_date' ";
		$classes_pids = $wpdb->get_results($query, OBJECT);
		if( count( $posts ) > 0 )
		{
			foreach( $classes_pids as $cpid )
			{
				if (in_array($cpid->ID, $posts))
					array_push($posts,$cpid->ID);
			}
		}
		else
		{
			foreach( $classes_pids as $cpid )
			{
				if (!in_array($cpid->ID, $posts))
					array_push($posts,$cpid->ID);
			}
		}
	}
	// get event AGE GROUPS posts
	if( count($classifications) > 0 )
	{
		$all_classes	=	'';
		foreach($classifications as $classification )
		{
			$all_classes .= "'" . $classification . "',";
		}
		$all_classes	=	 substr($all_classes, 0, -1);
		$query=	"SELECT * FROM $wpdb->postmeta WHERE meta_key IN($all_classes)";
		$classes_pids = $wpdb->get_results($query, OBJECT);
		if( count( $posts ) > 0 )
		{
			foreach( $classes_pids as $cpid )
			{
				if (in_array($cpid->post_id, $posts))
					array_push($posts,$cpid->post_id);
			}
		}
		else
		{
			foreach( $classes_pids as $cpid )
			{
				if (!in_array($cpid->post_id, $posts))
					array_push($posts,$cpid->post_id);
			}
		}
	}




	// get event Seasons posts
	if( count($years) > 0 )
	{
		$all_years	=	'';
		if( is_array( $years ) )
		{
			foreach($years as $year )
			{
				$all_years .= "'" . $year . "',";
			}
			$all_years	=	 substr($all_years, 0, -1);
		}
		else
		{
			$all_years	=	$years;
		}
		$query=	"SELECT ID FROM $wpdb->posts WHERE YEAR(post_date) IN($all_years) AND post_type = 'event'  and post_parent = '0'";
		$years_pids = $wpdb->get_results($query, OBJECT);
		if( $s_date != '' or $e_date !='' )
		{
			foreach( $years_pids as $ypid )
			{
				if (in_array($ypid->ID, $posts))
					array_push($posts,$ypid->ID);
			}
		}
		else
		{
			foreach( $years_pids as $ypid )
			{
				if (!in_array($ypid->ID, $posts))
					array_push($posts,$ypid->ID);
			}
		}
	}

	if( count( $areas ) > 0 &&  count($types) == 0 && count($formats) == 0 && count($years) == 0 && count($classifications) == 0 && $s_date == '' && $e_date == '')
	{
		$query=	"SELECT * FROM $wpdb->posts WHERE post_type = 'event' AND date(post_date) >= '$today' and post_status = 'publish' and post_parent = '0'";
		$events = $wpdb->get_results($query, OBJECT);
	}
	else
	{
		$posts	=	implode("," , $posts);

		$query=	"SELECT * FROM $wpdb->posts WHERE ID IN($posts) AND post_type = 'event' AND date(post_date) >= '$today' AND post_status = 'publish'  and post_parent = '0'";
		$events = $wpdb->get_results($query, OBJECT);
	}
}
else
{
	$query=	"SELECT * FROM $wpdb->posts WHERE post_type = 'event' AND date(post_date) >= '$today' and post_status = 'publish' and post_parent = '0'";
	$events = $wpdb->get_results($query, OBJECT);
}
?>
<style type="text/css">
	.apeel_input
	{
		border: solid 1px !important;
	}
	select
	{
		width:250px;
		height:130px !important;
	}
	.panel
	{
		width:auto;
		height:120px;
	}
	.panel-heading
	{
		color:#EB162D !important;
                text-transform: uppercase;
                font-size:18px !important;padding: 3px 10px !important;
	}
        .panel-body{
            font-size:13px;
            height:100px;

        }
        .col-md-3>.panel-default>.panel-body select{
            width:185px !important;
        }
        table > thead > tr > th, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > td, table > tfoot > tr > td{
            border:none;
        }
        table > tbody > tr > th{
            font-size:12px !important;
            font-weight: normal;
        }
        table > tbody > tr > td{
            border-right:none;
            border-top:none;
            border-left:none;
            border-bottom:solid 1px #E3E3E3;
            padding-top:5px;
            padding-bottom: 5px;
        }
</style>
<div class="container">
    <form method="post" name="find" id="findform">
<div class="row">
    <div class="col-md-8">
    	<div class="col-md-4">
            <div class="panel panel-default">
            <div class="panel-heading">CLASSIFICATION</div>
            <div class="panel-body">
                <select multiple="multiple" name="classifications[]" style="overflow:scroll;">
           <?php
				$classifications = get_terms( 'age_groups', 'orderby=count&hide_empty=0&parent=0' );
				foreach( $classifications as $classification)
				{
					?>
                    	<option value="age_group_parent_<?php echo $classification->term_id; ?>"><?php echo $classification->name; ?></option>
                        <?php
					$sub_classes = get_terms( 'age_groups', 'orderby=count&hide_empty=0&parent=' . $classification->term_id );
					foreach( $sub_classes as $sub_class)
					{
					?>
                    	<option value="age_group_child_<?php echo $classification->term_id; ?>_<?php echo $sub_class->term_id; ?>" style="margin-left:10px;"><?php echo $sub_class->name; ?></option>
                    <?php
					}
				}
		   ?>
           </select>
            </div>
            </div>
        </div>
    	<div class="col-md-4">
            <div class="panel panel-default">
            <div class="panel-heading">STATE / REGION</div>
            <div class="panel-body">
            <?php
            /**
             * Modified on 4th Aughtst, 2015
             **/
//				$query = "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'event_location' GROUP BY meta_value";
//				$locations = $wpdb->get_results($query, ARRAY_A);
                                $locations = get_terms( 'state', 'orderby=name&hide_empty=0&parent=0' );
			?>
            <select multiple="multiple" name="locations[]">
           <?php
 				foreach( $locations as $location)
				{
//						$location_name	=	'';
//                        $arr1	=	get_post_meta(  $location['post_id'], 'event_location', true ) ;
//							if(count($arr1) > 0)
//								$location_name	=	$arr1['address'];
					?>
                    	<option value="<?php echo $location->term_id; ?>"><?php echo  $location->name; ?></option>
                    <?php
				}
		   ?>
           </select>
            </div>
            </div>
        </div>
    	<div class="col-md-3">
            <div class="panel panel-default">
            <div class="panel-heading">TYPE</div>
            <div class="panel-body">
            <?php
				$query = "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'event_type' GROUP BY meta_value";
				$types = $wpdb->get_results($query, OBJECT);
			?>
            <select multiple="multiple" name="types[]">
           <?php
 				foreach( $types as $type)
				{
					?>
                    	<option value="<?php echo $type->meta_value; ?>"><?php echo  $type->meta_value; ?></option>
                    <?php
				}
		   ?>
           </select>
            </div>
            </div>
        </div>
    	<div class="col-md-4">
            <div class="panel panel-default">
            <div class="panel-heading">EVENT FORMAT</div>
            <div class="panel-body">
            <?php
				$query = "SELECT * FROM $wpdb->postmeta WHERE meta_key = 'format' GROUP BY meta_value";
				$formats = $wpdb->get_results($query, OBJECT);
			?>
            <select multiple="multiple" name="formats[]">
           <?php
 				foreach( $formats as $format)
				{
					?>
                    	<option value="<?php echo $format->meta_value; ?>"><?php echo  $format->meta_value; ?></option>
                    <?php
				}
		   ?>
           </select>
            </div>
            </div>
        </div>
    	<div class="col-md-4">
            <div class="panel panel-default">
            <div class="panel-heading">DATE RANGE</div>
            <div class="panel-body" style="padding-top:50px !important; background-color: #E5E7E8; height:90px;  min-height: 90px;">
            	<div class="pull-left">
                    From<br>
                    <input type="text" id="start_date" name="start_date" style="border:1px solid #CED0D2; width:75px; height:20px;" placeholder="From"/><img id="startdate" src="<?php echo get_bloginfo('template_directory');?>/images/start-icon.png">
                </div>
                <div class="pull-right">
                    To<Br>
            	<input type="text" id="end_date" name="end_date" style="border:1px solid #CED0D2;  width:75px;height:20px;" placeholder="To"/><img id="enddate" src="<?php echo get_bloginfo('template_directory');?>/images/end-icon.png" style="margin-right: 5px;">
                </div>
            </div>
            </div>
        </div>
    	<div class="col-md-3">
            <div class="panel panel-default" style="height: 80px !important">
            <div class="panel-heading">SEASON</div>
            <div class="panel-body" style="height:50px !important;">
            <?php
				$query = "SELECT YEAR(post_date) AS season FROM $wpdb->posts WHERE post_type = 'event' AND post_status = 'publish' GROUP BY YEAR(post_date)";
				$seasons = $wpdb->get_results($query, OBJECT);
            ?>
            <select multiple="multiple" style="height:50px !important;" name="seasons">
           <?php
 				foreach( $seasons as $season)
				{
					?>
                    	<option value="<?php echo $season->season; ?>"><?php echo  $season->season; ?></option>
                    <?php
				}
		   ?>
           </select>
            </div>
            </div>
        </div>
           <p style="margin-top:20px;" align="center">
           	<button type="submit" id="findevent" style="height: 30px;margin-left: -25px;margin-top: -17px;width: 27%; text-align:left !important; background-color:#EB162D;" name="find_event" class="btn btn-danger">
                    <span style="font-size:18px;">Find Event</span>
                </button>
           </p>
   </div>
    <div class="col-md-4"><img src="<?php echo get_bloginfo('template_directory');?>/images/banner-ad.png"></div>
    <div class="col-md-4" style="padding-top:3px">
            <div class="panel panel-default" style="height:130px;">
            <div class="panel-heading">TOURNAMENT ARCHIVE</div>
            <div class="panel-body">
            <?php
				$query=	"SELECT * FROM $wpdb->posts WHERE post_type = 'event' AND date(post_date) < '$today' and post_status = 'publish' and post_parent = '0'";
				$archives = $wpdb->get_results($query, OBJECT);
			?>
           <?php
 				foreach( $archives as $archive)
				{
					?>
                    	<div style="line-height:2">
                            <strong>
                                <a href="<?php echo site_url().'/event/'.$archive->post_name; ?>"><?php echo $archive->post_title; ?></a>
                            </strong>
                        </div>
                        <hr style="margin-bottom:3px;margin-top:3px;" />
                    <?php
				}
		   ?>

            </div>
            </div>
        </div>
</div>
</form>

    <?php
                $As     =   array();
                $AAs    =   array();
                $AAAs   =   array();
                $major  =   array();

		foreach($events as $event )
		{
        /*****************************************************************************
         *  THE CODE TO FILTER OUT THE AGE GROUPS
         *****************************************************************************/
            $age_groups	=	'';
            $groups = get_terms( 'age_groups', 'orderby=count&hide_empty=0&parent=0' );
            foreach( $groups as $age){
                $is_exist	=	get_post_meta( $event->ID, 'age_group_parent_' . $age->term_id, true );

                if( $is_exist != '' )
                {
                    $age_groups	.= $age->name . ",";
                    $groups_child = get_terms( 'age_groups', "orderby=count&hide_empty=0&parent=" . $age->term_id );
                    foreach($groups_child as $child )
                    {
                        $is_exist_child	=	get_post_meta( $event->ID, 'age_group_child_' . $age->term_id . '_' . $child->term_id , true );
                        if( $is_exist_child != '' ){
                            $findA   = "Under A";
                            $findAA  = "Under AA";
                            $findAAA = "Under AAA";
                            $findM   = "Under Major";
                            $findPA  = "Pitch A";
                            $findPAA  = "Pitch AA";
                            $findPAAA  = "Pitch AAA";
                            $findPM  = "Pitch Major";
                            if(preg_match("#$findA$#",$child->name)){
                                    array_push($As, $event);
                            }
                            if(preg_match("#$findPA$#",$child->name)){
                                    array_push($As, $event);
                            }
                            if(preg_match("#$findAA$#",$child->name)){
                                 array_push($AAs, $event);
                            }
                            if(preg_match("#$findPAA$#",$child->name)){
                                 array_push($AAs, $event);
                            }
                            if(preg_match("#$findAAA$#",$child->name)){
                                 array_push($AAAs, $event);
                            }
                            if(preg_match("#$findPAAA$#",$child->name)){
                                 array_push($AAAs, $event);
                            }
                            if(preg_match("#$findM$#",$child->name)){
                                 array_push($major, $event);
                            }
                            if(preg_match("#$findPM$#",$child->name)){
                                 array_push($major, $event);
                            }
                        }
                    }
                }
                else
                {

                }
            }

      	}

                function is_val_exists($needle, $haystack) {

                     foreach($haystack as $key => $element) {
                         if($element->ID    ==  $needle){
                             return $key;
                         }
                     }
                   return false;
                }
                    /*****************************************************************************
                     *  THE CODE TO FILTER OUT THE AGE GROUPS ENDS
                     *****************************************************************************/
                ?>
        <div class="row">
        <div class="col-md-12">
            <strong>Upcomming "A" Tournaments</strong>

            <table width="100%">
            	<tr style="background-color:#EB162D;color:#FFF;">
                    <th>Logo</th>
	                    <th width="80" align="center">Start Date</th>
						<th width="80" align="center">End Date</th>
	                    <th width="250">Tournament Name</th>
	                    <th>Venue</th>
	                    <th width="150">Format / Type</th>
						<th>Fee</th>
                    </tr>
                <?php
                $input = array_map("unserialize", array_unique(array_map("serialize", $As)));
                foreach ($input as $event){
				$address	=	'';
				$arr	=	get_post_meta( $event->ID, 'event_location', true ) ;
				if(count($arr) > 0)
				$address	=	 $arr['address'];
				if( count( $areas ) > 0 ){
					if(!in_array( $address, $areas ) )continue;
				}
				#$my_image = get_field('event_logo', $event->ID); ?>

				<?php // check if the post has a Post Thumbnail assigned to it.
                if ( has_post_thumbnail($event->ID) ) {
                	$tournamnet_logo = wp_get_attachment_image_src( get_post_thumbnail_id($event->ID), 'full' );
                    $tournamnet_logo = $tournamnet_logo[0];
                } else {  $tournamnet_logo = get_field('event_logo', $event->ID);
                        $tournamnet_logo = $tournamnet_logo['sizes']['px_media_1']; } ?>
                <tr>
                	<td><img src="<?=$tournamnet_logo;?>" width="70" class="img-responsive" style="border:solid 1px #e3e3e3;" /></td>
                    <td><?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'start_date', true )))  ?></td>
                	<td><?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'end_date', true )))  ?></td>
                	<td style="line-height:15px;">
						<a href="<?php echo $event->guid; ?>" style="color:#EB162D;font-size:12px;font-weight:bold;text-decoration:underline;">
						<?php echo $event->post_title; ?>
                        </a>
                        <div style="color:#999;font-size:11px;font-weight:bold;line-height: 15px;">
                        <?=$address; ?>
                        </div>
                    </td>
                    <td style="line-height:15px;padding-top:15px;">
			<?php

                        $venue = get_post_meta( $event->ID, 'event_venue', true ) ;
                        $venueval   =   explode(",",$venue);
                       if(count($venueval) > 2){
                           $counter =1 ;
                           foreach($venueval as $venuename){
                               if($counter == count($venueval))
                                   echo $venuename;
                               else
                                    echo $venuename.",";
                               if(($counter %2) == 0){
                                   echo "<BR>";
                               }
                            $counter++;
                           }
                       }else{
                           echo $venue;
                       }
						?>
                    </td>
                	<td>
                    <div style="line-height: 35px;">
                    		<?php echo get_post_meta( $event->ID, 'event_type', true ); ?>
                    </div>
                    <div style="color:#999; line-height: 0px;">
                    		<?php echo get_post_meta( $event->ID, 'format', true ); ?>
                    </div>
                    </td>
                	<td>
                    <div>
                    		<?php echo get_post_meta( $event->ID, 'fee_range', true ); ?>
                    </div>
                    </td><!--<td>POST HERE THE AGE GROUPS ACCORDINGLY</td>-->
                </tr>
    <?php
		}
	?>
            </table>
        </div>
  </div>


    <div class="row">
        <div class="col-md-12">
            <strong>Upcomming "AA" Tournaments</strong>
        	<table width="100%">
            	<tr style="background-color:#EB162D;color:#FFF;">
                    <th>
                    	Logo
                    </th>
                	<th width="80" align="center">
                    	Start Date
                    </th>
                	<th width="80" align="center">
                    	 End Date
                    </th>
                	<th width="250">
                    	Tournament Name
                    </th>
                	<th>
                    	Venue
                    </th>
                	<th width="150">
                    	Format / Type
                    </th>
                	<th>
                    	Fee
                    </th>
                	<!-- <th>
                    	Classification
                    </th> -->
                </tr>
                <?php
//                $uniquAs    = array_unique($As);
                $inputAAs = array_map("unserialize", array_unique(array_map("serialize", $AAs)));
                foreach ($inputAAs as $event){
			$address	=	'';
			$arr	=	get_post_meta( $event->ID, 'event_location', true ) ;
			if(count($arr) > 0)
			 $address	=	 $arr['address'];
			 if( count( $areas ) > 0 )
			 {
				 if(!in_array( $address, $areas ) )
					continue;
			 }
			$my_image = get_field('event_logo', $event->ID); ?>
                <tr>
                	<td><img src="<?php echo $my_image['sizes']['px_media_1']; ?>" width="70" class="img-responsive" style="border:solid 1px #e3e3e3;" /></td>
                    <td><?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'start_date', true )))  ?></td>
                	<td><?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'end_date', true )))  ?></td>
                	<td style="line-height:15px;">
						<a href="<?php echo $event->guid; ?>" style="color:#EB162D;font-size:12px;font-weight:bold;text-decoration:underline;"><?php echo $event->post_title; ?></a>
                        <div style="color:#999;font-size:11px;font-weight:bold;line-height: 15px;"><?=$address; ?></div>
                    <td style="line-height:15px;padding-top:15px;">
				<?php

                        $venue = get_post_meta( $event->ID, 'event_venue', true ) ;
                        $venueval   =   explode(",",$venue);
                       if(count($venueval) > 2){
                           $counter =1 ;
                           foreach($venueval as $venuename){
                               if($counter == count($venueval))
                                   echo $venuename;
                               else
                                    echo $venuename.",";
                               if(($counter %2) == 0){
                                   echo "<BR>";
                               }
                            $counter++;
                           }
                       }else{
                           echo $venue;
                       }
						?>
                    </td>
                	<td>
                    <div style="line-height: 35px;">
                    		<?php echo get_post_meta( $event->ID, 'event_type', true ); ?>
                    </div>
                    <div style="color:#999; line-height: 0px;">
                    		<?php echo get_post_meta( $event->ID, 'format', true ); ?>
                    </div>
                    </td>
                	<td>
                    <div>
                    		<?php echo get_post_meta( $event->ID, 'fee_range', true ); ?>
                    </div>
                    </td><!--
                	<td>



                                                    POST HERE THE AGE GROUPS ACCORDINGLY


                    </td>-->
                </tr>
    <?php
		}
	?>
            </table>
        </div>
  </div>



   <div class="row">
        <div class="col-md-12">
            <strong>Upcomming "AAA" Tournaments</strong>
        	<table width="100%">
            	<tr style="background-color:#EB162D;color:#FFF;">
                    <th>
                    	Logo
                    </th>
                	<th width="80" align="center">
                    	Start Date
                    </th>
                	<th width="80" align="center">
                    	 End Date
                    </th>
                	<th width="250">
                    	Tournament Name
                    </th>
                	<th>
                    	Venue
                    </th>
                	<th width="150">
                    	Format / Type
                    </th>
                	<th>
                    	Fee
                    </th>
                	<!-- <th>
                    	Classification
                    </th> -->
                </tr>
                <?php
//                $uniquAs    = array_unique($As);
                $inputAAAs = array_map("unserialize", array_unique(array_map("serialize", $AAAs)));
                foreach ($inputAAAs as $event){
			$address	=	'';
			$arr	=	get_post_meta( $event->ID, 'event_location', true ) ;
			if(count($arr) > 0)
			 $address	=	 $arr['address'];
			 if( count( $areas ) > 0 )
			 {
				 if(!in_array( $address, $areas ) )
					continue;
			 }

				//print_r($areas);
			$my_image = get_field('event_logo', $event->ID);
	?>
                <tr>
                	<td>
                    		 <img src="<?php echo $my_image['sizes']['px_media_1']; ?>" width="70" class="img-responsive" style="border:solid 1px #e3e3e3;" />
                    </td>
                    <td>
                        <?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'start_date', true )))  ?>
                    </td>
                	<td>
                            <?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'end_date', true )))  ?>
                    </td>
                	<td style="line-height:15px;">
						<a href="<?php echo $event->guid; ?>" style="color:#EB162D;font-size:12px;font-weight:bold;text-decoration:underline;">
						<?php echo $event->post_title; ?>
                        </a>
                        <div style="color:#999;font-size:11px;font-weight:bold;line-height: 15px;">
                        <?php
						 	echo $address;
						?>
                        </div>
                    </td>
                	<td style="line-height:15px;padding-top:15px;">
			<?php

                        $venue = get_post_meta( $event->ID, 'event_venue', true ) ;
                        $venueval   =   explode(",",$venue);
                       if(count($venueval) > 2){
                           $counter =1 ;
                           foreach($venueval as $venuename){
                               if($counter == count($venueval))
                                   echo $venuename;
                               else
                                    echo $venuename.",";
                               if(($counter %2) == 0){
                                   echo "<BR>";
                               }
                            $counter++;
                           }
                       }else{
                           echo $venue;
                       }
						?>
                    </td>
                	<td>
                     <div style="line-height: 35px;">
                    		<?php echo get_post_meta( $event->ID, 'event_type', true ); ?>
                    </div>
                    <div style="color:#999; line-height: 0px;">
                    		<?php echo get_post_meta( $event->ID, 'format', true ); ?>
                    </div>
                    </td>
                	<td>
                    <div>
                    		<?php echo get_post_meta( $event->ID, 'fee_range', true ); ?>
                    </div>
                    </td><!--
                	<td>



                                                    POST HERE THE AGE GROUPS ACCORDINGLY


                    </td>-->
                </tr>
    <?php
		}
	?>
            </table>
        </div>
  </div>



   <div class="row">
        <div class="col-md-12">
            <strong>Upcomming "Major" Tournaments</strong>
        	<table width="100%">
            	<tr style="background-color:#EB162D;color:#FFF;">
                    <th>
                    	Logo
                    </th>
                	<th width="80" align="center">
                    	Start Date
                    </th>
                	<th width="80" align="center">
                    	 End Date
                    </th>
                	<th width="250">
                    	Tournament Name
                    </th>
                	<th>
                    	Venue
                    </th>
                	<th width="150">
                    	Format / Type
                    </th>
                	<th>
                    	Fee
                    </th>
                	<!-- <th>
                    	Classification
                    </th> -->
                </tr>
                <?php
//                $uniquAs    = array_unique($As);
                $inputmajor = array_map("unserialize", array_unique(array_map("serialize", $major)));
                foreach ($inputmajor as $event){
			$address	=	'';
			$arr	=	get_post_meta( $event->ID, 'event_location', true ) ;
			if(count($arr) > 0)
			 $address	=	 $arr['address'];
			 if( count( $areas ) > 0 )
			 {
				 if(!in_array( $address, $areas ) )
					continue;
			 }

				//print_r($areas);
			$my_image = get_field('event_logo', $event->ID);
	?>
                <tr>
                    <td>
                         <img src="<?php echo $my_image['sizes']['px_media_1']; ?>" width="70" class="img-responsive" style="border:solid 1px #e3e3e3;" />
                    </td>
                    <td>
                        <?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'start_date', true )))  ?>
                    </td>
                	<td>
                            <?php echo date("m/d/y", strtotime(get_post_meta( $event->ID, 'end_date', true )))  ?>
                    </td>
                	<td style="line-height:15px;">
						<a href="<?php echo $event->guid; ?>" style="color:#EB162D;font-size:12px;font-weight:bold;text-decoration:underline;">
						<?php echo $event->post_title; ?>
                        </a>
                        <div style="color:#999;font-size:11px;font-weight:bold;line-height: 15px;">
                        <?php
						 	echo $address;
						?>
                        </div>
                    <td style="line-height:15px;padding-top:15px;">
			<?php

                        $venue = get_post_meta( $event->ID, 'event_venue', true ) ;
                        $venueval   =   explode(",",$venue);
                       if(count($venueval) > 2){
                           $counter =1 ;
                           foreach($venueval as $venuename){
                               if($counter == count($venueval))
                                   echo $venuename;
                               else
                                    echo $venuename.",";
                               if(($counter %2) == 0){
                                   echo "<BR>";
                               }
                            $counter++;
                           }
                       }else{
                           echo $venue;
                       }
						?>
                    </td>
                	<td>
                      <div style="line-height: 35px;">
                    		<?php echo get_post_meta( $event->ID, 'event_type', true ); ?>
                    </div>
                    <div style="color:#999; line-height: 0px;">
                    		<?php echo get_post_meta( $event->ID, 'format', true ); ?>
                    </div>
                    </td>
                	<td>
                    <div>
                    		<?php echo get_post_meta( $event->ID, 'fee_range', true ); ?>
                    </div>
                    </td><!--
                	<td>



                                                    POST HERE THE AGE GROUPS ACCORDINGLY


                    </td>-->
                </tr>
    <?php
		}
	?>
            </table>
        </div>
  </div>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/jquery.datetimepicker.css"/>
<script src="<?php echo get_template_directory_uri(); ?>/jquery.datetimepicker.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#start_date').datetimepicker({
			lang:'en',
			timepicker:false,
			format:'m/d/y',
			formatDate:'m/d/y',
		});

		jQuery('#end_date').datetimepicker({
			lang:'en',
			timepicker:false,
			format:'m/d/y',
			formatDate:'m/d/y',
		});
                jQuery('#startdate').click(function(){
                    jQuery('#start_date').datetimepicker('show'); //support hide,show and destroy command
                });
                jQuery('#enddate').click(function(){
                    jQuery('#end_date').datetimepicker('show'); //support hide,show and destroy command
                });
                jQuery("#findevent").on("click",function(){
                    jQuery("#findform").submit();
                });
	});
</script>
<?php get_footer();?>
<!-- Columns End -->

<?php
get_header();
$tournamentID = get_the_ID();
?>
<style>
    .grad{
      background: -webkit-linear-gradient(white, #cccccc); /* For Safari 5.1 to 6.0 */
      background: -o-linear-gradient(white, #cccccc); /* For Opera 11.1 to 12.0 */
      background: -moz-linear-gradient(white, #cccccc); /* For Firefox 3.6 to 15 */
      background: linear-gradient(white, #cccccc); /* Standard syntax */
      border:solid 1px #cccccc;
    }
    .container1{
		width: 100%;
		margin: 0 auto;
        padding:10px;
	}

	ul.tabs{
                    margin:0;
		padding: 0px;
		list-style: none;
	}
	ul.tabs li{
		background: none;
		color: #222;
		display: inline-block;
		padding: 2px 15px;
		cursor: pointer;
        font-size:10px;
        margin: 0 0 -1px 0;
        float:right;

	}
	ul.tabs li.current{
		background: #f6f6f6;
		color: #222;
	}
	.tab-content{
		display: none;
		background: ##FFFFFF;
		padding: 15px;
	}
	.tab-content.current{
		display: inherit;
	}
    .btn{
        margin-right:10px;
        padding: 0;
    }
    .tab-link{
        border-top:solid 1px #cccccc;
        border-left:solid 1px #cccccc;
        border-right:solid 1px #cccccc;
        text-transform: uppercase;
    }
    .accordion-inner{
        color:#00526F;
    }
    .infotable{
        height:auto;
    }
    .infotable tr td
    {
        border: 0px !important;
        padding-top:0px !important;
        font-size:11px !important;
    }
    .pagehead{
        font-size:2em;
        color:#de2026;
        font-weight: bold;
    }
    .headdate{
        font-size:14px;
    }
    .headvenue{
        font-weight:bold;
        font-size:14px;
    }
    body{
		font-family: 'Trebuchet MS', serif;
		line-height: 1.6
    }
</style>
<script>
    jQuery(document).ready(function () {
    	jQuery('ul.tabs li').click(function(){
    		var tab_id = jQuery(this).attr('data-tab');

    		jQuery('ul.tabs li').removeClass('current');
    		jQuery('.tab-content').removeClass('current');

    		jQuery(this).addClass('current');
    		jQuery("#"+tab_id).addClass('current');
    	});
    });
</script>
<div class="container">
    <div class="col-md-9 flow_sm">
        <div class="grad" style="padding:11.5px; margin-bottom:20px; ">
            <h1 class="pagehead"><?php echo get_the_title( $tournamentID );  ?></h1>
            <div><button type="button" onclick="javascript;" style="width:130px; padding:0 8px 0 8px; height:28px;background-color:#DE2026; float:right;" class="btn btn-danger"><span style="font-size:14px;">Weather Update</span></button>
            <span class="headvenue clear"><?php echo get_post_meta( $tournamentID, 'event_venue', true ); ?></span>
            <br>
            <span class="headdate">
            <?=date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'start_date', true )))  ?> - <?php echo date("m/d/Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )))  ?>
            </span>
            </div>
        </div>
    <?php if ( aaysc_tournament_common::is_children($tournamentID) ): ?>
            <h1><?=get_the_title( $tournamentID ). ' is coming soon!';  ?></h1>
        </div>
    <?php else: ?>
    <script>
        jQuery(document).ready(function () {
        	jQuery('ul.tabs li').click(function(){
        		var tab_id = jQuery(this).attr('data-tab');

        		jQuery('ul.tabs li').removeClass('current');
        		jQuery('.tab-content').removeClass('current');

        		jQuery(this).addClass('current');
        		jQuery("#"+tab_id).addClass('current');
        	});
        });
    </script>
            <div class="row">
                <div class="col-md-12">
                    <?php if( have_rows('event_gallery') ): ?>
                        <div id="<?=(count(get_field('event_gallery')) > 1)?'tournament-gallery':'tournament'; ?>">
                           <?php while ( have_rows('event_gallery') ) : the_row(); ?>
                             <?php  $images = get_sub_field('event_gallery_images');
                             if( $images ):?>
                                  <img src="<?=$images['url']; ?>" alt="<?php echo $images['alt']; ?>" width="729" />
                             <?php endif;
                           endwhile; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <div class="rich_editor_text pix-content-wrap">
            <p style="color:#000;margin:15px 0 5px 0 !important;font-size:16px;text-transform:uppercase;"><strong>Available Divisions</strong></p>

            <div class='division_box row' id="accordion">
                <?php
                global $wpdb;
                $even_parent;
                $odd_parent;
                $parents = get_terms('age_groups', 'orderby=count&hide_empty=0&parent=0');
                $number = 1;
                foreach ($parents as $parent) {
                    $age_price = get_post_meta(get_the_ID(), 'age_group_parent_' . $parent->term_id, true);
                    if ($age_price == '') { continue; }
                    if ($number % 2 == 0) {
                        $even_parent1 .= '<div style="clear:both;"></div><div class="accordion-heading" style="border:solid 1px #eee; font-size: 1.2em; background-color:#fafafa;padding:4px 10px; font-weight:bold;margin-bottom:5px;"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse'.$number.'"><span style="float:right;"><img src="'.get_template_directory_uri().'/images/nav-down.png" width="7"></span>';
                        $have_child = '';
                        $have_child = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE post_id=" . get_the_ID() . " AND meta_key LIKE 'age_group_child_" . $parent->term_id . "_%'");
                        if (count($have_child) == 0) {
                            $even_parent1 .= '<input type="radio" id="rd_age_' . $parent->term_id . '" name="age_groups_terms" onclick="return makeLink(' . $parent->term_id . ');" /><label for="rd_age_' . $parent->term_id . '">' . $parent->name . '</label>';
                        } else {
                            $even_parent1 .= '<span>'.$parent->name.'</span>';
                        }
                        $even_parent1 .= '<input type="hidden" id="age_'.$parent->term_id.'" value="' .site_url().'/tournament-registration/?price='.$age_price.'&tournament_id='.get_the_ID().'&ageClass='.$parent->name.'" /></a></div><div id="collapse'.$number.'" class="accordion-body collapse"><div class="accordion-inner">';
                        $childrens = get_terms('age_groups', 'orderby=count&hide_empty=0&parent=' . $parent->term_id);

                        foreach ($childrens as $my_child) {

                            $child_age_price = get_post_meta(get_the_ID(), 'age_group_child_' . $parent->term_id . '_' . $my_child->term_id, true);
                            if ($child_age_price == '') {
                                continue;
                            }
                            $term_change = str_replace("&", "and", $my_child->name);
                            $term_change = str_replace("andamp;", "and", $term_change);
                            $term_change = str_replace(" ", "_", $term_change);

                            $even_parent1 .='<div style="margin-left:20px;clear:both;"><div style="float:left;padding: 4px 6px;"><span class="radio radio-success"><input type="radio" id="rd_ch_age_' . $my_child->term_id . '" name="age_groups_terms" onclick="return makeLink(' . $my_child->term_id . ');" /><label for="rd_ch_age_' . $my_child->term_id . '">' . $my_child->name . '</label></span></div><div style="float:right;"> $ '.$child_age_price.'</div><input type="hidden" id="age_' . $my_child->term_id . '" value="' . site_url() . '/tournament-registration/?price=' . $child_age_price . '&tournament_id=' . get_the_ID() . '&ageClass=' . $term_change . '" /></div>';
                        }
                        $even_parent1   .=  '</div></div>';
                    } else {
                        $odd_parent .= '<div style="clear:both;"></div><div class="accordion-heading" style="border:solid 1px #eee; font-size: 1.2em; background-color:#fafafa;padding:4px 10px; font-weight:bold;margin-bottom:5px;"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse'.$number.'"><span style="float:right;"><img src="'.get_template_directory_uri().'/images/nav-down.png" width="7"></span>';
                        $have_child = '';
                        $have_child = $wpdb->get_results("SELECT * FROM wp_postmeta WHERE post_id=" . get_the_ID() . " AND meta_key LIKE 'age_group_child_" . $parent->term_id . "_%'");
                        if (count($have_child) == 0) {
                            $odd_parent .= '<input type="radio" id="rd_age_' . $parent->term_id . '" name="age_groups_terms" onclick="return makeLink(' . $parent->term_id . ');"  /><label for="rd_age_' . $parent->term_id . '">' . $parent->name . '</label>';
                        } else {
                            $odd_parent .= '<span>'.$parent->name.'</span>';
                        }
                        if($number == 1) $openclass = 'in';
                        else
                            $openclass  =   '';
                        $odd_parent .= '<input type="hidden" id="age_' . $parent->term_id . '" value="' . site_url() . '/tournament-registration/?price=' . $age_price . '&tournament_id=' . get_the_ID() . '&ageClass=' . $parent->name . '" />
                            </a></div><div id="collapse'.$number.'" class="accordion-body collapse '.$openclass.'"><div class="accordion-inner">';
                        $childrens = get_terms('age_groups', 'orderby=count&hide_empty=0&parent=' . $parent->term_id);
                        $selectcounter =    1;
                        foreach ($childrens as $my_child) {
                            if($number == 1){
                                if($selectcounter==1) $checked = 'checked';
                                else $checked = '';
                            }
                            $child_age_price = get_post_meta(get_the_ID(), 'age_group_child_' . $parent->term_id . '_' . $my_child->term_id, true);
                            if ($child_age_price == '') { continue; }
                            $term_change = str_replace("&", "and", $my_child->name);
                            $term_change = str_replace("andamp;", "and", $term_change);
                            $term_change = str_replace(" ", "_", $term_change);
                            $odd_parent .='<div style="margin-left:20px;clear:both;"><div style="float:left;padding: 4px 6px;"><span class="radio radio-success"><input type="radio" id="rd_ch_age_' . $my_child->term_id . '" name="age_groups_terms" onclick="return makeLink(' . $my_child->term_id . ');" '.$checked.' /><label for="rd_ch_age_' . $my_child->term_id . '">' . $my_child->name . '</label></span></div><div style="float:right;">$ '.$child_age_price.'</div><input type="hidden" id="age_' . $my_child->term_id . '" value="' . site_url() . '/tournament-registration/?price=' . $child_age_price . '&tournament_id=' . get_the_ID() . '&ageClass=' . $term_change . '" /></div>';
                            $selectcounter++;
                        }
                        $odd_parent   .=  '</div></div>';
                    }
                    ?>
                    <?php $number++; } ?>
                    <div class="division_row col-md-6 col-sm-6 col-xs-12">
                        <div class="accordion" id="accordion2">
                            <div class="accordion-group">
                                    <?=$odd_parent;?>
                            </div>
                        </div>
                    </div>
                    <div class="division_row col-md-6 col-sm-6 col-xs-12">
                        <?=$even_parent1;?>
                    </div>
                    <input type="hidden" id="current_radio" value="" />
            </div>
        </div>
        <?php
        $entryDeadline = aaysc_tournament_common::entry_deadline($tournamentID);
        ?>
        <?php if(strtotime($entryDeadline->format("M d, Y")) >= strtotime(date("M d, Y"))): ?>
        <div align="right" class="span2">
            <button type="button" onclick="return makeSubmit();" style="width:150px;height:28px;background-color:#DE2026;" class="btn btn-danger"><span style="font-size:14px;">Register For Event</span></button>
        </div>
        <?php endif; ?>
            <div>&nbsp;</div>
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="grad" style="padding:5px;">
                            <?php // check if the post has a Post Thumbnail assigned to it.
                            if ( has_post_thumbnail($tournamentID) ) {
                                $tournamnet_logo = wp_get_attachment_image_src( get_post_thumbnail_id($tournamentID), 'full' );
                                $tournamnet_logo = $tournamnet_logo[0];
                            } else {  $tournamnet_logo = get_field('event_logo', $tournamentID);
                                    $tournamnet_logo = $tournamnet_logo['sizes']['px_media_1']; } ?>
                            <img src="<?=$tournamnet_logo;?>" width="210" class="img-responsive" style="border:solid 1px #e3e3e3;" />
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="grad" style="padding:5px;">
                            <div>
                                <strong> QUICK TOURNAMENT PREVIEW </strong>
                            </div>
                            <div style="background-color:#FFFFFF;border:solid 1px #999999;margin-top:5px; padding:5px;">

                                <table width="100%" class="infotable" style="margin-bottom:8px;">
                                    <tr>
                                        <td><strong>Tournament Date:</strong></td>
                                        <td><?php echo date("M, d", strtotime(get_post_meta( $tournamentID, 'start_date', true ))) .' - '. date("M d, Y", strtotime(get_post_meta( $tournamentID, 'end_date', true )));  ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Tournament Format</strong></td>
                                        <td><?php echo get_post_meta( $tournamentID, 'format', true ); ?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Age Cut-off Date:</strong></td>
                                        <td>May 1st, 2016</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Entry Deadline:</strong></td>
                                        <td><?=$entryDeadline->format("M d, Y");?></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Sanctioning Body:</strong></td>
                                        <td>NA</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Gate Fee Information:</strong></td>
                                        <td>Gate Fee: Adult Daily Gate Fee- $5.00<br>
                                            Senior Citizen Daily Gate Fee- $3.00<br>
                                            15 & younger- Free</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>&nbsp;</div>
            <div class="row">
                <div class="col-md-12">

                    <div class="container1">
                        <ul class="tabs" >
                            <li class="tab-link" data-tab="tab-5">CONTACT</li>
                            <li class="tab-link" data-tab="tab-4">CHAMPIONSHIP BIRTHS</li>
                            <li class="tab-link" data-tab="tab-3">ROSTER REQUIREMENTS</li>
                            <li class="tab-link" data-tab="tab-2">EVENT REWARDS</li>
                            <li class="tab-link current" data-tab="tab-1">TOURNAMENT DETAILS</li>
                        </ul>
                        <div class="grad" style="clear:both;float:none;">
                            <div  style="margin:10px;border:solid 1px #999; background-color:#FFF;">
                                <div id="tab-5" class="tab-content">
                                        <?php echo get_post_meta( $tournamentID, 'contact', true ); ?>
                                </div>
                                <div id="tab-4" class="tab-content">
                                        <?php echo get_post_meta( $tournamentID, 'championship_births', true ); ?>
                                </div>
                                <div id="tab-3" class="tab-content">
                                         <?php echo get_post_meta( $tournamentID, 'roster_requirements', true ); ?>
                                </div>
                                <div id="tab-2" class="tab-content">
                                    <?php echo get_post_meta( $tournamentID, 'event_awards', true ); ?>
                                </div>
                                <div id="tab-1" class="tab-content current">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>

                    </div><!-- container -->
            </div>
            </div>
            <div>&nbsp;</div>
        <hr width=100%  align=left>
    </div>
    <?php endif; ?>
    <aside class="col-md-3" id="tour_nav_aside">
        <?php dynamic_sidebar('sidebar-1'); ?>
    </aside>
</div>

<script type="text/javascript">
    function makeLink(id)
    {
        document.getElementById('current_radio').value = id;
    }
    function makeSubmit()
    {
        radio = document.getElementById('current_radio').value;
        if (radio == "")
            alert("please select one of divisions.");
        url = document.getElementById('age_' + radio).value;
        window.location = url;
    }
</script>
<style type="text/css">
    .radio {
        padding-left: 20px;
        display:initial !important;
    }
    .radio label {
        display: inline-block;
        vertical-align: middle;
        position: relative;
        padding-left: 5px; }
    .radio label::before {
        content: "";
        display: inline-block;
        position: absolute;
        width: 12px;
        height: 12px;
        left: 0;
        margin-left: -20px;
        border: 1px solid #cccccc;
        border-radius: 50%;
        background-color: #fff;
        -webkit-transition: border 0.15s ease-in-out;
        -o-transition: border 0.15s ease-in-out;
        transition: border 0.15s ease-in-out; }
    .radio label::after {
        display: inline-block;
        position: absolute;
        content: " ";
        width: 11px;
        height: 11px;
        left: 3px;
        top: 3px;
        margin-left: -20px;
        border-radius: 50%;
        background-color: #555555;
        -webkit-transform: scale(0, 0);
        -ms-transform: scale(0, 0);
        -o-transform: scale(0, 0);
        transform: scale(0, 0);
        -webkit-transition: -webkit-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -moz-transition: -moz-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        -o-transition: -o-transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33);
        transition: transform 0.1s cubic-bezier(0.8, -0.33, 0.2, 1.33); }
    .radio input[type="radio"] {
        opacity: 0;
        z-index: 1; }
    .radio input[type="radio"]:focus + label::before {
        outline: thin dotted;
        outline: 5px auto -webkit-focus-ring-color;
        outline-offset: -2px; }
    .radio input[type="radio"]:checked + label::after {
        -webkit-transform: scale(0.2, 0.2);
        -ms-transform: scale(0.2, 0.2);
        -o-transform: scale(0.2,0.2);
        transform: scale(0.2, 0.2); }
    .radio input[type="radio"]:disabled + label {
        opacity: 0.65; }
    .radio input[type="radio"]:disabled + label::before {
        cursor: not-allowed; }
    .radio.radio-inline {
        margin-top: 0; }

    .radio-primary input[type="radio"] + label::after {
        background-color: #337ab7; }
    .radio-primary input[type="radio"]:checked + label::before {
        border-color: #337ab7;
    }
    .radio-primary input[type="radio"]:checked + label::after {
        background-color: #337ab7; }

    .radio-danger input[type="radio"] + label::after {
        background-color: #d9534f; }
    .radio-danger input[type="radio"]:checked + label::before {
        border-color: #d9534f; }
    .radio-danger input[type="radio"]:checked + label::after {
        background-color: #d9534f; }

    .radio-info input[type="radio"] + label::after {
        background-color: #5bc0de; }
    .radio-info input[type="radio"]:checked + label::before {
        border-color: #5bc0de; }
    .radio-info input[type="radio"]:checked + label::after {
        background-color: #5bc0de; }

    .radio-warning input[type="radio"] + label::after {
        background-color: #f0ad4e; }
    .radio-warning input[type="radio"]:checked + label::before {
        border-color: #f0ad4e; }
    .radio-warning input[type="radio"]:checked + label::after {
        background-color: #f0ad4e; }

    .radio-success input[type="radio"] + label::after {
        background-color: transparent; }
    .radio-success input[type="radio"]:checked + label::before {
        border:solid 5px #009dda; }
    .radio-success input[type="radio"]:checked + label::after {
        background-color: #transparent; }

    input[type="checkbox"].styled:checked + label:after,
    input[type="radio"].styled:checked + label:after {
        font-family: 'FontAwesome';
        content: "\f00c"; }
    input[type="checkbox"] .styled:checked + label::before,
    input[type="radio"] .styled:checked + label::before {
        color: #fff; }
    input[type="checkbox"] .styled:checked + label::after,
    input[type="radio"] .styled:checked + label::after {
        color: #fff; }

</style>
<?php get_footer(); ?>
<!-- Columns End -->

<?php
get_header();
global  $px_theme_option;
if(!isset($px_theme_option['px_layout'])){ $px_layout = 'right'; }
if(isset($px_theme_option['px_layout'])){ $px_layout = $px_theme_option['px_layout']; }else{ $px_layout = '';}
?>
<?php
if ( $px_layout <> '' and $px_layout  <> "none" and $px_layout  == 'left') :  ?>
	<aside class="left-content col-md-3">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($px_theme_option['px_sidebar_left']) ) : endif; ?>
	</aside>
<?php endif; ?>
	<div class="<?php echo 'col-md-9 flow_sm'; ?>">
		<?php px_page_title();
		$pageID = $_GET['pageID'] ;
		 $rowNumber = $_GET['rowNumber'] ;
         $field_key = "schedule_row_".$rowNumber."_tournament_title";
		 $subtitle = get_post_meta($pageID,$field_key,true);
		 echo "<h2>".$subtitle."</h2>";
		
		?>
        
        
        <div class="pix-blog blog-medium">
			<table  class="pool_table t_result">
               <tr style="display:none;"><th colspan="11"><?php echo  "Pool ".$poolName ; ?></th></tr>
				<tr>
					<th>
						Team #
					</th><th>
						C
					</th>
					<th>Team Name</th>
					<th>W</th>
					<th>L</th>
					<th>T</th>
					<th>PCT</th>
					<th>ARA</th>
					<th>ARS</th>
					<th>ARD</th>
					<th>Seed</th>
				</tr>
			<?php
			$iterator = 0 ;
			while(have_posts()):the_post();
				?>
				<tr>
                	<td> <?php echo  ++$iterator;  ?></td>
					<td> <?php the_field("aaysc_points") ?></td>
					<td> <?php the_title(); ?></td>
					<td> <?php the_field('win'); ?></td>
					<td> <?php the_field('lost'); ?></td>
					<td> <?php the_field('tied'); ?></td>
					<td> <?php the_field('win_pct'); ?></td>
					<td> <?php the_field('average_runs'); ?></td>
					<td> <?php the_field('avergae_runs_scored'); ?></td>
					<td> <?php the_field('average_runs_diff'); ?></td>
					<td> <?php the_field('seed'); ?></td>



				</tr>


				<?php
				endwhile;
			?>
			</table>
		</div>
        
	</div>
<?php
$tournamentID = $_GET['tournament_id'];
//if ( $px_layout <> '' and $px_layout  <> "none" and $px_layout  == 'right') :  ?>
	<aside class="left-content col-md-3" id="tour_nav_aside">
		 <div class="tournament_nav">
                                    <div class="list-group">
  <a class="list-group-item" href="<?php echo get_permalink($pageID); ?>">Event Home</a>
  <a class="list-group-item" href="<?php echo site_url()."/pool-schedules?tournament_id=".$tournamentID."&pageID=".$pageID; ?>" >Pool Schedule</a>
 <a class="list-group-item active" href="<?php echo site_url()."/?tournament=june-19-21st-tournament&tournament_id=".$tournamentID."&pageID=".$pageID; ?>">Tournament Results</a>  <a class="list-group-item" href="<?php echo site_url()."/tour_brackets?tournament_id=".$tournamentID."&pageID=".$pageID; ?>">Tournament Bracket</a>
  <a class="list-group-item" href="<?php echo site_url()."/map?tournament_id=".$tournamentID."&pageID=".$pageID; ?>" >Map</a>
  <a class="list-group-item" href="<?php echo site_url()."/points-table?tournament_id=".$tournamentID."&pageID=".$pageID; ?>" >Look Who's Coming</a>
  <a class="list-group-item" href="<?php echo site_url()."/rules?tournament_id=".$tournamentID."&pageID=".$pageID; ?>" >Tournament Rules</a>
                                    </div>
                                  </div>
		<?php
		if(isset($px_theme_option['px_sidebar_right'])){
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar($px_theme_option['px_sidebar_right']) ) : endif;
		}else{
			if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar-1') ) : endif;
		}
		?>
	</aside>
<?php // endif; ?>
<?php get_footer(); ?>